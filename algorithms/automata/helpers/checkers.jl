
#=
@everywhere include(ENV["PYGMALION_DIR"] * "algorithms/automata/period_automaton.jl")
@everywhere include(ENV["PYGMALION_DIR"] * "algorithms/automata/region_automaton.jl")
@everywhere include(ENV["PYGMALION_DIR"] * "algorithms/automata/region_automaton2.jl")
@everywhere include(ENV["PYGMALION_DIR"] * "algorithms/automata/region_automaton3.jl")
=#

function check_periodicity(so::SystemObservation, values)
    check = true
    for om in values[1]
        A = PeriodAutomaton(so, om; prct_extremum = 0.1)
        step_eps = values[2]
        nb_periods = values[3]
        tp = values[4]
        ep = values[5][step_eps]
        dt = values[6]
        check = test_so(A, so, om, dt, nb_periods; time_period = tp, err_period = ep)
        if !check
            return false
        end
    end
    return true
end

const dict_properties = Dict([("periodicity",check_periodicity)])

function check_properties(so::SystemObservation, ll_properties::LabelList, dict_properties_values::Dict)
	check = true
	for property in ll_properties
		check = dict_properties[property](so, dict_properties_values[property])
		if !check
			return false
		end
	end
	return true
end

