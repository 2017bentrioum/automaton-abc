
import LinearAlgebra: inv, det
import DelimitedFiles: writedlm
import SpecialFunctions: beta
import Base: rand, length, maximum, minimum
import Statistics: std
import Random: AbstractRNG
#import Distributions: pdf, logpdf, _logpdf, _rand!, ContinuousUnivariateDistribution, ContinuousMultivariateDistribution
using Distributions
import QuadGK: quadgk
import HCubature: hcubature
using PyCall
pa = pyimport("matplotlib.patches")
cm = pyimport("matplotlib.cm")
mplot3d = pyimport("mpl_toolkits.mplot3d")

struct UnivariateKDE <: ContinuousUnivariateDistribution
    l_obs::Vector{Float64}
    l_obs_scaled::Vector{Float64}
    nbr_obs::Int
    weights::Vector{Float64}
    bandwidth::Float64
    kernel::String
    lower_bound::Float64
    upper_bound::Float64
end
UnivariateKDE(l_obs::Vector{Float64}, bandwidth::Float64) = 
UnivariateKDE(l_obs, zeros(0), length(l_obs), [1/length(l_obs) for i=1:length(l_obs)], bandwidth, "gaussian", -Inf, Inf)
function UnivariateKDE(l_obs::Vector{Float64}; 
                       weights::Union{Vector{Float64}, Nothing}=nothing, bandwidth::Float64=0.0, 
                       kernel::String="gaussian", lower_bound::Float64=-Inf, upper_bound::Float64=Inf)
    @assert lower_bound < upper_bound
    len_obs = length(l_obs)
    l_w = (weights == nothing) ? [1/len_obs for i=1:len_obs] : weights
    if kernel == "gaussian"
        b = (bandwidth == 0.0) ? 1.06*std(l_obs)*len_obs^(-1/5) : bandwidth
        return UnivariateKDE(l_obs, l_obs, len_obs, l_w, b, kernel, -Inf, Inf)
    elseif kernel == "chen99"
        b = (bandwidth == 0.0) ? len_obs^(-2/5) : bandwidth
        l_bound = (lower_bound == -Inf) ? 0.0 : lower_bound
        u_bound = (upper_bound == Inf) ? 1.0 : upper_bound
        obs_scaled = zeros(len_obs)
        for i = 1:len_obs
            obs_scaled[i] = (l_obs[i]-l_bound)/(u_bound-l_bound)
        end
        return UnivariateKDE(l_obs, obs_scaled, len_obs, l_w, b, kernel, l_bound, u_bound)
    else 
        return missing
    end
end
length(d::UnivariateKDE) = 1
rand(gen::AbstractRNG, d::UnivariateKDE) = rand(gen, Normal(d.l_obs[rand(gen, Categorical(d.weights))], d.bandwidth))
function rand(d::UnivariateKDE)
    if d.kernel == "gaussian"
        return rand(Normal(d.l_obs[rand(1:(d.nbr_obs))], d.bandwidth))
    elseif d.kernel == "chen99"
        return missing
    end
    return missing
end
Distributions.sampler(d::UnivariateKDE) = d
Distributions.pdf(d::UnivariateKDE, x::Real) = exp(logpdf(d, x))
#cdf
#quantile
Distributions.maximum(d::UnivariateKDE) = d.upper_bound
Distributions.minimum(d::UnivariateKDE) = d.lower_bound
function Distributions.mean(d::UnivariateKDE)
    if d.kernel == "gaussian"
        res = 0.0
        for i = 1:d.nbr_obs
            res += d.l_obs[i]
        end
        return res / d.nbr_obs
    end
    return missing
end
Distributions.insupport(d::UnivariateKDE, x::Real) = (d.lower_bound <= x <= d.upper_bound)
eltype(d::UnivariateKDE) = Float64
gaussian_kernel(x_obs::Float64, mu::Float64, h::Float64) = exp(-0.5*log(2*pi) - (x_obs - mu)^2 / (2*h^2))
beta_integrande(x_obs::Float64, x::Float64, h::Float64) = x_obs^(x/h) * (1 - x_obs)^((1-x)/h)
function Distributions.logpdf(d::UnivariateKDE, x::Real)
    if d.kernel == "gaussian"
        elt_logpdf = 0.0
        for j = 1:d.nbr_obs
            elt_logpdf += d.weights[j] * gaussian_kernel(x, d.l_obs[j], d.bandwidth) 
        end
        elt_logpdf = -log(d.bandwidth) + log(elt_logpdf)
        return elt_logpdf
    elseif d.kernel == "chen99"
        x_scaled = (x-d.lower_bound)/(d.upper_bound-d.lower_bound)
        elt_logpdf = 0.0
        for j = 1:d.nbr_obs
            obs_scaled = (d.l_obs[j]-d.lower_bound)/(d.upper_bound-d.lower_bound)
            elt_logpdf += d.weights[j] * beta_integrande(obs_scaled, x_scaled, d.bandwidth)
        end
        elt_logpdf = -log(d.upper_bound - d.lower_bound) -log(beta(x_scaled/d.bandwidth + 1, (1-x_scaled)/d.bandwidth + 1)) + log(elt_logpdf)
        return elt_logpdf
    end
    return missing
end
# Leave-one-out estimator
function loo_kde(d::UnivariateKDE, i::Int, bw::Float64)
    if d.kernel == "chen99"
        func_num_res = beta_integrande
    elseif d.kernel == "gaussian"
        func_num_res = gaussian_kernel
    else
        return missing
    end
    l_res = zeros(Threads.nthreads())
    l_norm_weights = zeros(Threads.nthreads())
    Threads.@threads for j = 1:d.nbr_obs
        if (j == i) continue end
        th_id = Threads.threadid()
        l_res[th_id] += d.weights[j] * func_num_res(d.l_obs_scaled[j], 
                                                    d.l_obs_scaled[i], bw)
        l_norm_weights[th_id] += d.weights[j]
    end
    norm_weights = sum(l_norm_weights)
    res = sum(l_res)
    cte_denom = 0.0
    if d.kernel == "chen99"
        cte_denom = ((d.upper_bound - d.lower_bound)*beta(d.l_obs_scaled[i]/bw + 1, 
                                                          (1-d.l_obs_scaled[i])/bw + 1)*norm_weights)
    elseif d.kernel == "gaussian"
        cte_denom = (bw*norm_weights)
    else 
        return missing
    end
    return res / cte_denom
end
lscv_gaussian_convol(X_i::Float64, X_j::Float64, h::Float64) = 
    (1/(2*h*sqrt(pi)))*exp(-(X_i-X_j)^2/(4*h^2))
function lscv(d::UnivariateKDE, bw::Float64)
    if d.kernel == "chen99"
        int_kde_square = zeros(Threads.nthreads())
        Threads.@threads for k = 1:d.nbr_obs
            th_id = Threads.threadid()
            integral, err = quadgk(x -> 
                                   beta_integrande(d.l_obs_scaled[k], x, bw)^2 /
                                   beta(x/bw + 1, (1-x)/bw + 1)^2, 
                                   0, 1, rtol=1e-5)
            int_kde_square[th_id] += d.weights[k]^2 * integral
        end
        Threads.@threads for i = 1:d.nbr_obs
            th_id = Threads.threadid()
            @inbounds for j = (i+1):d.nbr_obs
                integral, err = quadgk(x -> 
                (beta_integrande(d.l_obs_scaled[i], x, bw) *
                beta_integrande(d.l_obs_scaled[j], x, bw)) /
                beta(x/bw + 1, (1-x)/bw + 1)^2, 
                0, 1, rtol=1e-5)
                int_kde_square[th_id] += 2*d.weights[i]*d.weights[j] * integral
            end
        end
        int_kde_sq = sum(int_kde_square)
        int_kde_sq /= (d.upper_bound - d.lower_bound)
        res_loo = 0.0
        for i = 1:d.nbr_obs
            res_loo += loo_kde(d, i, bw)
        end
        return (int_kde_sq - (2/d.nbr_obs)*res_loo) 
    elseif d.kernel == "gaussian"
        int_kde_square = zeros(Threads.nthreads())
        Threads.@threads for k = 1:d.nbr_obs
            th_id = Threads.threadid()
            int_kde_square[th_id] += d.weights[k]^2 * (1/(2*bw*sqrt(pi)))
        end
        Threads.@threads for i = 1:d.nbr_obs
            th_id = Threads.threadid()
            @inbounds for j = (i+1):d.nbr_obs
                int_kde_square[th_id] += 2*d.weights[i]*d.weights[j] * lscv_gaussian_convol(d.l_obs[i], d.l_obs[j], bw)
            end
        end
        int_kde_sq = sum(int_kde_square)
        res_loo = 0.0
        for i = 1:d.nbr_obs
            res_loo += loo_kde(d, i, bw)
        end
        return (int_kde_sq - (2/d.nbr_obs)*res_loo) 
 
    else
        return missing
    end
end
function optim_bw(d::UnivariateKDE, l_bound::Float64, u_bound::Float64, nbr_points::Int; 
                  flag_save_results = false, path_results = "./")
    bw_ref = 0.0
    if d.kernel == "chen99"
        bw_ref = d.nbr_obs^(-2/5)
    elseif d.kernel == "gaussian"
        bw_ref = 1.06*std(d.l_obs)*d.nbr_obs^(-1/5)
    else
        return missing
    end
    lscv_min = Inf
    bw_min = 0.0
    a = (l_bound*bw_ref)
    b = (u_bound*bw_ref)
    l_bw = a:((b-a)/(nbr_points-1)):b
    l_lscv = zeros(nbr_points)
    for i = 1:nbr_points
        bw = l_bw[i]
        lscv_current = lscv(d, bw) 
        l_lscv[i] = lscv_current
        if (lscv_current <= lscv_min) 
            bw_min = bw 
            lscv_min = lscv_current
        end
        @show lscv_current
    end
    if flag_save_results
        if !isdir(path_results) mkdir(path_results) end
        coefft_bw = bw_min / bw_ref
        @show coefft_bw, bw_min, lscv_min
        fig = plt.figure()
        plt.plot(l_bw, l_lscv, "ro--")
        plt.title("coeff min = $coefft_bw ; bw_min = $bw_min\n nbr_obs = $(d.nbr_obs)")
        plt.savefig(path_results * "lscv.png", dpi=480)
        plt.close()
        mat_bw = zeros(nbr_points,2)
        mat_bw[:,1] = convert(Array, l_bw)
        mat_bw[:,2] = convert(Array, l_bound:((u_bound-l_bound)/(nbr_points-1)):u_bound)
        writedlm(path_results * "bw.csv", mat_bw, ',')
        writedlm(path_results * "lscv.csv", l_lscv, ',')
    end
    return bw_min
end


struct MultivariateKDE <: ContinuousMultivariateDistribution
    l_obs::Array{Float64, 2} # one column = one sample
    l_obs_scaled::Array{Float64, 2} # one column = one sample
    nbr_obs::Int
    weights::Array{Float64, 1}
    bandwidth::AbstractArray
    kernel::String
    lower_bound::Array{Float64, 1}
    upper_bound::Array{Float64, 1}
end
MultivariateKDE(l_obs::Array{Float64, 2}, bandwidth::Float64) = MultivariateKDE(l_obs, zeros(0,0), size(l_obs)[2], 
                                                                                (1/size(l_obs)[2])*ones(size(l_obs)[2]), 
                                                                                bandwidth*ones(size(l_obs)[1]), "gaussian",
                                                                                -Inf*ones(size(l_obs)[1]), Inf*ones(size(l_obs)[1]))
function MultivariateKDE(l_obs::Array{Float64, 2}; 
                         weights::Union{Vector{Float64}, Nothing}=nothing, bandwidth::Union{Float64, AbstractArray}=0.0, 
                         kernel::String="gaussian", 
                         lower_bound::Union{Float64, Array{Float64, 1}}=-Inf, 
                         upper_bound::Union{Float64, Array{Float64, 1}}=Inf)
    dims = size(l_obs)[1]
    len_obs = size(l_obs)[2]
    l_w = (weights == nothing) ? [1/len_obs for i=1:len_obs] : weights
    if kernel == "gaussian" || kernel == "gaussian2"
        b = bandwidth
        if isa(bandwidth, Float64)
            b = (bandwidth == 0.0) ? [(4/(dims+2))^(1/(dims+4))*len_obs^(-1/(dims+4)) for i=1:dims] : [bandwidth for i=1:dims]
        end
        return MultivariateKDE(l_obs, l_obs, len_obs, l_w, b, kernel, -Inf.*ones(dims), Inf.*ones(dims))
    elseif kernel == "chen99"
        b = bandwidth
        if isa(bandwidth, Float64)
            b = (bandwidth == 0.0) ? [len_obs^(-2/(dims+4)) for i=1:dims] : [bandwidth for i=1:dims]
        end
        l_bound = lower_bound
        if isa(lower_bound, Float64)
            l_bound = (lower_bound == -Inf) ? zeros(dims) : lower_bound*ones(dims)
        end
        u_bound = upper_bound
        if isa(upper_bound, Float64)
            u_bound = (upper_bound == Inf) ? ones(dims) : upper_bound*ones(dims)
        end
        obs_scaled = zeros(dims, len_obs)
        for i = 1:len_obs
            obs_scaled[:,i] = (l_obs[:,i]-l_bound)./(u_bound-l_bound)
        end
        return MultivariateKDE(l_obs, obs_scaled, len_obs, l_w, b, kernel, l_bound, u_bound)
    else 
        return missing
    end
end
Distributions.length(d::MultivariateKDE) = size(d.l_obs)[1]
Distributions.sampler(d::MultivariateKDE) = d
Distributions.eltype(d::MultivariateKDE) = Float64
function Distributions._rand!(gen::AbstractRNG, d::MultivariateKDE, x::AbstractVector)
    if d.kernel == "gaussian"
        idx = rand(gen, Categorical(d.weights))
        _rand!(gen, MvNormal(d.l_obs[:, idx], d.bandwidth), x)
    end
end
gaussian_kernel(x::Vector{Float64}, mu::Vector{Float64}, h::Float64) = exp(-(length(x)/2)*log(2*pi) - ((x - mu)'*(x - mu) / (2*h^2)))
gaussian_kernel(x::Vector{Float64}, mu::Vector{Float64}, inv_H::AbstractArray) = 1/(sqrt((2*pi)^length(x))*sqrt((det(inv_H))))*exp(-0.5*(x - mu)'*inv_H*(x - mu))
gaussian_kernel(x::Vector{Float64}) = exp(-(length(x)/2)*log(2*pi) - (x'*x)/2)
# With product of univariate kde
function chen99_multivariate_kde(d::MultivariateKDE, x::AbstractArray)
    if d.kernel == "chen99"
        elt_pdf = 0.0
        dims = size(d.l_obs)[1]
        x_scaled = (x-d.lower_bound)./(d.upper_bound-d.lower_bound)
        for i = 1:d.nbr_obs
            elt_pdf_obs = d.weights[i]
            for s = 1:dims
                elt_pdf_obs *= beta_integrande(d.l_obs_scaled[s,i], x_scaled[s], d.bandwidth[s])
            end
            elt_pdf += elt_pdf_obs
        end
        cte_denom = prod(d.upper_bound - d.lower_bound)
        for s = 1:dims
            cte_denom *= beta(x_scaled[s]/d.bandwidth[s] + 1, (1-x_scaled[s])/d.bandwidth[s] + 1) 
        end
        elt_pdf = elt_pdf / cte_denom
        return elt_pdf
    end
    return missing
end
function Distributions._logpdf(d::MultivariateKDE, x::AbstractArray)
    if d.kernel == "gaussian"
        elt_logpdf = 0.0
        for j = 1:d.nbr_obs
            elt_logpdf += d.weights[j] * gaussian_kernel((x - d.l_obs[:,j]) ./ d.bandwidth) 
        end
        elt_logpdf = - log(prod(d.bandwidth)) + log(elt_logpdf) # verif si ya pas sqrt
        return elt_logpdf
    elseif d.kernel == "gaussian2"
        elt_logpdf = 0.0
        inv_H = inv(d.bandwidth)
        for j = 1:d.nbr_obs
            elt_logpdf += d.weights[j] * gaussian_kernel(x, d.l_obs[:,j], inv_H) 
        end
        return log(elt_logpdf)
    elseif d.kernel == "chen99"
        return log(chen99_multivariate_kde(d, x))
    else
        return missing
    end
    return missing
end
# Leave-one-out estimator
function loo_kde(d::MultivariateKDE, i::Int, bw::Vector{Float64})
    if d.kernel == "chen99"
        func_num_res = beta_integrande
    elseif d.kernel == "gaussian"
        func_num_res = gaussian_kernel
    else
        return missing
    end
    l_res = zeros(Threads.nthreads())
    dim = size(d.l_obs)[1]
    l_res = zeros(Threads.nthreads())
    l_norm_weights = zeros(Threads.nthreads())
    Threads.@threads for j = 1:d.nbr_obs
        if (j == i) continue end
        th_id = Threads.threadid()
        for s=1:dim
            l_res[th_id] += d.weights[j] * func_num_res(d.l_obs_scaled[s,j], 
                                                           d.l_obs_scaled[s,i], bw[s])
        end
        l_norm_weights[th_id] += d.weights[j]
    end
    norm_weights = sum(l_norm_weights)
    res = sum(l_res)
    
    cte_denom = 0.0
    if d.kernel == "chen99"
        cte_denom = prod(d.upper_bound - d.lower_bound)
        for s = 1:dim
            cte_denom *= beta(d.l_obs_scaled[s,i]/bw[s] + 1, (1-d.l_obs_scaled[s,i])/bw[s] + 1)
        end
        cte_denom *= norm_weights
    elseif d.kernel == "gaussian"
        cte_denom = prod(bw)*norm_weights
    else 
        return missing
    end
    return res / cte_denom
    #=
    if d.kernel == "chen99"
    norm_weights = 0.0
        res = 0.0
        dims = size(d.l_obs)[1]
        for j = 1:d.nbr_obs
            if (j == i) continue end
            res_j = d.weights[j] 
            for s = 1:dims
                res_j *= beta_integrande(d.l_obs_scaled[s,j], d.l_obs_scaled[s,i], bw[s])
            end
            res += res_j
            norm_weights += d.weights[j]
        end
        cte_denom = prod(d.upper_bound - d.lower_bound)
        for s = 1:dims
            cte_denom *= beta(d.l_obs_scaled[s,i]/bw[s] + 1, (1-d.l_obs_scaled[s,i])/bw[s] + 1)
        end
        res /= (cte_denom*norm_weights)
        return res
    end
    =#
end
function int_estim_square(x::AbstractArray, d::MultivariateKDE, bw::Vector{Float64}, i::Int, j::Int)
    res = 1.0
    for s = 1:size(d.l_obs)[1]
        res *= (beta_integrande(d.l_obs_scaled[s, i], x[s], bw[s]) * 
                beta_integrande(d.l_obs_scaled[s, j], x[s], bw[s])) /
        beta(x[s]/bw[s] + 1, (1-x[s])/bw[s] + 1)^2
    end
    return res
end
function lscv_gaussian_convol(X_i::Vector{Float64}, X_j::Vector{Float64}, h::Vector{Float64})
    dim = length(X_i)
    sigma2_prod = 1/(2*sum(h.^-2))
    mu_prod = 0.0
    for s = 1:dim
        mu_prod += (X_i[s] + X_j[s])/(h[s])^2
    end
    mu_prod *= sigma2_prod
    res = (2*pi)^(-(2*dim-1)/2)
    res *= sqrt(sigma2_prod / prod(h.^4))
    in_exp = 0.0
    for s = 1:dim
        in_exp += (X_i[s] + X_j[s])^2/(h[s])^2
    end
    in_exp -= mu_prod^2 / sigma2_prod
    in_exp *= -0.5
    res *= exp(in_exp)
    return res
end
function lscv(d::MultivariateKDE, bw::Vector{Float64})
    if d.kernel == "chen99"
        int_kde_square = zeros(Threads.nthreads())
        dims = size(d.l_obs)[1]
        Threads.@threads for k = 1:d.nbr_obs
            th_id = Threads.threadid()
            integral, err = hcubature(x -> int_estim_square(x, d, bw, k, k), 
                                   zeros(dims), ones(dims), rtol=1e-5)
            int_kde_square[th_id] += d.weights[k]^2 * integral
        end
        Threads.@threads for i = 1:d.nbr_obs
            th_id = Threads.threadid()
            @inbounds for j = (i+1):d.nbr_obs
                integral, err = hcubature(x -> int_estim_square(x, d, bw, i, j),  
                                       zeros(dims), ones(dims), rtol=1e-5)
                int_kde_square[th_id] += 2*d.weights[i]*d.weights[j] * integral
            end
        end
        int_kde_sq = sum(int_kde_square)
        int_kde_sq /= prod(d.upper_bound - d.lower_bound)
        res_loo = 0.0
        for i = 1:d.nbr_obs
            res_loo += loo_kde(d, i, bw)
        end
        return (int_kde_sq - (2/d.nbr_obs)*res_loo) 
    elseif d.kernel == "gaussian"
        int_kde_square = zeros(Threads.nthreads())
        dims = size(d.l_obs)[1]
        Threads.@threads for k = 1:d.nbr_obs
            th_id = Threads.threadid()
            int_kde_square[th_id] += d.weights[k]^2 * lscv_gaussian_convol(d.l_obs[:,k], d.l_obs[:,k], bw)
        end
        Threads.@threads for i = 1:d.nbr_obs
            th_id = Threads.threadid()
            @inbounds for j = (i+1):d.nbr_obs
                int_kde_square[th_id] += 2*d.weights[i]*d.weights[j] * lscv_gaussian_convol(d.l_obs[:,i], d.l_obs[:,j], bw)
            end
        end
        int_kde_sq = sum(int_kde_square)
        res_loo = 0.0
        for i = 1:d.nbr_obs
            res_loo += loo_kde(d, i, bw)
        end
        return (int_kde_sq - (2/d.nbr_obs)*res_loo) 
    else
        return missing
    end
end
function optim_bw(d::MultivariateKDE, l_bound::Vector{Float64}, u_bound::Vector{Float64}, nbr_points::Vector{Int64}; 
                  flag_save_results = false, path_results = "./")
    dims = size(d.l_obs)[1]
    if dims > 2 error("optim_bw not supported for dims > 2") end
    bw_ref = 0.0
    if d.kernel == "chen99"
        bw_ref = d.nbr_obs^(-2/(dims+4))
    elseif d.kernel == "gaussian"
        bw_ref = (4/(dims+2))^(1/(dims+4))*d.nbr_obs^(-1/(dims+4))
    else
        return missing
    end
    lscv_min = Inf
    bw_min = 0.0
    a = (l_bound*bw_ref)
    b = (u_bound*bw_ref)
    step_1 = ((b[1]-a[1])/(nbr_points[1]-1))
    step_2 = ((b[2]-a[2])/(nbr_points[2]-1))
    tuple_pts = (nbr_points[1], nbr_points[2])
    l_lscv = zeros(tuple_pts)
    l_x = zeros(tuple_pts)
    l_y = zeros(tuple_pts)
    for i = 1:nbr_points[1]
        for j = 1:nbr_points[2]
            x = a[1] + (i-1)*step_1
            y = a[2] + (j-1)*step_2
            bw = [x, y]
            l_x[i, j] = x
            l_y[i, j] = y
            println("$bw ...")
            lscv_current = lscv(d, bw) 
            l_lscv[i, j] = lscv_current
            if (lscv_current <= lscv_min) 
                bw_min = bw 
                lscv_min = lscv_current
            end
            @show lscv_current
        end
    end
    if flag_save_results
        if !isdir(path_results) mkdir(path_results) end
        fig = plt.figure()
        plt.title("bw_min = $(bw_min)")
        ax = fig.gca(projection="3d")
        surf = ax.plot_surface(l_x, l_y, l_lscv, cmap=cm.coolwarm, linewidth=0, antialiased=false)
        ax.set_xlabel("x")
        ax.set_ylabel("y")
        fig.colorbar(surf, shrink=0.5, aspect=5)
        plt.savefig(path_results * "lscv.png", dpi=480)
        plt.close()
        writedlm(path_results * "mesh_x.csv", l_x, ',')
        writedlm(path_results * "mesh_y.csv", l_y, ',')
        writedlm(path_results * "lscv.csv", l_lscv, ',')
    end
    return bw_min
end

