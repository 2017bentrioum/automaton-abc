
import Distributions: pdf
import KernelDensity: kde_lscv
import Distributed: @distributed

function redirect_debug_files(dofunc, outfile, errfile)
	open(outfile, "w") do out
		open(errfile, "w") do err
			redirect_stdout(out) do
				redirect_stderr(err) do
					dofunc()
				end
			end
		end
	end
end

function estimate_probability(A::Union{RegionAutomatonF_v2, RegionAutomatonG_v2, 
                              RegionAutomatonG_F}, alpha::Float64, epsilon::Float64, 
                              f::Function, g::ObserverList, 
                              x0::State, u::Control, p::Parameters, 
                              mn, on; om_F = "P", om_G = "E") where {State, Parameters, Control, ModellingNoise, ObservationNoise}
    N = convert(Int, round(-log(alpha/2) / (2*epsilon^2)))
    @show N
    if nprocs() == 1
        return (seq_estimate_probability(A, N, f, g, x0, u, p, mn, on; om_F = om_F, om_G = om_G) / N)
    else
        return par_estimate_probability(A, N, f, g, x0, u, p, mn, on; om_F = om_F, om_G = om_G)
    end
end

function par_estimate_probability(A::Union{RegionAutomatonF_v2, RegionAutomatonG_v2, 
                              RegionAutomatonG_F}, N::Int64, 
                              f::Function, g::ObserverList, 
                              x0::State, u::Control, p::Parameters, 
                              mn, on; om_F = "P", om_G = "E") where {State, Parameters, Control, ModellingNoise, ObservationNoise}
    l_nbr_checked = zeros(nworkers())
    @sync for id_w in workers()
        N_worker = div(N, nworkers())
        N_worker = (id_w == workers()[end]) ? (N % nworkers()) + N_worker : N_worker
        t_id_w = id_w - workers()[1] + 1
        @async l_nbr_checked[t_id_w] = remotecall_fetch(() -> seq_estimate_probability(A, N_worker, f, g, x0, u, p, mn, on; om_F = om_F, om_G = om_G), id_w)
    end
    return (sum(l_nbr_checked) / N)
end

function seq_estimate_probability(A::Union{RegionAutomatonF_v2, RegionAutomatonG_v2, 
                              RegionAutomatonG_F}, N::Int64, 
                              f::Function, g::ObserverList, 
                              x0::State, u::Control, p::Parameters, 
                              mn, on; om_F = "P", om_G = "E") where {State, Parameters, Control, ModellingNoise, ObservationNoise}
    nb_checked = 0
    Threads.@threads for i = 1:N
        so = simulate(f, g, x0, u, p; mn = mn, on = on)
        score = Inf
        if typeof(A) <: RegionAutomatonG_F
            S_end = read_so(A, so; om_F = om_F, om_G = om_G)
            score = S_end.dtotal
        elseif typeof(A) <: RegionAutomatonF_v2
            S_end = read_so(A, so; om = om_F)
            score = S_end.d
        else
            S_end = read_so(A, so; om = om_G)
            score = S_end.d
        end
        if score == 0.0
            nb_checked += 1
        end
    end
    return (nb_checked)
end

function estimate_probability_debug(A::Union{RegionAutomatonF_v2, RegionAutomatonG_v2, 
                              RegionAutomatonG_F}, alpha::Float64, epsilon::Float64, 
                              f::Function, g::ObserverList, 
                              x0::State, u::Control, p::Parameters, 
                              mn, on; om_F = "P", om_G = "E") where {State, Parameters, Control, ModellingNoise, ObservationNoise}
    N = - log(alpha/2) / (2*epsilon^2)
    nb_checked = 0
    @show N
    Threads.@threads for i = 1:N
        so = simulate(f, g, x0, u, p; mn = mn, on = on)
        score = Inf
        is_checked = false
        if typeof(A) <: RegionAutomatonG_F
            S_end = read_so(A, so; x0 = x0, om_F = om_F, om_G = om_G)
            score = S_end.dtotal
            y_time = to_vec(so, "time")
            t_G = [i for i in 1:length(y_time) if A.t1 <= y_time[i] <= A.t2] 
            t_F = [i for i in 1:length(y_time) if A.t3 <= y_time[i] <= A.t4] 
            y_G = to_vec(so, om_G)
            y_F = to_vec(so, om_F)
            max_G, min_G = maximum(y_G[t_G]), minimum(y_G[t_G])
            col_F = vcat(y_F[t_F], y_F[t_G[end]])
            F_zone = [f for f in col_F if A.x3 <= f <= A.x4]
            compared_distance = true 
            dist_F_zone = Inf
            for f in col_F
                d = min(abs(f - A.x3), abs(f - A.x4))
                if (A.x3 <= f <= A.x4)
                    dist_F_zone = 0.0
                    break
                end
                if (f < A.x3 || A.x4 < f) && (dist_F_zone > d) 
                    dist_F_zone = d 
                end
            end
            compared_distance = (dist_F_zone == S_end.dprime || length(t_F) == 0) # on esquive le calcul de distance quand y'a pas de pts ds la zone
            # F formula is OK
            if (length(F_zone) > 0)
                # G formula is OK
                if (max_G <= A.x2 && A.x1 <= min_G)
                    is_checked = true
                end
            end
            #=
        elseif typeof(A) <: RegionAutomatonF_v2
            S_end = read_so(A, so; om_F = om_F)
            score = S_end.d
        else
            S_end = read_so(A, so; om_G = om_G)
            score = S_end.d
            =#
        end
        if (score == 0.0 && !is_checked) || (score > 0.0 && is_checked) || !compared_distance
            redirect_debug_files("prob_estim.log", "prob_estim.err") do
                @show i
                @show compared_distance
                @show is_checked, S_end.dtotal
                @show max_G, min_G
                @show y_F[t_G[end]], y_time[t_G[end]]
                @show S_end.dprime, dist_F_zone
                @show length(t_F), length(t_G)
                @show length(y_F), length(y_G)
                @show y_time[t_G]
                @show y_G[t_G]
                @show y_time[t_F]
                @show y_F[t_F]
                read_so(A, so; x0 = x0, om_F = om_F, om_G = om_G, verbose = true)
                error("WTF")
            end
        end
        if is_checked
            nb_checked += 1
        end
    end
    return (nb_checked / N)
end

function estimate_constant(prob::Float64, x::Float64, vec_p::Array{Float64, 1})
    abc_posterior = kde_lscv(vec_p)
    return (prob / pdf(abc_posterior, x))
end

function estimate_constant(prob::Float64, x::Array{Float64, 1}, mat_p::Array{Float64, 2})
    abc_posterior = kde_lscv(mat_p)
    return (prob / pdf(abc_posterior, x[1], x[2]))
end

