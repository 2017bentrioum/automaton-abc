
const Location = String

mutable struct StateRegionAutomatonF_v1
    loc::Location
    t::Float64
    d::Float64
    n::Float64
    n3::Float64
end

StateRegionAutomatonF_v1() = StateRegionAutomatonF_v1("l0", 0.0, 0.0, 0.0, 0.0)
StateRegionAutomatonF_v1(t::Float64) = StateRegionAutomatonF_v1("l0", t, 0.0, 0.0, 0.0)
Base.copy(S::StateRegionAutomatonF_v1) = StateRegionAutomatonF_v1(S.loc, S.t, S.d, S.n, S.n3)

function Base.show(io::IO, S::StateRegionAutomatonF_v1)
	print(io, "State region automaton \n")
	print(io, "- loc : $(S.loc) \n")
	print(io, "- n : $(S.n) \n")
	print(io, "- time : $(S.t) \n")
	print(io, "- distance : $(S.d)")
end

struct RegionAutomatonF_v1
    x1::Float64
    x2::Float64
    t1::Float64
    t2::Float64
    l_loc::Array{String, 1}
    edges::Dict
    inv_predicate::Dict
end

RegionAutomatonF_v1(x1::Float64, x2::Float64, t1::Float64, t2::Float64) = RegionAutomatonF_v1(x1, x2, t1, t2, region_F_v1_l_loc, region_F_v1_edges, region_F_v1_inv_predicate)

struct Edge
	name_event::String
	transition_predicate::Function
	update_state!::Function
end


## Locations
region_F_v1_l_loc = ["l0", "l1", "l2", "l3"]

## Invariant predicates
region_F_v1_inv_predicate = Dict()
region_F_v1_inv_predicate["l0"] = (A::RegionAutomatonF_v1, S:: StateRegionAutomatonF_v1) -> return true 
region_F_v1_inv_predicate["l1"] = (A::RegionAutomatonF_v1, S:: StateRegionAutomatonF_v1) -> return true 
region_F_v1_inv_predicate["l2"] = (A::RegionAutomatonF_v1, S:: StateRegionAutomatonF_v1) -> return true 
region_F_v1_inv_predicate["l3"] = (A::RegionAutomatonF_v1, S:: StateRegionAutomatonF_v1) -> return true 

# Edges
region_F_v1_edges = Dict()

# l0 loc
tuple = ("l0", "l0")
tp_l0l0_1(A::RegionAutomatonF_v1, S::StateRegionAutomatonF_v1) = S.t < A.t1
us_l0l0_1!(A::RegionAutomatonF_v1, S::StateRegionAutomatonF_v1, P::Float64) = S.loc = "l0"
edge1 = Edge("ALL", tp_l0l0_1, us_l0l0_1!)
region_F_v1_edges[tuple] = [edge1]

tuple = ("l0", "l1")
tp_l0l1_1(A::RegionAutomatonF_v1, S::StateRegionAutomatonF_v1) = S.t >= A.t1
us_l0l1_1!(A::RegionAutomatonF_v1, S::StateRegionAutomatonF_v1, P::Float64) = S.loc = "l1"
edge1 = Edge("#", tp_l0l1_1, us_l0l1_1!)
region_F_v1_edges[tuple] = [edge1]

# l1 loc
tuple = ("l1", "l1")
tp_l1l1_1(A::RegionAutomatonF_v1, S::StateRegionAutomatonF_v1) = S.t < A.t2
us_l1l1_1!(A::RegionAutomatonF_v1, S::StateRegionAutomatonF_v1, P::Float64) = S.loc = "l1"
edge1 = Edge("R1,R2", tp_l1l1_1, us_l1l1_1!)
region_F_v1_edges[tuple] = [edge1]

tuple = ("l1", "l2")
tp_l1l2_1(A::RegionAutomatonF_v1, S::StateRegionAutomatonF_v1) = S.t < A.t2
us_l1l2_1!(A::RegionAutomatonF_v1, S::StateRegionAutomatonF_v1, P::Float64) = (S.n = P; S.n3 += 1; S.loc = "l2")
edge1 = Edge("R3", tp_l1l2_1, us_l1l2_1!)
region_F_v1_edges[tuple] = [edge1]

tuple = ("l1", "l3")
tp_l1l3_1(A::RegionAutomatonF_v1, S::StateRegionAutomatonF_v1) = S.t >= A.t2
us_l1l3_1!(A::RegionAutomatonF_v1, S::StateRegionAutomatonF_v1, P::Float64) = (S.d /= S.n3; S.loc = "l3")
edge1 = Edge("#", tp_l1l3_1, us_l1l3_1!)
tp_l1l3_2(A::RegionAutomatonF_v1, S::StateRegionAutomatonF_v1) = (S.t <= A.t2 && A.x1 <= S.n && S.n <= A.x2)
us_l1l3_2!(A::RegionAutomatonF_v1, S::StateRegionAutomatonF_v1, P::Float64) = (S.d = 0; S.loc = "l3")
edge2 = Edge("#", tp_l1l3_2, us_l1l3_2!)
region_F_v1_edges[tuple] = [edge1, edge2]

# l2 loc
tuple = ("l2", "l1")
tp_l2l1_1(A::RegionAutomatonF_v1, S::StateRegionAutomatonF_v1) = S.n > A.x2
us_l2l1_1!(A::RegionAutomatonF_v1, S::StateRegionAutomatonF_v1, P::Float64) = (S.d += (S.n - A.x2); S.loc = "l1")
edge1 = Edge("#", tp_l2l1_1, us_l2l1_1!)
tp_l2l1_2(A::RegionAutomatonF_v1, S::StateRegionAutomatonF_v1) = S.n < A.x1
us_l2l1_2!(A::RegionAutomatonF_v1, S::StateRegionAutomatonF_v1, P::Float64) = (S.d += (A.x1 - S.n); S.loc = "l1")
edge2 = Edge("#", tp_l2l1_2, us_l2l1_2!)
region_F_v1_edges[tuple] = [edge1, edge2]

#=
tuple = ("l2", "l3")
tp_l2l3_1(A::RegionAutomatonF_v1, S::StateRegionAutomatonF_v1) = (A.x1 <= S.n && S.n <= A.x2)
us_l2l3_1!(A::RegionAutomatonF_v1, S::StateRegionAutomatonF_v1, P::Float64) = (S.d = 0; S.loc = "l3")
edge1 = Edge("#", tp_l2l3_1, us_l2l3_1!)
region_F_v1_edges[tuple] = [edge1]
=#

function next_state(A::RegionAutomatonF_v1, Sn::StateRegionAutomatonF_v1, tnplus1::Float64, Rnplus1::Int64, Pnplus1::Float64; verbose = false)
    edge_candidates = convert(Array{Edge, 1}, [])
    tuple_candidates = convert(Array{Tuple{String, String}, 1}, [])
    Snplus1 = copy(Sn)
    Snplus1.t = tnplus1
    if tnplus1 > A.t2
        Snplus1.t = A.t2
        Rnplus1 = 0
        Pnplus1 = Sn.n    
    end
    first_round = true
    detected_event = (Rnplus1 == 0) ? true : false
    turns = 1
    while first_round || !detected_event || length(edge_candidates) > 0
        edge_candidates = convert(Array{Edge, 1}, [])
        tuple_candidates = convert(Array{Tuple{String, String}, 1}, [])
        current_loc = Snplus1.loc
        if verbose
            @show turns
        end
        # Save all edges that satisfies transition predicate (synchronous or autonomous)
        for loc in A.l_loc
            tuple_edges = (current_loc, loc)
            if haskey(A.edges, tuple_edges)
                for edge in A.edges[tuple_edges]
                    if edge.transition_predicate(A, Snplus1)
                        push!(edge_candidates, edge)
                        push!(tuple_candidates, tuple_edges)
                    end
                end
            end
        end
        # Search the one we must chose
        ind_edge = 0
        for (i, edge) in enumerate(edge_candidates)
            if edge.name_event == "#"
                ind_edge = i
                break
            end
            if first_round || !detected_event
                if ((Rnplus1 != 0 && edge.name_event == "ALL") || ("R$(Rnplus1)" in split(edge.name_event, ",")))
                    detected_event = true
                    ind_edge = i
                end
            end
        end
        # Update the state with the chosen one (if it exists)
        if ind_edge > 0
            edge_candidates[ind_edge].update_state!(A, Snplus1, Pnplus1)
        end
        if verbose
            @show first_round, detected_event
            @show tnplus1, Rnplus1, Pnplus1
            @show ind_edge
            @show edge_candidates
            @show tuple_candidates
            @show Snplus1
        end
        if ind_edge == 0
            return Snplus1
        end
        # Case :
        if turns > 10
            println("Turns, Bad behavior of region2 automaton")
            @show first_round, detected_event
            @show length(edge_candidates)
            @show edge_candidates
            @show tnplus1
            @show Rnplus1
            @show Pnplus1
            @show tp_l0l1_1(A, Snplus1)
            for edge in edge_candidates
                @show edge.transition_predicate(A, Snplus1)
            end
            error("Unpredicted behavior automaton F v2")
        end
        turns += 1
        first_round = false
    end
    return Snplus1
end

function read_so(A::RegionAutomatonF_v1, so::SystemObservation; x0 = nothing, om = "P", verbose = false)
    l_t = to_vec(so, "time")
    l_R = convert(Array{Int64, 1}, to_vec(so, "R"))
    l_P = to_vec(so, om)
    Sn = StateRegionAutomatonF_v2(l_t[1])
    if x0 != nothing
        Sn = next_state(A, Sn, getfield(x0, :time), 0, convert(Float64, getfield(x0, Symbol(om))); verbose = verbose)
    end
    if verbose
        println("(Init) j = 0")
        @show Sn
    end
    for i in 1:length(l_t)
        if verbose
            @show i
        end
        Sn = next_state(A, Sn, l_t[i], l_R[i], l_P[i]; verbose = verbose)
        if Sn.loc == "l3"
            return Sn
        end
    end
    if Sn.loc != "l3"
        @warn "Bad behavior of automaton"
        @show Sn.t, A.t2
        @show Sn.t > A.t2
        @show Sn
        @show to_vec(so, "time")[end]
        @show to_vec(so, om)[end]
    end 
    return Sn
end

