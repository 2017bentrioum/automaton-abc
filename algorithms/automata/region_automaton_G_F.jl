
# Debug tools

include("region_automaton_F_v2.jl")
include("region_automaton_G_v2.jl")

function redirect_debug_files(dofunc, outfile, errfile)
	open(outfile, "w") do out
		open(errfile, "w") do err
			redirect_stdout(out) do
				redirect_stderr(err) do
					dofunc()
				end
			end
		end
	end
end

struct RegionAutomatonG_F
    x1::Float64
    x2::Float64
    x3::Float64
    x4::Float64
    t1::Float64
    t2::Float64
    t3::Float64
    t4::Float64
    l_loc::Array{Location, 1}
    A_G::RegionAutomatonG_v2
    A_F::RegionAutomatonF_v2
end

mutable struct StateRegionAutomatonG_F
    loc::Location
    t::Float64
    tprime::Float64
    d::Float64
    dprime::Float64
    dtotal::Float64
    n::Float64
    in::Bool
end

function Base.show(io::IO, S::StateRegionAutomatonG_F)
	print(io, "State region automaton G F \n")
	print(io, "- loc : $(S.loc) \n")
	print(io, "- n : $(S.n) \n")
	print(io, "- time : $(S.t) \n")
    print(io, "- d (G formula) : $(S.d) \n")
    print(io, "- dprime (F formula) : $(S.dprime) \n")
	print(io, "- dtotal : $(S.dtotal)")
end

StateRegionAutomatonG_F() = StateRegionAutomatonG_F("l0_G", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, true)
StateRegionAutomatonG_F(t::Float64) = StateRegionAutomatonG_F("l0_G", t, 0.0, 0.0, 0.0, 0.0, 0.0, true)

function update_state!(Sn::StateRegionAutomatonG_F, Sn_G::StateRegionAutomatonG_v2)
    Sn.loc = Sn_G.loc * "_G"
    Sn.n = Sn_G.n
    Sn.t = Sn_G.t
    Sn.tprime = Sn_G.tprime
    Sn.d = Sn_G.d
    Sn.in = Sn_G.in
    Sn.dtotal = Sn.d + Sn.dprime
end

function update_state!(Sn::StateRegionAutomatonG_F, Sn_F::StateRegionAutomatonF_v2)
    Sn.loc = Sn_F.loc * "_F"
    Sn.n = Sn_F.n
    Sn.t = Sn_F.t
    Sn.dprime = Sn_F.d
    Sn.dtotal = Sn.d + Sn.dprime
end

region_G_F_l_loc = vcat(["l0", "l1", "l2", "l3", "l4"] .* "_G", ["l1", "l2", "l3"] .* "_F")

function RegionAutomatonG_F(x1::Float64, x2::Float64, x3::Float64, x4::Float64, t1::Float64,  t2::Float64, t3::Float64, t4::Float64)
    if t2 > t3 error("t2 > t3") end
    A_G = RegionAutomatonG_v2(x1, x2, t1, t2)
    A_F = RegionAutomatonF_v2(x3, x4, t3, t4)

    return RegionAutomatonG_F(x1, x2, x3, x4, t1, t2, t3, t4, region_G_F_l_loc, A_G, A_F)
end

global IS_FIRST_DEBUG = true

function get_log_debug(A::RegionAutomatonG_F, str_dir_debug::String, so::SystemObservation, Sn::StateRegionAutomatonG_F)
    if !isdir(str_dir_debug)
        mkdir(str_dir_debug)
    end
    nb_param = rand(1:(2^63-1))
    str_dir_debug_param = str_dir_debug * "p_$(nb_param)/"
    while isdir(str_dir_debug_param)
        nb_param = rand(1:(2^63-1))
        str_dir_debug_param = str_dir_debug * "p_$(nb_param)/"
    end
    mkdir(str_dir_debug_param)
    for om in so.oml
        fig = pygmalion_plot()
        title(string(p_test))
        x = to_vec(so, "time")
        y = to_vec(so, om)
        plt.step(x, y, "ro--", marker="x", linewidth = 1.0)
        current_axis = fig.axes[1]
        if om == om_G
            rect = pa.Rectangle((A.t1,A.x1),(A.t2-A.t1),(A.x2-A.x1),linewidth=1.0,edgecolor="blue",facecolor="none")
            current_axis.add_patch(rect)
        end
        if om == om_F
            rect2 = pa.Rectangle((A.t3,A.x3),(A.t4-A.t3),(A.x4-A.x3),linewidth=1.0,edgecolor="green",facecolor="none")
            current_axis.add_patch(rect2)
        end
        ylim(0, 100)
        xlim(0, 1.01*A.t4)
        savefig(str_dir_debug_param * om * "_sim.eps")
        close()
    end

    redirect_debug_files(str_dir_debug_param * "log", str_dir_debug_param * "err") do
        println("Final state : ")
        @show Sn
    end
end 

function read_so(A::RegionAutomatonG_F, so::SystemObservation; x0 = nothing, om_G = "E", om_F = "P", verbose = false)
    if IS_FIRST_DEBUG
        global STR_DIR_DEBUG = get_last_results_directory() * "p_fail_automaton_G_F_$(myid())_$(Threads.threadid())/"
        global IS_FIRST_DEBUG = false
    end
    l_t = to_vec(so, "time")
    l_reaction = convert(Array{Int64, 1}, to_vec(so, "R"))
    l_E = to_vec(so, om_G)
    l_P = to_vec(so, om_F)
    Sn = StateRegionAutomatonG_F(0.0)
    Sn_G = StateRegionAutomatonG_v2(0.0)
    if x0 != nothing
        Sn_G = next_state(A.A_G, Sn_G, getfield(x0, :time), 0, convert(Float64, getfield(x0, Symbol(om_G))); verbose = verbose)
        update_state!(Sn, Sn_G)
    end
    # G automaton
    if verbose
        println("(Init) j = 0")
        @show Sn
    end
    length_t = length(l_t)
    i = 1
    for j = 1:length_t
        if verbose
            println("j = $j : Before state update")
            @show l_t[j], l_reaction[j], l_E[j]
        end
        Sn_G = next_state(A.A_G, Sn_G, l_t[j], convert(Int, l_reaction[j]), l_E[j]; verbose = verbose)
        update_state!(Sn, Sn_G)
        if verbose
            println("After state update")
            @show Sn
            println()
        end
        if Sn.loc == "l2_G"
            break
        end
        i += 1
    end
    if i > length_t
        get_log_debug(A, STR_DIR_DEBUG, so, Sn)
        @warn "Automaton G in G and F hasn't finished"
        Sn.dtotal = Inf
        return Sn
    end
    # Transition from G to F
    if verbose println("i = $i : Before transition from G to F") end
    # Case : we finished the read of the trajectory because of a stable state
    l_t_im1 = (i == 1) ? getfield(x0, :time) : l_t[i-1]
    l_P_im1 = (i == 1) ? getfield(x0, Symbol(om_F)) : l_P[i-1]
    if i == length_t
        Sn_F = StateRegionAutomatonF_v2(l_t_im1)
        us_l0l1_1!(A.A_F, Sn_F, l_P_im1)
    else
        Sn_F = StateRegionAutomatonF_v2(l_t[i])
        us_l0l1_1!(A.A_F, Sn_F, l_P_im1)
    end
    update_state!(Sn, Sn_F)
    # F automaton
    # Step i without reactions
    # Case : we finished the read of the trajectory because of a stable state
    if i == length_t
        Sn_F = next_state(A.A_F, Sn_F, l_t_im1, 0, l_P_im1; verbose = verbose)
        update_state!(Sn, Sn_F)
    end
    #Sn_F = next_state(A.A_F, Sn_F, l_t[i], 0, l_P[i]; verbose = verbose)
    #update_state!(Sn, Sn_F)
    if verbose
        println("After transition from G to F")
        @show Sn
    end
    for j = i:length_t
        if verbose
            println("j = $j : Before state update")
            @show l_t[j], l_reaction[j], l_E[j]
        end
        Sn_F = next_state(A.A_F, Sn_F, l_t[j], convert(Int, l_reaction[j]), l_P[j]; verbose = verbose)
        update_state!(Sn, Sn_F)
        if verbose
            println("After state update")
            @show Sn
            println()
        end
        if Sn.loc == "l2_F"
            break
        end
    end
    if Sn.loc != "l2_F"
       get_log_debug(A, STR_DIR_DEBUG, so, Sn)
       @warn "The G F automaton hasn't finished"
       Sn.dtotal = Inf
    end
    return Sn
end

#=
function read_so2(A::RegionAutomatonG_F, so::SystemObservation; om_G = "E", om_F = "P", verbose = false)
    if IS_FIRST_DEBUG
        global STR_DIR_DEBUG = get_last_results_directory() * "p_fail_automaton_G_F_$(myid())_$(Threads.threadid())/"
        global IS_FIRST_DEBUG = false
    end
    #=
    l_t = to_vec(so, "time")
    l_reaction = convert(Array{Int64, 1}, to_vec(so, "R"))
    l_E = to_vec(so, om_G)
    l_P = to_vec(so, om_F)
    =#
    Sn = StateRegionAutomatonG_F(so.getfirstvalue("time", 1))
    Sn_G = StateRegionAutomatonG_v2(so.getfirstvalue("time", 1))
    # G automaton
    if verbose
        println("j = 1")
        @show Sn
    end
    length_t = so.getlengthobs("time")
    i = 2
    for j = 2:length_t
        Sn_G = next_state(A.A_G, Sn_G, so.getfirstvalue("time", j), convert(Int, so.getfirstvalue("R", j)), so.getfirstvalue(om_G, j))
        update_state!(Sn, Sn_G)
        if verbose
            @show j
            @show Sn
        end
        if Sn.loc == "l2_G"
            break
        end
        i += 1
    end
    # Transition from G to F
    Sn_F = StateRegionAutomatonF_v2(so.getfirstvalue("time", i))
    us_l0l1_1!(A.A_F, Sn_F, so.getfirstvalue(om_F, i))
    update_state!(Sn, Sn_F)
    if verbose
        println("After transition from F to G")
        @show Sn
    end
    # F automaton
    # Step i without reactions
    Sn_F = next_state(A.A_F, Sn_F, so.getfirstvalue("time", i), 0, so.getfirstvalue(om_F, i))
    update_state!(Sn, Sn_F)
    for j = (i+1):length_t
        if verbose
            @show j
            @show Sn
        end
        Sn_F = next_state(A.A_F, Sn_F, so.getfirstvalue("time", j), convert(Int, so.getfirstvalue("R", j)), so.getfirstvalue(om_F, j))
        update_state!(Sn, Sn_F)
        if Sn.loc == "l2_F"
            break
        end
    end
    if Sn.loc != "l2_F"
        if !isdir(STR_DIR_DEBUG)
            mkdir(STR_DIR_DEBUG)
        end
        nb_param = rand(1:(2^63-1))
        str_dir_debug_param = STR_DIR_DEBUG * "p_$(nb_param)/"
        while isdir(str_dir_debug_param)
            nb_param = rand(1:(2^63-1))
            str_dir_debug_param = STR_DIR_DEBUG * "p_$(nb_param)/"
        end
        mkdir(str_dir_debug_param)
        for om in so.oml
            fig = pygmalion_plot()
            title(string(p_test))
            x = to_vec(so, "time")
            y = to_vec(so, om)
            plt.step(x, y, "ro--", marker="x", linewidth = 1.0)
            current_axis = fig.axes[1]
            if om == om_G
                rect = pa.Rectangle((A.t1,A.x1),(A.t2-A.t1),(A.x2-A.x1),linewidth=1.0,edgecolor="blue",facecolor="none")
                current_axis.add_patch(rect)
            end
            if om == om_F
                rect2 = pa.Rectangle((A.t3,A.x3),(A.t4-A.t3),(A.x4-A.x3),linewidth=1.0,edgecolor="green",facecolor="none")
                current_axis.add_patch(rect2)
            end
            ylim(0, 100)
            xlim(0, 1.01*A.t4)
            savefig(str_dir_debug_param * om * "_sim.eps")
            close()
        end
        
        redirect_debug_files(str_dir_debug_param * "log", str_dir_debug_param * "err") do
            println("Final state : ")
            @show Sn
        end
        println("The G F automaton doesn't finish")
        Sn.dtotal = Inf
    end
    Sn.dtotal = Sn.d + Sn.dprime
    return Sn
end
=#

