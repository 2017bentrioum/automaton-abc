
const Location = String

mutable struct StateRegionAutomatonG_v1
    loc::Location
    t::Float64
    d::Float64
    n::Float64
end

StateRegionAutomatonG_v1() = StateRegionAutomatonG_v1("l0", 0.0, 0.0, 0.0)
StateRegionAutomatonG_v1(t::Float64) = StateRegionAutomatonG_v1("l0", t, 0.0, 0.0)
Base.copy(S::StateRegionAutomatonG_v1) = StateRegionAutomatonG_v1(S.loc, S.t, S.d, S.n)

function Base.show(io::IO, S::StateRegionAutomatonG_v1)
	print(io, "State region automaton \n")
	print(io, "- loc : $(S.loc) \n")
	print(io, "- n : $(S.n) \n")
	print(io, "- time : $(S.t) \n")
	print(io, "- distance : $(S.d)")
end

struct RegionAutomatonG_v1
    x1::Float64
    x2::Float64
    t1::Float64
    t2::Float64
    l_loc::Array{String, 1}
    edges::Dict
    inv_predicate::Dict
end

RegionAutomatonG_v1(x1::Float64, x2::Float64, t1::Float64, t2::Float64) = RegionAutomatonG_v1(x1, x2, t1, t2, region_G_v1_l_loc, region_G_v1_edges, region_G_v1_inv_predicate)

struct Edge
	name_event::String
	transition_predicate::Function
	update_state!::Function
end


## Locations
region_G_v1_l_loc = ["l0", "l2", "l4", "l5"]

## Invariant predicates
region_G_v1_inv_predicate = Dict()
region_G_v1_inv_predicate["l0"] = (A::RegionAutomatonG_v1, S:: StateRegionAutomatonG_v1) -> return true 
region_G_v1_inv_predicate["l2"] = (A::RegionAutomatonG_v1, S:: StateRegionAutomatonG_v1) -> return true 
region_G_v1_inv_predicate["l4"] = (A::RegionAutomatonG_v1, S:: StateRegionAutomatonG_v1) -> return true 
region_G_v1_inv_predicate["l5"] = (A::RegionAutomatonG_v1, S:: StateRegionAutomatonG_v1) -> return true 

# Edges
region_G_v1_edges = Dict()

# l0 loc
tuple = ("l0", "l4")
tp_l0l4_1(A::RegionAutomatonG_v1, S::StateRegionAutomatonG_v1) = true
us_l0l4_1!(A::RegionAutomatonG_v1, S::StateRegionAutomatonG_v1, P::Float64) = (S.loc = "l4"; S.d = 0; S.n = P)
edge1 = Edge("#", tp_l0l4_1, us_l0l4_1!)
region_G_v1_edges[tuple] = [edge1]

# l4 loc
tuple = ("l4", "l5")
tp_l4l5_1(A::RegionAutomatonG_v1, S::StateRegionAutomatonG_v1) = (S.t <= A.t2 && (A.x1 <= S.n <= A.x2))
us_l4l5_1!(A::RegionAutomatonG_v1, S::StateRegionAutomatonG_v1, P::Float64) = (S.loc = "l5")
edge1 = Edge("#", tp_l4l5_1, us_l4l5_1!)
tp_l4l5_2(A::RegionAutomatonG_v1, S::StateRegionAutomatonG_v1) = (S.t <= A.t2 && (S.n < A.x1 || S.n > A.x2))
us_l4l5_2!(A::RegionAutomatonG_v1, S::StateRegionAutomatonG_v1, P::Float64) = (S.loc = "l5"; S.d += min(abs(A.x1 - S.n), abs(A.x1 - S.n)))
edge2 = Edge("#", tp_l4l5_2, us_l4l5_2!)
region_G_v1_edges[tuple] = [edge1, edge2]

tuple = ("l4", "l2")
tp_l4l2_1(A::RegionAutomatonG_v1, S::StateRegionAutomatonG_v1) = (S.t > A.t2)
us_l4l2_1!(A::RegionAutomatonG_v1, S::StateRegionAutomatonG_v1, P::Float64) = (S.loc = "l2")
edge1 = Edge("#", tp_l4l2_1, us_l4l2_1!)
region_G_v1_edges[tuple] = [edge1]

# l5 loc
tuple = ("l5", "l4")
tp_l5l4_1(A::RegionAutomatonG_v1, S::StateRegionAutomatonG_v1) = true
us_l5l4_1!(A::RegionAutomatonG_v1, S::StateRegionAutomatonG_v1, P::Float64) = (S.n = P; S.loc = "l4")
edge1 = Edge("ALL", tp_l5l4_1, us_l5l4_1!)
region_G_v1_edges[tuple] = [edge1]


function next_state(A::RegionAutomatonG_v1, Sn::StateRegionAutomatonG_v1, tnplus1::Float64, Rnplus1::Int64, Pnplus1::Float64; verbose = false)
    edge_candidates = convert(Array{Edge, 1}, [])
    tuple_candidates = convert(Array{Tuple{String, String}, 1}, [])
    Snplus1 = copy(Sn)
    Snplus1.t = tnplus1
    first_round = true
    detected_event = (Rnplus1 == 0) ? true : false
    turns = 1
    while first_round || !detected_event || length(edge_candidates) > 0
        edge_candidates = convert(Array{Edge, 1}, [])
        tuple_candidates = convert(Array{Tuple{String, String}, 1}, [])
        current_loc = Snplus1.loc
        if verbose
            println()
            @show turns
        end
        # Save all edges that satisfies transition predicate (synchronous or autonomous)
        for loc in A.l_loc
            tuple_edges = (current_loc, loc)
            if haskey(A.edges, tuple_edges)
                for edge in A.edges[tuple_edges]
                    if edge.transition_predicate(A, Snplus1)
                        push!(edge_candidates, edge)
                        push!(tuple_candidates, tuple_edges)
                    end
                end
            end
        end
        # Search the one we must chose
        ind_edge = 0
        for (i, edge) in enumerate(edge_candidates)
            if edge.name_event == "#"
                ind_edge = i
                break
            end
            if first_round || !detected_event
                if (edge.name_event == "ALL" || ("R$(Rnplus1)" in split(edge.name_event, ",")))
                    detected_event = true
                    ind_edge = i
                end
            end
        end
        # Update the state with the chosen one (if it exists)
        if ind_edge > 0
            edge_candidates[ind_edge].update_state!(A, Snplus1, Pnplus1)
        end
        if verbose
            @show first_round, detected_event
            @show tnplus1, Rnplus1, Pnplus1
            @show ind_edge
            @show edge_candidates
            @show tuple_candidates
            @show Snplus1
            println()
        end
        if ind_edge == 0
            return Snplus1
        end
        if turns > 10
            println("Tursn, Bad behavior of region_G_v1 automaton")
            @show first_round, detected_event
            @show length(edge_candidates)
            @show edge_candidates
            @show tnplus1
            @show Rnplus1
            @show Pnplus1
            @show tp_l0l1_1(A, Snplus1)
            for edge in edge_candidates
                @show edge.transition_predicate(A, Snplus1)
            end
            exit()
        end
        turns += 1
        first_round = false
    end
    println()
    return Snplus1
end

function read_so(A::RegionAutomatonG_v1, so::SystemObservation; om = "E", verbose = false)
    l_t = to_vec(so, "time")
    l_R = convert(Array{Int64, 1}, to_vec(so, "R"))
    l_P = to_vec(so, om)
    Sn = StateRegionAutomatonG_v1(l_t[1])
    if verbose
        @show Sn
    end
    for i in 2:length(l_t)
        if verbose
            @show i
        end
        Sn = next_state(A, Sn, l_t[i], l_R[i], l_P[i]; verbose = verbose)
        if Sn.loc == "l2"
            return Sn
        end
    end
    if Sn.loc != "l2"
        @warn "Bad behavior of automaton"
        @show Sn.t, A.t2
        @show Sn.t > A.t2
        @show Sn
        @show to_vec(so, "time")[end]
        @show to_vec(so, "P")[end]
        @show to_vec(so, "E")[end]
        @show to_vec(so, "ES")[end]
        @show to_vec(so, "S")[end]
    end 
    return Sn
end

