
const Location = String

mutable struct StateRegionAutomatonG_v2
    loc::Location
    t::Float64
    tprime::Float64
    d::Float64
    n::Float64
    in::Bool
end

StateRegionAutomatonG_v2() = StateRegionAutomatonG_v2("l0", 0.0, 0.0, 0.0, 0.0, true)
StateRegionAutomatonG_v2(t::Float64) = StateRegionAutomatonG_v2("l0", t, 0.0, 0.0, 0.0, true)
Base.copy(S::StateRegionAutomatonG_v2) = StateRegionAutomatonG_v2(S.loc, S.t, S.tprime, S.d, S.n, S.in)

function Base.show(io::IO, S::StateRegionAutomatonG_v2)
	print(io, "State region automaton \n")
	print(io, "- loc : $(S.loc) \n")
	print(io, "- n : $(S.n) \n")
	print(io, "- time : $(S.t) \n")
	print(io, "- time prime : $(S.tprime) \n")
	print(io, "- bool in : $(S.in) \n")
	print(io, "- distance : $(S.d)")
end

struct RegionAutomatonG_v2
    x1::Float64
    x2::Float64
    t1::Float64
    t2::Float64
    l_loc::Array{String, 1}
    edges::Dict
    inv_predicate::Dict
end

RegionAutomatonG_v2(x1::Float64, x2::Float64, t1::Float64, t2::Float64) = RegionAutomatonG_v2(x1, x2, t1, t2, region_G_v2_l_loc, region_G_v2_edges, region_G_v2_inv_predicate)

struct Edge
	name_event::String
	transition_predicate::Function
	update_state!::Function
end


## Locations
region_G_v2_l_loc = ["l0", "l1", "l2", "l3", "l4"]

## Invariant predicates
region_G_v2_inv_predicate = Dict()
region_G_v2_inv_predicate["l0"] = (A::RegionAutomatonG_v2, S:: StateRegionAutomatonG_v2) -> return true 
region_G_v2_inv_predicate["l1"] = (A::RegionAutomatonG_v2, S:: StateRegionAutomatonG_v2) -> return true 
region_G_v2_inv_predicate["l2"] = (A::RegionAutomatonG_v2, S:: StateRegionAutomatonG_v2) -> return true 
region_G_v2_inv_predicate["l3"] = (A::RegionAutomatonG_v2, S:: StateRegionAutomatonG_v2) -> return true 
region_G_v2_inv_predicate["l4"] = (A::RegionAutomatonG_v2, S:: StateRegionAutomatonG_v2) -> return true 

# Edges
region_G_v2_edges = Dict()

# l0 loc
tuple = ("l0", "l1")
tp_l0l1_1(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2) = true
us_l0l1_1!(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2, P::Float64) = (S.loc = "l1"; S.d = 0; S.n = P; S.in = true)
edge1 = Edge("#", tp_l0l1_1, us_l0l1_1!)
region_G_v2_edges[tuple] = [edge1]

# l1 loc
tuple = ("l1", "l3")
tp_l1l3_1(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2) = (S.t < A.t1 && (S.n < A.x1 || S.n > A.x2))
us_l1l3_1!(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2, P::Float64) = (S.loc = "l3"; S.d = min(abs(A.x1 - S.n), abs(A.x2 - S.n)); S.in = false)
edge1 = Edge("#", tp_l1l3_1, us_l1l3_1!)
tp_l1l3_2(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2) = (S.t < A.t1 && (A.x1 <= S.n <= A.x2))
us_l1l3_2!(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2, P::Float64) = (S.loc = "l3"; S.d = 0; S.in = false)
edge2 = Edge("#", tp_l1l3_2, us_l1l3_2!)
tp_l1l3_3(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2) = (!S.in && (A.t1 <= S.t <= A.t2) && (A.x1 <= S.n <= A.x2))
us_l1l3_3!(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2, P::Float64) = (S.loc = "l3"; S.d = S.d * (S.t - A.t1); S.tprime = 0.0)
edge3 = Edge("#", tp_l1l3_3, us_l1l3_3!)
tp_l1l3_4(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2) = (S.in && (A.t1 <= S.t <= A.t2) && (A.x1 <= S.n <= A.x2))
us_l1l3_4!(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2, P::Float64) = (S.loc = "l3"; S.tprime = 0.0)
edge4 = Edge("#", tp_l1l3_4, us_l1l3_4!)
region_G_v2_edges[tuple] = [edge1, edge2, edge3, edge4]

tuple = ("l1", "l4")
tp_l1l4_1(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2) = (!S.in && (A.t1 <= S.t <= A.t2) && (S.n < A.x1 || S.n > A.x2))
us_l1l4_1!(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2, P::Float64) = (S.loc = "l4"; S.d += S.d * (S.t - A.t1))
edge1 = Edge("#", tp_l1l4_1, us_l1l4_1!)
tp_l1l4_2(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2) = (S.in && (A.t1 <= S.t <= A.t2) && (S.n < A.x1 || S.n > A.x2))
us_l1l4_2!(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2, P::Float64) = (S.loc = "l4")
edge2 = Edge("#", tp_l1l4_2, us_l1l4_2!)
region_G_v2_edges[tuple] = [edge1, edge2]

tuple = ("l1", "l2")
tp_l1l2_1(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2) = (S.in && S.t >= A.t2)
us_l1l2_1!(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2, P::Float64) = (S.loc = "l2")
edge1 = Edge("#", tp_l1l2_1, us_l1l2_1!)
tp_l1l2_2(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2) = (!S.in && S.t >= A.t2)
us_l1l2_2!(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2, P::Float64) = (S.loc = "l2"; S.d = S.d * (A.t2 - A.t1))
edge2 = Edge("#", tp_l1l2_2, us_l1l2_2!)
region_G_v2_edges[tuple] = [edge1, edge2]

# l3 loc
tuple = ("l3", "l1")
tp_l3l1_1(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2) = true
us_l3l1_1!(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2, P::Float64) = (S.loc = "l1"; S.n = P)
edge1 = Edge("ALL", tp_l3l1_1, us_l3l1_1!)
region_G_v2_edges[tuple] = [edge1]

tuple = ("l3", "l2")
tp_l3l2_1(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2) = (S.in && S.t >= A.t2)
us_l3l2_1!(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2, P::Float64) = (S.loc = "l2")
edge1 = Edge("#", tp_l3l2_1, us_l3l2_1!)
tp_l3l2_2(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2) = (!S.in && S.t >= A.t2)
us_l3l2_2!(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2, P::Float64) = (S.loc = "l2"; S.d = S.d * (A.t2 - A.t1))
edge2 = Edge("#", tp_l3l2_2, us_l3l2_2!)
region_G_v2_edges[tuple] = [edge1, edge2]

# l4 loc
tuple = ("l4", "l1")
tp_l4l1_1(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2) = true
us_l4l1_1!(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2, P::Float64) = (S.loc = "l1"; S.d += S.tprime * min(abs(A.x1 - S.n), abs(A.x2 - S.n)); S.tprime = 0.0; S.n = P; S.in = true)
edge1 = Edge("ALL", tp_l4l1_1, us_l4l1_1!)
region_G_v2_edges[tuple] = [edge1]

tuple = ("l4", "l2")
tp_l4l2_1(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2) = (S.t >= A.t2)
us_l4l2_1!(A::RegionAutomatonG_v2, S::StateRegionAutomatonG_v2, P::Float64) = (S.loc = "l2"; S.d +=  S.tprime * min(abs(A.x1 - S.n), abs(A.x2 - S.n)); S.tprime = 0.0)
edge1 = Edge("#", tp_l4l2_1, us_l4l2_1!)
region_G_v2_edges[tuple] = [edge1]


function next_state(A::RegionAutomatonG_v2, Sn::StateRegionAutomatonG_v2, tnplus1::Float64, Rnplus1::Int64, Pnplus1::Float64; verbose = false)
    edge_candidates = convert(Array{Edge, 1}, [])
    tuple_candidates = convert(Array{Tuple{String, String}, 1}, [])
    Snplus1 = copy(Sn)
    ## Timer
    Snplus1.t = tnplus1
    if tnplus1 > A.t2
        Snplus1.t = A.t2
        Rnplus1 = 0
        Pnplus1 = Sn.n    
    end
    if Sn.loc == "l4"
        if tnplus1 > A.t2
            Snplus1.tprime = (A.t2 - Sn.t)
        else
            Snplus1.tprime += (tnplus1 - Sn.t)
        end        
    end
    ## End timer
    first_round = true
    detected_event = (Rnplus1 == 0) ? true : false
    turns = 1
    while first_round || !detected_event || length(edge_candidates) > 0
        edge_candidates = convert(Array{Edge, 1}, [])
        tuple_candidates = convert(Array{Tuple{String, String}, 1}, [])
        current_loc = Snplus1.loc
        if verbose
            @show turns
        end
        # Save all edges that satisfies transition predicate (synchronous or autonomous)
        for loc in A.l_loc
            tuple_edges = (current_loc, loc)
            if haskey(A.edges, tuple_edges)
                for edge in A.edges[tuple_edges]
                    if edge.transition_predicate(A, Snplus1)
                        push!(edge_candidates, edge)
                        push!(tuple_candidates, tuple_edges)
                    end
                end
            end
        end
        # Search the one we must chose
        ind_edge = 0
        for (i, edge) in enumerate(edge_candidates)
            if edge.name_event == "#"
                ind_edge = i
                break
            end
            if first_round || !detected_event
                if ((Rnplus1 != 0 && edge.name_event == "ALL") || ("R$(Rnplus1)" in split(edge.name_event, ",")))
                    detected_event = true
                    ind_edge = i
                end
            end
        end
        # Update the state with the chosen one (if it exists)
        if ind_edge > 0
            edge_candidates[ind_edge].update_state!(A, Snplus1, Pnplus1)
        end
        if verbose
            @show first_round, detected_event
            @show tnplus1, Rnplus1, Pnplus1
            @show ind_edge
            @show edge_candidates
            @show tuple_candidates
            @show Snplus1
        end
        if ind_edge == 0
            return Snplus1
        end
        if turns > 10
            @show first_round, detected_event
            @show length(edge_candidates)
            @show edge_candidates
            @show tnplus1
            @show Rnplus1
            @show Pnplus1
            @show tp_l0l1_1(A, Snplus1)
            for edge in edge_candidates
                @show edge.transition_predicate(A, Snplus1)
            end
            error("Unpredicted behavior automaton G v2")
        end
        turns += 1
        first_round = false
    end
    return Snplus1
end

function read_so(A::RegionAutomatonG_v2, so::SystemObservation; x0 = nothing, om = "E", verbose = false)
    l_t = to_vec(so, "time")
    l_R = convert(Array{Int64, 1}, to_vec(so, "R"))
    l_P = to_vec(so, om)
    Sn = StateRegionAutomatonG_v2(0.0)
    if x0 != nothing
        Sn = next_state(A, Sn, getfield(x0, :time), 0, convert(Float64, getfield(x0, Symbol(om))); verbose = verbose)
    end
    if verbose
        println("(Init) j = 0")
        @show Sn
    end
    for i in 1:length(l_t)
        if verbose 
            @show i
        end
        Sn = next_state(A, Sn, l_t[i], l_R[i], l_P[i]; verbose = verbose)
        if Sn.loc == "l2"
            return Sn
        end
    end
    if Sn.loc != "l2"
        @warn "Bad behavior of automaton"
        @show Sn.t, A.t2
        @show Sn.t > A.t2
        @show Sn
        @show to_vec(so, "time")[end]
        @show to_vec(so, om)[end]
    end 
    return Sn
end

