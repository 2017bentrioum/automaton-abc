
@everywhere import StatsBase: cov, ProbabilityWeights
@everywhere import NearestNeighbors: KDTree, knn 
@everywhere using DistributedArrays
@everywhere import Distributions

if VERSION >= v"0.7.0"
    using LinearAlgebra
    using SharedArrays
    import Distributed: @distributed
    import Statistics: mean, median, std, quantile
    using DelimitedFiles
end

include(ENV["PYGMALION_DIR"] * "algorithms/estimation/parallel_abc.jl")

struct ResultAbcPopMc
	p_hat
	vec_stdev::Array{Float64, 2}
	vec_mat_p::Array{Array{Float64, 2}, 1}
	mat_p_end::Array{Float64, 2}
    nbr_sim::Int64
    vec_dist::Array{Float64, 1}
    vec_dist2::Array{Float64, 1}
    epsilon::Float64
    epsilon2::Float64
    weights::Array{Float64, 1}
    l_ess::Array{Float64, 1}
end

struct ConfigurationAbcPopMc
	ll_p::LabelList
	srl_p::SamplingRuleList
	bl_p::BoundList
	nbr_particles::Int64
    eta_function::Union{Label, Function}
	distance_function::Union{Label, Function}
	str_statistical_measure::Label
	l_epsilon::Array{Float64, 1}
	alpha::Float64
    kernel_type::Label
    ll_properties::LabelList
	dict_properties_values::Dict
    full_sim_eta::Bool
    eta_function2::Union{Label, Function}
	l_epsilon2::Array{Float64, 1}
	distance_function2::Union{Label, Function}
end

ConfigurationAbcPopMc(ll_p::LabelList, srl_p::SamplingRuleList, 
					  bl_p::BoundList, nbr_particles::Int64,
                      eta_function::Union{Label, Function}, 
                      distance_function::Union{Label, Function}, 
					  str_statistical_measure::Label, 
					  l_epsilon::Array{Float64, 1}, kernel_type::Label) =
                    ConfigurationAbcPopMc(ll_p, srl_p, 
					  bl_p, nbr_particles,
					  eta_function, distance_function, 
					  str_statistical_measure, l_epsilon, 0.0, kernel_type, 
                      Array{Label,1}(), Dict(), false, "default_eta", [Inf], euclidean_distance)

ConfigurationAbcPopMc(ll_p::LabelList, srl_p::SamplingRuleList, 
					  bl_p::BoundList, nbr_particles::Int64,
                      eta_function::Union{Label, Function}, 
                      distance_function::Union{Label, Function}, 
					  str_statistical_measure::Label, 
                      l_epsilon::Array{Float64, 1}, alpha::Float64,
                      kernel_type::Label) =
                    ConfigurationAbcPopMc(ll_p, srl_p, 
					  bl_p, nbr_particles,
					  eta_function, distance_function, 
                      str_statistical_measure, l_epsilon, alpha, kernel_type, 
                      Array{Label,1}(), Dict(), false, "default_eta", [Inf], euclidean_distance)

ConfigurationAbcPopMc(ll_p::LabelList, srl_p::SamplingRuleList, 
					  bl_p::BoundList, nbr_particles::Int64,
                      eta_function::Union{Label, Function}, 
                      distance_function::Union{Label, Function}, 
					  str_statistical_measure::Label, 
                      l_epsilon::Array{Float64, 1}, alpha::Float64,
                      kernel_type::Label, 
                      eta_function2::Union{Label, Function}, l_epsilon2::Array{Float64, 1},
                      distance_function2) =
                    ConfigurationAbcPopMc(ll_p, srl_p, 
					  bl_p, nbr_particles,
					  eta_function, distance_function, 
                      str_statistical_measure, l_epsilon, alpha, kernel_type, 
                      Array{Label,1}(), Dict(), false, eta_function2, l_epsilon2, distance_function2)

ConfigurationAbcPopMc(ll_p::LabelList, srl_p::SamplingRuleList, 
					  bl_p::BoundList, nbr_particles::Int64,
                      eta_function::Union{Label, Function}, 
                      distance_function::Union{Label, Function}, 
					  str_statistical_measure::Label, 
					  l_epsilon::Array{Float64, 1}, kernel_type::Label,
                      full_sim_eta::Bool) = 
                    ConfigurationAbcPopMc(ll_p, srl_p, 
					  bl_p, nbr_particles,
					  eta_function, distance_function, 
					  str_statistical_measure, l_epsilon, 0.0, kernel_type,
                      Array{Label,1}(), Dict(), full_sim_eta, "default_eta", [Inf], euclidean_distance)

ConfigurationAbcPopMc(ll_p::LabelList, srl_p::SamplingRuleList, 
					  bl_p::BoundList, nbr_particles::Int64,
                      eta_function::Union{Label, Function}, 
                      distance_function::Union{Label, Function}, 
					  str_statistical_measure::Label, 
					  l_epsilon::Array{Float64, 1}, alpha::Float64, kernel_type::Label,
                      full_sim_eta::Bool) = 
                    ConfigurationAbcPopMc(ll_p, srl_p, 
					  bl_p, nbr_particles,
					  eta_function, distance_function, 
					  str_statistical_measure, l_epsilon, alpha, kernel_type, 
                      Array{Label,1}(), Dict(), full_sim_eta, "default_eta", [Inf], euclidean_distance)

ConfigurationAbcPopMc(ll_p::LabelList, srl_p::SamplingRuleList, 
					  bl_p::BoundList, nbr_particles::Int64,
                      eta_function::Union{Label, Function}, 
                      distance_function::Union{Label, Function}, 
					  str_statistical_measure::Label, 
					  l_epsilon::Array{Float64, 1}, kernel_type::Label,
                      ll_properties::LabelList,
                      dict_properties_values::Dict) =  
                    ConfigurationAbcPopMc(ll_p, srl_p, 
					  bl_p, nbr_particles,
					  eta_function, distance_function, 
					  str_statistical_measure, l_epsilon, 0.0, kernel_type, 
                      ll_properties, dict_properties_values, false, "default_eta", [Inf], euclidean_distance)

ConfigurationAbcPopMc(ll_p::LabelList, srl_p::SamplingRuleList, 
					  bl_p::BoundList, nbr_particles::Int64,
                      eta_function::Union{Label, Function}, 
                      distance_function::Union{Label, Function}, 
					  str_statistical_measure::Label, 
					  l_epsilon::Array{Float64, 1}, alpha::Float64, 
                      kernel_type::Label, ll_properties::LabelList,
                      dict_properties_values::Dict) =  
                    ConfigurationAbcPopMc(ll_p, srl_p, 
					  bl_p, nbr_particles,
					  eta_function, distance_function, 
					  str_statistical_measure, l_epsilon, alpha, kernel_type, 
                      ll_properties, dict_properties_values, false, "default_eta", [Inf], euclidean_distance)

function check_cfg(cfg::ConfigurationAbcPopMc)
	check_distance_function(cfg.distance_function)
	check_distance_function(cfg.distance_function2)
	check_eta_function(cfg.eta_function)
	check_eta_function(cfg.eta_function2)
	for i = 1:(length(cfg.l_epsilon)-1)
		if l_epsilon[i] < l_epsilon[i+1] || l_epsilon[i] < 0 || l_epsilon[i+1] < 0
			error("Epsilons must decreasing and be non-negative")
		end
	end
    if cfg.alpha < 0.0 || cfg.alpha > 1.0
        error("Wrong value for alpha")
    end
    if cfg.alpha > 0.0 && length(cfg.l_epsilon) > 2
        error("Choose alpha method or list of epsilons")
    end
	if cfg.nbr_particles < 1
		error("Number of particles must be positive")
	end
end

function abc_pop_mc(f::Function, g::ObserverList, 
                    x0::State, u::Control, p::Parameters, 
                    mn::ModellingNoise, on::ObservationNoise, 
                    so_exp::SystemObservation, cfg::ConfigurationAbcPopMc; 
                    eta_so_exp = nothing, mat_p_init = nothing, save_mat_p_end = true, NT = 0) where
                        {State, Parameters, Control, 
                         ModellingNoise, ObservationNoise}

	file_cfg = open(get_last_results_directory() * "cfg_abc_pmc.txt", "w")
	write(file_cfg, "Number of parameters : $(length(cfg.ll_p)) \n")
	write(file_cfg, "Number of particles : $(cfg.nbr_particles) \n")
	write(file_cfg, "Epsilons : $(cfg.l_epsilon) \n")
	if on != nothing 
        write(file_cfg, "On : $(on) \n")
    else
        write(file_cfg, "No on \n")
    end
    if :dt in fieldnames(Control)
        write(file_cfg, "dt : $(u.dt) \n")
    end
    write(file_cfg, "alpha : $(cfg.alpha) \n")
	write(file_cfg, "ll_p : $(cfg.ll_p) \n")
	write(file_cfg, "srl_p : $(cfg.srl_p) \n")
	write(file_cfg, "kernel type : $(cfg.kernel_type) \n")
	close(file_cfg)
    if NT == 0 cfg_NT = cfg.nbr_particles / 2 else cfg_NT = NT end
	if nprocs() == 1
		println("Begin multi-threaded ABC PMC")
		return par2_abc_pop_mc_threads(f, g, x0, u, p, mn, on, so_exp, cfg; eta_so_exp = eta_so_exp, mat_p_init = mat_p_init, save_mat_p_end = save_mat_p_end, NT = cfg_NT)
	end
	println("Begin distributed ABC PMC")
    return par2_abc_pop_mc_darray(f, g, x0, u, p, mn, on, so_exp, cfg; eta_so_exp = eta_so_exp, mat_p_init = mat_p_init, save_mat_p_end = save_mat_p_end, NT = cfg_NT)
end	

@everywhere function compute_distance(mat_p::Array{Float64,2}, 
                          f::Function, 
                          g_exp::Array{pygmalion.Observer, 1}, 
                          eta_so_exp::Array{Float64, 1}, 
                          x0::State, p::Parameters, u::Control, 
                          mn::ModellingNoise, 
                          on::ObservationNoise, 
                          dist::Function, eta::Function, 
                          srl_p::SamplingRuleList, 
                          bl_p::BoundList, 
                          ll_p::LabelList,
                          full_sim_eta::Bool) where 
                            {State, Parameters, Control, 
                            ModellingNoise, ObservationNoise}
    nbr_particles = size(mat_p)[2]
    vec_dist = zeros(nbr_particles)
    Threads.@threads for i = 1:nbr_particles
        p_i = create_from_subset(p, mat_p[:,i], ll_p)
        if full_sim_eta
            so_sim_i = simulate(f, g_exp, x0, u, p_i; mn = mn, on = on, full_timeline = true)
        else
            so_sim_i = simulate(f, g_exp, x0, u, p_i; mn = mn, on = on)
        end
        vec_dist[i] = dist(eta(so_sim_i), eta_so_exp)
    end
    return vec_dist
end

@everywhere function task_worker!(d_mat_p_t::DArray, d_wl::DArray, 
                                  d_vec_dist::DArray, d_vec_dist2::Union{Nothing, DArray}, 
                                  f::Function, g_exp::Array{pygmalion.Observer, 1}, 
                                  so_exp::SystemObservation, 
                                  eta_so_exp::Array{Float64, 1}, eta_so_exp2::Array{Float64, 1}, 
                                  tml_exp::Array{Int, 1}, 
                                  x0::State, p::Parameters, u::Control, 
                                  mn::ModellingNoise, on::ObservationNoise, 
                                  dist::Function, dist2::Function,
                                  eta::Function, eta2::Function,  
                                  srl_p::SamplingRuleList, bl_p::BoundList, 
                                  ll_p::LabelList,
                                  epsilon::Float64, epsilon2::Float64, ll_properties::LabelList, 
                                  dict_properties_values::Dict, 
                                  wl_old::Array{Float64, 1},
                                  vec_mat_p_old::Array{Float64, 2}, full_sim_eta::Bool,
                                  kernel_type::Label) where 
                                {State, Parameters, Control, 
                                ModellingNoise, ObservationNoise}

    mat_cov = zeros(0, 0)
    inv_sqrt_mat_cov = zeros(0, 0)
    if kernel_type == "mv_normal"
        mat_cov = 2 * cov(vec_mat_p_old, ProbabilityWeights(wl_old), 2; corrected=false)
        if myid() == 2
            @show diag(mat_cov)
            if det(mat_cov) == 0.0
                @show det(mat_cov)
                @show rank(mat_cov)
                @show effective_sample_size(wl_old)
                error("Bad inv mat cov")
            end
        end
        if VERSION >= v"0.7.0" sqrt_mat_cov = sqrt(mat_cov)
        else sqrt_mat_cov = sqrtm(mat_cov)
        end
	    inv_sqrt_mat_cov = inv(sqrt_mat_cov)
    end
    tree_mat_p = KDTree(zeros(1,1))
    if kernel_type == "knn_mv_normal"
        tree_mat_p = KDTree(vec_mat_p_old)
    end
    local_mat = localpart(d_mat_p_t)
    local_wl = localpart(d_wl)
	local_vec_dist = localpart(d_vec_dist)
	local_vec_dist2 = localpart(d_vec_dist2)
    nbr_p = size(local_mat)[1]
    nbr_total_sim = 0
    Threads.@threads  for i = 1:length(local_wl)
        if d_vec_dist2 == nothing
            local_mat[:,i], local_vec_dist[i], _, nbr_sim = update_param(f, g_exp, so_exp, eta_so_exp, eta_so_exp2, tml_exp, x0, p, u, mn, on, dist, dist2, eta, eta2, srl_p, bl_p, ll_p, epsilon, epsilon2, ll_properties, dict_properties_values, tree_mat_p, mat_cov, wl_old, vec_mat_p_old, full_sim_eta, kernel_type)
        else
            local_mat[:,i], local_vec_dist[i], local_vec_dist2[i], nbr_sim = update_param(f, g_exp, so_exp, eta_so_exp, eta_so_exp2, tml_exp, x0, p, u, mn, on, dist, dist2, eta, eta2, srl_p, bl_p, ll_p, epsilon, epsilon2, ll_properties, dict_properties_values, tree_mat_p, mat_cov, wl_old, vec_mat_p_old, full_sim_eta, kernel_type)
        end
        nbr_total_sim += nbr_sim
        local_wl[i] = update_weight(wl_old, vec_mat_p_old, local_mat[:, i], tree_mat_p, inv_sqrt_mat_cov, kernel_type, p, ll_p, srl_p)
    end 
    return nbr_total_sim
end

@everywhere function draw_param_kernel(kernel_type::Label, vec_p_star::Array{Float64, 1},
                                       vec_mat_p_old::Array{Float64, 2}, wl_old::Array{Float64, 1},
                                       mat_cov::Array{Float64, 2}, tree_mat_p::Union{KDTree,Nothing})
    if kernel_type == "mv_normal"
        d_mvnormal = Distributions.MvNormal(vec_p_star, mat_cov)
        return rand(d_mvnormal)
    elseif kernel_type == "knn_mv_normal"
        k = Int(round(0.25 * size(vec_mat_p_old)[2]))
        idxs, dist = knn(tree_mat_p, vec_p_star, k, true)
        knn_mat_cov = 2 * cov(vec_mat_p_old[:,idxs], ProbabilityWeights(wl_old[idxs]), 2; corrected=false)
        d_mvnormal = Distributions.MvNormal(vec_p_star, knn_mat_cov)
        return rand(d_mvnormal)
    else
        error("Unknown specified kernel")
    end
end

@everywhere function update_param(f::Function, 
                                  g_exp::Array{pygmalion.Observer, 1}, 
                                  so_exp::SystemObservation, 
                                  eta_so_exp::Array{Float64, 1}, eta_so_exp2::Array{Float64, 1},
                                  tml_exp::Array{Int, 1}, 
                                  x0::State, p::Parameters, u::Control, 
                                  mn::ModellingNoise, on::ObservationNoise, 
                                  dist::Function, dist2::Function,
                                  eta::Function, eta2::Function,  
                                  srl_p::SamplingRuleList, bl_p::BoundList, 
                                  ll_p::LabelList,
                                  epsilon::Float64, epsilon2::Float64,
                                  ll_properties::LabelList, 
                                  dict_properties_values::Dict,
                                  tree_mat_p::KDTree, 
                                  mat_cov::Array{Float64, 2}, 
                                  wl_old::Array{Float64, 1},
                                  vec_mat_p_old::Array{Float64, 2},
                                  full_sim_eta::Bool, kernel_type::Label) where 
                                    {State, Parameters, Control, 
                                    ModellingNoise, ObservationNoise}
                                    
    d_weights = Distributions.Categorical(wl_old)
    so_sim_full, p_prime = nothing, nothing
    bool_properties = true
    vec_p_prime = nothing
    is_eta2 = (string(eta2) != "default_eta")
    dist_sim = Inf
    dist_sim2 = Inf
    nbr_sim = 0
    while so_sim_full == nothing || dist_sim > epsilon || dist_sim2 > epsilon2 || !bool_properties
        ind_p_star = rand(d_weights)
        vec_p_star = vec_mat_p_old[:,ind_p_star]
        #srl_mv = [SamplingRule(ll_p, "mvnormal", (vec_p_star, mat_cov))]
        #p_prime = sample(p, srl_mv)
        vec_p_prime = draw_param_kernel(kernel_type, vec_p_star, vec_mat_p_old, wl_old, mat_cov, tree_mat_p)
        p_prime = create_from_subset(p, vec_p_prime, ll_p)
        #check_bounds!(p_prime, bl_p)
        if !check_bounds(p_prime, bl_p)
            continue
        end
        so_sim_full = nothing
        eta_so_sim, eta_so_sim2 = nothing, nothing
        try
            so_sim_full = simulate(f, g_exp, x0, u, p_prime; mn = mn, on = on, full_timeline = true)
            if full_sim_eta
                eta_so_sim = eta(so_sim_full)
                eta_so_sim2 = eta2(so_sim_full)
            else
                eta_so_sim = eta(filter(so_sim_full, so_sim_full.oml, tml_exp))
                eta_so_sim2 = eta2(filter(so_sim_full, so_sim_full.oml, tml_exp))
            end
        catch err
            if isa(err, DomainError)
                #@warn "Computation error while simulating the model"
                continue
            end
            throw(err)
        end
        dist_sim = dist(eta_so_sim, eta_so_exp)
        dist_sim2 = is_eta2 ? dist2(eta_so_sim2, eta_so_exp2) : Inf
        bool_properties = check_properties(so_sim_full, ll_properties, dict_properties_values)
        nbr_sim += 1
    end
    vec_p_prime = to_vec(p_prime, ll_p)::Array{Float64, 1}
    return (vec_p_prime, dist_sim, dist_sim2, nbr_sim)
end

@everywhere function update_weight(wl_old::Array{Float64, 1}, 
                                   vec_mat_p_old::Array{Float64, 2}, 
                                   vec_p_current::Array{Float64, 1},
                                   tree_mat_p::KDTree, inv_sqrt_mat_cov::Array{Float64, 2}, 
                                   kernel_type::Label, 
                                   p::Parameters, ll_p::LabelList,
                                   srl_p::SamplingRuleList) where Parameters
    denom = 0.0
    nbr_p = length(ll_p)
    d_normal = Distributions.MvNormal(nbr_p, 1.0)
    for j in 1:length(wl_old)
        if kernel_type == "mv_normal"
            denom += wl_old[j] * Distributions.pdf(d_normal, inv_sqrt_mat_cov * (vec_p_current - vec_mat_p_old[:,j]))::Float64 
            #mat_cov = 2 * cov(vec_mat_p_old, ProbabilityWeights(wl_old), 2; corrected=false)
            #denom += wl_old[j] * Distributions.pdf(d_normal, inv_sqrt_mat_cov * (vec_p_current - vec_mat_p_old[:,j]))::Float64 
        elseif kernel_type == "knn_mv_normal"
            k = Int(round(0.25 * size(vec_mat_p_old)[2]))
            idxs, dist = knn(tree_mat_p, vec_mat_p_old[:,j], k, true)
            knn_mat_cov = 2 * cov(vec_mat_p_old[:,idxs], ProbabilityWeights(wl_old[idxs]), 2; corrected=false)
            denom += wl_old[j] * Distributions.pdf(Distributions.MvNormal(vec_mat_p_old[:,j], knn_mat_cov), vec_p_current)::Float64 
        end
    end
    p_current = create_from_subset(p, vec_p_current, ll_p)::Parameters
    #=
    @show p_current
    @show denom
    @show log_pdf(p_current, srl_p)
    @show exp(log_pdf(p_current, srl_p))
    =#
    return exp(pygmalion.log_pdf(p_current, srl_p)) / denom 
end


function par2_abc_pop_mc_darray(f::Function, g::ObserverList, 
                         x0::State, u::Control, p::Parameters, 
                         mn::ModellingNoise, on::ObservationNoise, 
                         so_exp::SystemObservation, cfg::ConfigurationAbcPopMc; 
                         eta_so_exp = nothing, eta_so_exp2 = nothing, mat_p_init = nothing, save_mat_p_end = true, NT = 0) where 
                                {State, Parameters, Control, 
                                ModellingNoise, ObservationNoise}
    println("ABC PMC with $(nworkers()) processus and $(Threads.nthreads()) threads")
	check_cfg(cfg)
    str_dir = get_last_results_directory()
	# Get distance and summary statistics functions
	dist = distance_function(cfg.distance_function)
	dist2 = distance_function(cfg.distance_function2)
	eta = eta_function(cfg.eta_function)
	eta2 = eta_function(cfg.eta_function2)
    name_eta2 = string(eta2)
    tml = get_merged_timeline(so_exp)
	g_exp = create_observation_function([ObserverModel(join(so_exp.oml,","),collect(tml))])
    eta_so_exp = (eta_so_exp != nothing) ? eta_so_exp : eta(so_exp)
    eta_so_exp2 = (eta_so_exp2 != nothing) ? eta_so_exp2 : eta2(so_exp)
    l_epsilon = cfg.l_epsilon
    l_epsilon2 = cfg.l_epsilon2
    nbr_p = length(cfg.ll_p)
	ll_p = cfg.ll_p
	bl_p = cfg.bl_p
	srl_p = cfg.srl_p
    nbr_particles = cfg.nbr_particles
    ll_properties = cfg.ll_properties
    dict_properties_values = deepcopy(cfg.dict_properties_values)
    if cfg.alpha == 0.0
        nbr_epsilons = length(l_epsilon)
        if VERSION >= v"0.7.0" vec_mat_p = Array{Array{Float64, 2}, 1}(undef, nbr_epsilons)
        else vec_mat_p = Array{Array{Float64, 2}, 1}(nbr_epsilons)
        end
    end
    d_wl_current = dzeros(cfg.nbr_particles)
    d_vec_mat_p_t = distribute(zeros(nbr_p, cfg.nbr_particles); dist=(1, nworkers()))
    full_sim_eta = cfg.full_sim_eta
    kernel_type = cfg.kernel_type

    # Choice of epsilons
    first_epsilon = (cfg.alpha > 0.0 && length(l_epsilon) == 1) ? Inf : l_epsilon[1]
    last_epsilon = (cfg.alpha > 0.0 && length(l_epsilon) == 1) ? l_epsilon[1] : l_epsilon[end]
    first_epsilon2 = (cfg.alpha > 0.0 && length(l_epsilon2) == 1) ? Inf : l_epsilon2[1]
    last_epsilon2 = (cfg.alpha > 0.0 && length(l_epsilon2) == 1) ? l_epsilon2[1] : l_epsilon2[end]
    @show first_epsilon, last_epsilon
    @show first_epsilon2, last_epsilon2
	epsilon = first_epsilon
    epsilon2 = first_epsilon2
    # Init. Iteration 1
    println("Step 1 : Init")
    if mat_p_init == nothing
        cfg_abc = ConfigurationParallelAbc(cfg.ll_p, cfg.srl_p, cfg.bl_p, cfg.nbr_particles, 2^63 - 1, cfg.eta_function, cfg.distance_function, cfg.str_statistical_measure, first_epsilon, cfg.ll_properties, dict_properties_values, cfg.full_sim_eta, cfg.eta_function2, first_epsilon2, cfg.distance_function2)
        if haskey(dict_properties_values, "periodicity")
            dict_properties_values["periodicity"][1] = 1
        end
        println("Init. with simple ABC")
        r_abc = parallel_abc(f, g, x0, u, p, mn, on, so_exp, cfg_abc; eta_so_exp = eta_so_exp, eta_so_exp2 = eta_so_exp2)
        old_mat_p = r_abc.mat_p
        vec_dist = r_abc.vec_dist
        vec_dist2 = r_abc.vec_dist2
    else
        nbr_pa_init = size(mat_p_init)[2]
        old_mat_p = zeros(length(ll_p), nbr_particles)
        nbr_duplicates = div(nbr_particles, nbr_pa_init)
        Threads.@threads for i in 1:nbr_pa_init
            ind_begin = (i-1) * nbr_duplicates + 1
            ind_end = (i == nbr_pa_init) ? nbr_particles : i*nbr_duplicates
            for j in ind_begin:ind_end
                old_mat_p[:,j] = mat_p_init[:,i]
            end
        end
        vec_dist = compute_distance(old_mat_p, f, g_exp, eta_so_exp2, x0, p, u, mn, on, dist, eta, srl_p, bl_p, ll_p, full_sim_eta)
        vec_dist2 = (name_eta2 != "default_eta") ? compute_distance(old_mat_p, f, g_exp, eta_so_exp2, x0, p, u, mn, on, dist, eta, srl_p, bl_p, ll_p, full_sim_eta) : nothing
    end
    if cfg.alpha == 0.0
        vec_mat_p[1] = old_mat_p
    end
    wl_old = [exp(pygmalion.log_pdf(create_from_subset(p, old_mat_p[:,j], ll_p), srl_p)) for j = 1:cfg.nbr_particles]
    normalize!(wl_old, 1)
	d_vec_dist = distribute(vec_dist)
	d_vec_dist2 = (name_eta2 != "default_eta") ? distribute(vec_dist2) : nothing
	ess = effective_sample_size(wl_old)
	@show effective_sample_size(wl_old)
    flush(stdout)
    t = 1
    nbr_tot_sim = r_abc.nbr_sim
    l_ess = zeros(0)
    l_ess = vcat(l_ess, ess)
    old_epsilon = epsilon
    old_epsilon2 = epsilon2
    while epsilon > last_epsilon || epsilon2 > last_epsilon2
        t += 1
        if cfg.alpha == 0.0
		    println("Step $t/$(nbr_epsilons)")
            epsilon = l_epsilon[t]
            epsilon2 = l_epsilon2[t]
        else
            println("Step $t")
			vec_dist = convert(Array, d_vec_dist)
            u_vec_dist = unique(vec_dist)
            if length(u_vec_dist) == 2
                epsilon = quantile(u_vec_dist, cfg.alpha)
            else
                epsilon = quantile(vec_dist, cfg.alpha)
            end
            if d_vec_dist2 != nothing
                epsilon2 = quantile(convert(Array, d_vec_dist2), cfg.alpha)
            end
            if old_epsilon == epsilon
                println("eps == old_eps,  we readjust")
                s_dist = sort(u_vec_dist)
                max_dist = 0.0
                for d in s_dist
                    if (d < epsilon) && (max_dist < d)
                        max_dist = d
                    end
                end
                max_dist = (max_dist == 0) ? 0.9*epsilon : max_dist
                epsilon = max_dist
            end
            @show length(u_vec_dist)
            @show sort(vec_dist)[1:5]
            @show mean(vec_dist), maximum(vec_dist), median(vec_dist), std(vec_dist)
            @show epsilon, epsilon2
            @show nbr_tot_sim
        end
        epsilon = (epsilon < last_epsilon) ? last_epsilon : epsilon
        epsilon2 = (epsilon2 < last_epsilon2) ? last_epsilon2 : epsilon2
		if haskey(dict_properties_values, "periodicity")
		    dict_properties_values["periodicity"][1] = t
        end
        l_nbr_sim = zeros(nworkers()) 
        d_wl_current = dzeros(cfg.nbr_particles)
        d_vec_mat_p_t = distribute(zeros(nbr_p, cfg.nbr_particles); dist=(1, nworkers()))
        @sync for id_w in workers()
            t_id_w = id_w - workers()[1] + 1
            @async l_nbr_sim[t_id_w] = remotecall_fetch(() -> task_worker!(d_vec_mat_p_t, d_wl_current, d_vec_dist, d_vec_dist2, f, g_exp, so_exp, eta_so_exp, eta_so_exp2, tml, x0, p, u, mn, on, dist, dist2, eta, eta2, srl_p, bl_p, ll_p, epsilon, epsilon2, ll_properties, dict_properties_values, wl_old, old_mat_p, full_sim_eta, kernel_type), id_w)
        end
        wl_current = convert(Array, d_wl_current)        
        normalize!(wl_current, 1)
        nbr_tot_sim += sum(l_nbr_sim)
		old_mat_p = convert(Array, d_vec_mat_p_t)
        ess = effective_sample_size(wl_current)
        l_ess = vcat(l_ess, ess)
        @show ess
        if ess < NT
            println("Resampling..")
            d_weights = Distributions.Categorical(Array{Float64, 1}(wl_current))
            ind_w = rand(d_weights, nbr_particles)
            old_mat_p = old_mat_p[:,ind_w]
            wl_current = ones(nbr_particles)
            normalize!(wl_current, 1)
            println("End")
        end
        if cfg.alpha == 0.0
            vec_mat_p[t] = old_mat_p
        end
        println("Computations after current iteration")
        wl_old = wl_current
	    flush(stdout)
        old_epsilon = epsilon
        old_epsilon2 = epsilon2
    end

	mat_p = old_mat_p
	p_hat = create_from_subset(p, get_p_hat(cfg.str_statistical_measure, mat_p), cfg.ll_p)
    vec_mean, vec_stdev = mean_and_stdev(mat_p)
    vec_dist2 = (d_vec_dist2 == nothing) ? zeros(0) : convert(Array, d_vec_dist2)
    if save_mat_p_end
        @show str_dir
        writedlm(str_dir * "weights_end.csv", wl_old, ',')
        writedlm(str_dir * "mat_p_end.csv", old_mat_p, ',')
    end
    if cfg.alpha == 0.0
        r = ResultAbcPopMc(p_hat, vec_stdev, vec_mat_p, vec_mat_p[end], nbr_tot_sim, convert(Array, d_vec_dist), vec_dist2, epsilon, epsilon2, wl_old, l_ess)
    else
        r = ResultAbcPopMc(p_hat, vec_stdev, Array{Array{Float64, 2}, 1}(), old_mat_p, nbr_tot_sim, convert(Array, d_vec_dist), vec_dist2, epsilon, epsilon2, wl_old, l_ess)
    end
    return r
end

function par2_abc_pop_mc_threads(f::Function, g::ObserverList, 
                         x0::State, u::Control, p::Parameters, 
                         mn::ModellingNoise, on::ObservationNoise, 
                         so_exp::SystemObservation, cfg::ConfigurationAbcPopMc; 
                         eta_so_exp = nothing, eta_so_exp2 = nothing, mat_p_init = nothing, save_mat_p_end = true, NT = 0) where 
                                {State, Parameters, Control, 
                                ModellingNoise, ObservationNoise}
    println("ABC PMC with $(nworkers()) processus and $(Threads.nthreads()) threads")
	check_cfg(cfg)
    str_dir = get_last_results_directory()
	# Get distance and summary statistics functions
	dist = distance_function(cfg.distance_function)
	dist2 = distance_function(cfg.distance_function2)
	eta = eta_function(cfg.eta_function)
	eta2 = eta_function(cfg.eta_function2)
    name_eta2 = string(eta2)
    tml = get_merged_timeline(so_exp)
	g_exp = create_observation_function([ObserverModel(join(so_exp.oml,","),collect(tml))])
    eta_so_exp = (eta_so_exp != nothing) ? eta_so_exp : eta(so_exp)
    eta_so_exp2 = (eta_so_exp2 != nothing) ? eta_so_exp2 : eta2(so_exp)
    l_epsilon = cfg.l_epsilon
    l_epsilon2 = cfg.l_epsilon2
    nbr_p = length(cfg.ll_p)
	ll_p = cfg.ll_p
	bl_p = cfg.bl_p
	srl_p = cfg.srl_p
    nbr_particles = cfg.nbr_particles
    ll_properties = cfg.ll_properties
    dict_properties_values = deepcopy(cfg.dict_properties_values)
    if cfg.alpha == 0.0
        nbr_epsilons = length(l_epsilon)
        if VERSION >= v"0.7.0" vec_mat_p = Array{Array{Float64, 2}, 1}(undef, nbr_epsilons)
        else vec_mat_p = Array{Array{Float64, 2}, 1}(nbr_epsilons)
        end
    end
	vec_mat_p_t = zeros(nbr_p, cfg.nbr_particles)
    full_sim_eta = cfg.full_sim_eta
    kernel_type = cfg.kernel_type

    # Choice of epsilons
    first_epsilon = (cfg.alpha > 0.0 && length(l_epsilon) == 1) ? Inf : l_epsilon[1]
    last_epsilon = (cfg.alpha > 0.0 && length(l_epsilon) == 1) ? l_epsilon[1] : l_epsilon[end]
    first_epsilon2 = (cfg.alpha > 0.0 && length(l_epsilon2) == 1) ? Inf : l_epsilon2[1]
    last_epsilon2 = (cfg.alpha > 0.0 && length(l_epsilon2) == 1) ? l_epsilon2[1] : l_epsilon2[end]
    @show first_epsilon, last_epsilon
    @show first_epsilon2, last_epsilon2
	epsilon = first_epsilon
    epsilon2 = first_epsilon2
   
    old_epsilon = epsilon
    old_epsilon2 = epsilon2
    # Init. Iteration 1
    println("Step 1 : Init")
    if mat_p_init == nothing
        cfg_abc = ConfigurationParallelAbc(cfg.ll_p, cfg.srl_p, cfg.bl_p, cfg.nbr_particles, 2^63 - 1, cfg.eta_function, cfg.distance_function, cfg.str_statistical_measure, first_epsilon, cfg.ll_properties, dict_properties_values, cfg.full_sim_eta, cfg.eta_function2, first_epsilon2, cfg.distance_function2)
        if haskey(dict_properties_values, "periodicity")
            dict_properties_values["periodicity"][1] = 1
        end
        r_abc = parallel_abc(f, g, x0, u, p, mn, on, so_exp, cfg_abc; eta_so_exp = eta_so_exp, eta_so_exp2 = eta_so_exp2)
        old_mat_p = r_abc.mat_p
        vec_dist = r_abc.vec_dist
        vec_dist2 = (length(r_abc.vec_dist2) > 0) ? r_abc.vec_dist2 : nothing  
    else
        nbr_pa_init = size(mat_p_init)[2]
        old_mat_p = zeros(length(ll_p), nbr_particles)
        nbr_duplicates = div(nbr_particles, nbr_pa_init)
        Threads.@threads for i in 1:nbr_pa_init
            ind_begin = (i-1) * nbr_duplicates + 1
            ind_end = (i == nbr_pa_init) ? nbr_particles : i*nbr_duplicates
            for j in ind_begin:ind_end
                old_mat_p[:,j] = mat_p_init[:,i]
            end
        end
        vec_dist = compute_distance(old_mat_p, f, g_exp, eta_so_exp2, x0, p, u, mn, on, dist, eta, srl_p, bl_p, ll_p, full_sim_eta)
        vec_dist2 = (name_eta2 != "default_eta") ? compute_distance(old_mat_p, f, g_exp, eta_so_exp2, x0, p, u, mn, on, dist, eta, srl_p, bl_p, ll_p, full_sim_eta) : nothing
    end
    wl_old = [exp(pygmalion.log_pdf(create_from_subset(p, old_mat_p[:,j], ll_p), srl_p)) for j = 1:cfg.nbr_particles]
    normalize!(wl_old, 1)
    if cfg.alpha == 0.0
        vec_mat_p[1] = old_mat_p
    end
    @show effective_sample_size(wl_old)
    
    ess = effective_sample_size(wl_old)
    l_ess = zeros(0)
    l_ess = vcat(l_ess, ess)
    
    flush(stdout)
    t = 1
    nbr_tot_sim = r_abc.nbr_sim
	while epsilon > last_epsilon || epsilon2 > last_epsilon2
        t += 1
        # Compute epsilons
        if cfg.alpha == 0.0
		    println("Step $t/$(nbr_epsilons)")
            epsilon = l_epsilon[t]
            epsilon2 = l_epsilon2[t]
        else
            println("Step $t")
            u_vec_dist = unique(vec_dist)
            if length(u_vec_dist) == 2
                epsilon = quantile(u_vec_dist, cfg.alpha)
            else
                epsilon = quantile(vec_dist, cfg.alpha)
            end
            if vec_dist2 != nothing
                epsilon2 = quantile(vec_dist2, cfg.alpha)
            end
            if old_epsilon == epsilon
                print("eps == old_eps,  we readjust")
                s_dist = sort(u_vec_dist)
                max_dist = 0.0
                for d in s_dist
                    if (d < epsilon) && (max_dist < d)
                        max_dist = d
                    end
                end
                max_dist = (max_dist == 0) ? 0.9*epsilon : max_dist
                epsilon = max_dist
            end
            @show sort(vec_dist)[1:5]
            @show mean(vec_dist), maximum(vec_dist), median(vec_dist), std(vec_dist)
        end
        epsilon = (epsilon < last_epsilon) ? last_epsilon : epsilon
        epsilon2 = (epsilon2 < last_epsilon2) ? last_epsilon2 : epsilon2
		if haskey(dict_properties_values, "periodicity")
		    dict_properties_values["periodicity"][1] = t
        end
        @show epsilon, epsilon2
        @show nbr_tot_sim
        # Broadcast simulations
        l_nbr_sim = zeros(nbr_particles) 
        wl_current = zeros(cfg.nbr_particles)
        mat_cov = zeros(0, 0)
        inv_sqrt_mat_cov = zeros(0, 0)
        if kernel_type == "mv_normal"
            mat_cov = 2 * cov(old_mat_p, ProbabilityWeights(wl_old), 2; corrected=false)
            @show diag(mat_cov)
            if det(mat_cov) == 0.0
                @show det(mat_cov)
                @show rank(mat_cov)
                @show effective_sample_size(wl_old)
                error("Bad inv mat cov")
            end
            if VERSION >= v"0.7.0" sqrt_mat_cov = sqrt(mat_cov)
            else sqrt_mat_cov = sqrtm(mat_cov)
            end
            inv_sqrt_mat_cov = inv(sqrt_mat_cov)
        end
        tree_mat_p = KDTree(zeros(1,1))
        if kernel_type == "knn_mv_normal"
            tree_mat_p = KDTree(old_mat_p)
        end
        Threads.@threads for i = 1:nbr_particles
            if string(eta2) == "default_eta"
                vec_mat_p_t[:,i], vec_dist[i], _, l_nbr_sim[i] = update_param(f, g_exp, so_exp, eta_so_exp, eta_so_exp2, tml, x0, p, u, mn, on, dist, dist2, eta, eta2, srl_p, bl_p, ll_p, epsilon, epsilon2, ll_properties, dict_properties_values, tree_mat_p, mat_cov, wl_old, old_mat_p, full_sim_eta, kernel_type)
            else
                vec_mat_p_t[:,i], vec_dist[i], vec_dist2[i], l_nbr_sim[i] = update_param(f, g_exp, so_exp, eta_so_exp, eta_so_exp2, tml, x0, p, u, mn, on, dist, dist2, eta, eta2, srl_p, bl_p, ll_p, epsilon, epsilon2, ll_properties, dict_properties_values, tree_mat_p, mat_cov, wl_old, old_mat_p, full_sim_eta, kernel_type)
            end
            wl_current[i] = update_weight(wl_old, old_mat_p, vec_mat_p_t[:,i], tree_mat_p, inv_sqrt_mat_cov, kernel_type, p, ll_p, srl_p)
        end
        normalize!(wl_current, 1)
        nbr_tot_sim += sum(l_nbr_sim)
        old_mat_p = vec_mat_p_t
        ess = effective_sample_size(wl_current)
        @show ess
        l_ess = vcat(l_ess, ess)
        if ess < NT
            println("Resampling..")
            d_weights = Distributions.Categorical(Array{Float64, 1}(wl_current))
            ind_w = rand(d_weights, nbr_particles)
            old_mat_p = old_mat_p[:,ind_w]
            wl_current = ones(nbr_particles)
            normalize!(wl_current, 1)
            println("End")
        end
        if cfg.alpha == 0.0
            vec_mat_p[t] = old_mat_p
        end
        println("Computations after current iteration")
        wl_old = copy(wl_current)
        flush(stdout)
        old_epsilon = epsilon
        old_epsilon2 = epsilon2
	end

	mat_p = old_mat_p
	p_hat = create_from_subset(p, get_p_hat(cfg.str_statistical_measure, mat_p), cfg.ll_p)
    vec_mean, vec_stdev = mean_and_stdev(mat_p)
    vec_dist2 = (vec_dist2 == nothing) ? zeros(0) : vec_dist2
    if save_mat_p_end
        writedlm(str_dir * "weights_end.csv", wl_old, ',')
        writedlm(str_dir * "mat_p_end.csv", old_mat_p, ',')
    end
    if cfg.alpha == 0.0
        r = ResultAbcPopMc(p_hat, vec_stdev, vec_mat_p, vec_mat_p[end], nbr_tot_sim, vec_dist, vec_dist2, epsilon, epsilon2, wl_old, l_ess)
    else
        r = ResultAbcPopMc(p_hat, vec_stdev, Array{Array{Float64, 2}, 1}(), old_mat_p, nbr_tot_sim, vec_dist, vec_dist2, epsilon, epsilon2, wl_old, l_ess)
    end
    return r
end


## Parallel version without distributed arrays, comparable performances

#=
@everywhere function task_worker(f::Function, g_exp::Array{pygmalion.Observer, 1}, 
                                  so_exp::SystemObservation, 
                                  eta_so_exp::Array{Float64, 1}, eta_so_exp2::Array{Float64, 1}, 
                                  tml_exp::Array{Int, 1}, 
                                  x0::State, p::Parameters, u::Control, 
                                  mn::ModellingNoise, on::ObservationNoise, nbr_pa::Int64,  
                                  dist::Function, dist2::Function,
                                  eta::Function, eta2::Function,  
                                  srl_p::SamplingRuleList, bl_p::BoundList, 
                                  ll_p::LabelList,
                                  epsilon::Float64, epsilon2::Float64, ll_properties::LabelList, 
                                  dict_properties_values::Dict, 
                                  wl_old::Array{Float64, 1},
                                  vec_mat_p_old::Array{Float64, 2}, full_sim_eta::Bool,
                                  kernel_type::Label)  where 
                                                       {State, Parameters, Control, 
                                                       ModellingNoise, ObservationNoise}
    mat_cov = zeros(0, 0)
    inv_sqrt_mat_cov = zeros(0, 0)
    if kernel_type == "mv_normal"
        mat_cov = 2 * cov(vec_mat_p_old, ProbabilityWeights(wl_old), 2; corrected=false)
        if myid() == 2
			@show diag(mat_cov)
        end
        if det(mat_cov) == 0.0
            @show det(mat_cov)
            @show rank(mat_cov)
            @show effective_sample_size(wl_old)
            error("Bad inv mat cov")
        end
        if VERSION >= v"0.7.0" sqrt_mat_cov = sqrt(mat_cov)
        else sqrt_mat_cov = sqrtm(mat_cov)
        end
	    inv_sqrt_mat_cov = inv(sqrt_mat_cov)
    end
    tree_mat_p = KDTree(zeros(1,1))
    if kernel_type == "knn_mv_normal"
        tree_mat_p = KDTree(vec_mat_p_old)
    end
    nbr_p = length(ll_p)
    local_mat = zeros(nbr_p, nbr_pa) 
    local_wl = zeros(nbr_pa)
    local_vec_dist = zeros(nbr_pa)
    local_vec_dist2 = zeros(nbr_pa)
    nbr_total_sim = 0
    for i = 1:nbr_pa
        if string(eta2) == "default_eta"
            local_mat[:,i], local_vec_dist[i], _, nbr_sim = update_param(f, g_exp, so_exp, eta_so_exp, eta_so_exp2, tml_exp, x0, p, u, mn, on, dist, dist2, eta, eta2, srl_p, bl_p, ll_p, epsilon, epsilon2, ll_properties, dict_properties_values, tree_mat_p, mat_cov, wl_old, vec_mat_p_old, full_sim_eta, kernel_type)
        else
            local_mat[:,i], local_vec_dist[i], local_vec_dist2[i], nbr_sim = update_param(f, g_exp, so_exp, eta_so_exp, eta_so_exp2, tml_exp, x0, p, u, mn, on, dist, dist2, eta, eta2, srl_p, bl_p, ll_p, epsilon, epsilon2, ll_properties, dict_properties_values, tree_mat_p, mat_cov, wl_old, vec_mat_p_old, full_sim_eta, kernel_type)
        end
        nbr_total_sim += nbr_sim
        local_wl[i] = update_weight(wl_old, vec_mat_p_old, local_mat[:, i], tree_mat_p, inv_sqrt_mat_cov, kernel_type, p, ll_p, srl_p)
    end
    return (local_mat, local_wl, local_vec_dist, local_vec_dist2, nbr_total_sim)
end

function par2_abc_pop_mc(f::Function, g::ObserverList, 
                         x0::State, u::Control, p::Parameters, 
                         mn::ModellingNoise, on::ObservationNoise, 
                         so_exp::SystemObservation, cfg::ConfigurationAbcPopMc; 
                         eta_so_exp = nothing, eta_so_exp2 = nothing, mat_p_init = nothing) where 
                                {State, Parameters, Control, 
                                ModellingNoise, ObservationNoise}
    println("ABC PMC with $(nworkers()) processus and $(Threads.nthreads()) threads")
	check_cfg(cfg)
	# Get distance and summary statistics functions
	dist = distance_function(cfg.distance_function)
	dist2 = distance_function(cfg.distance_function2)
	eta = eta_function(cfg.eta_function)
	eta2 = eta_function(cfg.eta_function2)
    name_eta2 = string(eta2)
    tml = get_merged_timeline(so_exp)
	g_exp = create_observation_function([ObserverModel(join(so_exp.oml,","),collect(tml))])
    eta_so_exp = (eta_so_exp != nothing) ? eta_so_exp : eta(so_exp)
    eta_so_exp2 = (eta_so_exp2 != nothing) ? eta_so_exp2 : eta2(so_exp)
    l_epsilon = cfg.l_epsilon
    l_epsilon2 = cfg.l_epsilon2
    nbr_p = length(cfg.ll_p)
	ll_p = cfg.ll_p
	bl_p = cfg.bl_p
	srl_p = cfg.srl_p
    nbr_particles = cfg.nbr_particles
    ll_properties = cfg.ll_properties
    dict_properties_values = deepcopy(cfg.dict_properties_values)
    if cfg.alpha == 0.0
        nbr_epsilons = length(l_epsilon)
        if VERSION >= v"0.7.0" vec_mat_p = Array{Array{Float64, 2}, 1}(undef, nbr_epsilons)
        else vec_mat_p = Array{Array{Float64, 2}, 1}(nbr_epsilons)
        end
    end
    wl_old = ones(cfg.nbr_particles)
    normalize!(wl_old, 1)
    wl_current = zeros(cfg.nbr_particles)
	vec_mat_p_t = zeros(nbr_p, cfg.nbr_particles)
    full_sim_eta = cfg.full_sim_eta
    kernel_type = cfg.kernel_type

    # Choice of epsilons
    first_epsilon = (cfg.alpha > 0.0 && length(l_epsilon) == 1) ? Inf : l_epsilon[1]
    last_epsilon = (cfg.alpha > 0.0 && length(l_epsilon) == 1) ? l_epsilon[1] : l_epsilon[end]
    first_epsilon2 = (cfg.alpha > 0.0 && length(l_epsilon2) == 1) ? Inf : l_epsilon2[1]
    last_epsilon2 = (cfg.alpha > 0.0 && length(l_epsilon2) == 1) ? l_epsilon2[1] : l_epsilon2[end]
    @show first_epsilon, last_epsilon
    @show first_epsilon2, last_epsilon2
    @show eta_so_exp, eta_so_exp2
	epsilon = first_epsilon
    epsilon2 = first_epsilon2
    
    # Init. Iteration 1
    if mat_p_init == nothing
        cfg_abc = ConfigurationParallelAbc(cfg.ll_p, cfg.srl_p, cfg.bl_p, cfg.nbr_particles, 2^63 - 1, cfg.eta_function, cfg.distance_function, cfg.str_statistical_measure, first_epsilon, cfg.ll_properties, dict_properties_values, cfg.full_sim_eta, cfg.eta_function2, first_epsilon2, cfg.distance_function2)
        if haskey(dict_properties_values, "periodicity")
            dict_properties_values["periodicity"][1] = 1
        end
        r_abc = parallel_abc(f, g, x0, u, p, mn, on, so_exp, cfg_abc; eta_so_exp = eta_so_exp, eta_so_exp2 = eta_so_exp2)
        old_mat_p = r_abc.mat_p
    else
        nbr_pa_init = size(mat_p_init)[2]
        old_mat_p = zeros(length(ll_p), nbr_particles)
        nbr_duplicates = div(nbr_particles, nbr_pa_init)
        for i in 1:nbr_pa_init
            ind_begin = (i-1) * nbr_duplicates + 1
            ind_end = (i == nbr_pa_init) ? nbr_particles : i*nbr_duplicates
            for j in ind_begin:ind_end
                old_mat_p[:,j] = mat_p_init[:,i]
            end
        end
    end
    if cfg.alpha == 0.0
        vec_mat_p[1] = old_mat_p
    end
	vec_dist = compute_distance(old_mat_p, f, g_exp, eta_so_exp, x0, p, u, mn, on, dist, eta, srl_p, bl_p, ll_p, full_sim_eta)
	vec_dist2 = nothing
    @show name_eta2
    if name_eta2 != "default_eta"
        vec_dist2 = compute_distance(old_mat_p, f, g_exp, eta_so_exp2, x0, p, u, mn, on, dist2, eta2, srl_p, bl_p, ll_p, full_sim_eta)
    end
	@show effective_sample_size(wl_old)
    flush(stdout)
    t = 1
    nbr_tot_sim = r_abc.nbr_sim
	while epsilon > last_epsilon || epsilon2 > last_epsilon2
        t += 1
        if cfg.alpha == 0.0
		    println("Step " * string(t) * "/" * string(nbr_epsilons))
            epsilon = l_epsilon[t]
            epsilon2 = l_epsilon2[t]
        else
            epsilon = quantile(vec_dist, cfg.alpha)
            if vec_dist2 != nothing
                epsilon2 = quantile(vec_dist2, cfg.alpha)
            end
            @show mean(vec_dist), maximum(vec_dist), median(vec_dist), std(vec_dist)
            @show epsilon, epsilon2
        end
        epsilon = (epsilon < last_epsilon) ? last_epsilon : epsilon
        epsilon2 = (epsilon2 < last_epsilon2) ? last_epsilon2 : epsilon2
		if haskey(dict_properties_values, "periodicity")
		    dict_properties_values["periodicity"][1] = t
        end
        l_nbr_sim = zeros(nworkers()) 
        @sync for id_w in workers()
            t_id_w = id_w - workers()[1] + 1
            ind_begin = (t_id_w-1) * div(nbr_particles, nworkers()) + 1
            ind_end = t_id_w * div(nbr_particles, nworkers())
            if id_w == workers()[end]
                ind_end = nbr_particles
            end
            nbr_pa_per_worker = ind_end - ind_begin + 1
            if vec_dist2 == nothing
                @async vec_mat_p_t[:,ind_begin:ind_end], wl_current[ind_begin:ind_end], vec_dist[ind_begin:ind_end], _, l_nbr_sim[t_id_w] = remotecall_fetch(() -> task_worker(f, g_exp, so_exp, eta_so_exp, eta_so_exp2, tml, x0, p, u, mn, on, nbr_pa_per_worker, dist, dist2, eta, eta2, srl_p, bl_p, ll_p, epsilon, epsilon2, ll_properties, dict_properties_values, wl_old, old_mat_p, full_sim_eta, kernel_type), id_w)
            else
                @async vec_mat_p_t[:,ind_begin:ind_end], wl_current[ind_begin:ind_end], vec_dist[ind_begin:ind_end], vec_dist2[ind_begin:ind_end], l_nbr_sim[t_id_w] = remotecall_fetch(() -> task_worker(f, g_exp, so_exp, eta_so_exp, eta_so_exp2, tml, x0, p, u, mn, on, nbr_pa_per_worker, dist, dist2, eta, eta2, srl_p, bl_p, ll_p, epsilon, epsilon2, ll_properties, dict_properties_values, wl_old, old_mat_p, full_sim_eta, kernel_type), id_w)
            end
        end
        normalize!(wl_current, 1)
        nbr_tot_sim += sum(l_nbr_sim)
        old_mat_p = vec_mat_p_t
        if cfg.alpha == 0.0
            vec_mat_p[t] = old_mat_p
        end
        println("Computations after current iteration")
        @show effective_sample_size(wl_current)
        wl_old = wl_current
        flush(stdout)
	end

	mat_p = old_mat_p
	p_hat = create_from_subset(p, get_p_hat(cfg.str_statistical_measure, mat_p), cfg.ll_p)
    vec_mean, vec_stdev = mean_and_stdev(mat_p)
    vec_dist2 = (vec_dist2 == nothing) ? zeros(0) : vec_dist2
    if cfg.alpha == 0.0
        r = ResultAbcPopMc(p_hat, vec_stdev, vec_mat_p, vec_mat_p[end], nbr_tot_sim, vec_dist, vec_dist2, epsilon, epsilon2)
    else
        r = ResultAbcPopMc(p_hat, vec_stdev, Array{Array{Float64, 2}, 1}(), old_mat_p, nbr_tot_sim, vec_dist, vec_dist2, epsilon, epsilon2)
    end
    return r
end
=#


