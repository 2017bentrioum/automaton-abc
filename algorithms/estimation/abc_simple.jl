
include(ENV["PYGMALION_DIR"] * "algorithms/estimation/helpers/metrics.jl")

macro passdomainerr(expr)
    quote
        try
            $expr
        catch err
            error(err)
            if isa(err, DomainError)
                continue
            end
        end
    end
end

struct ResultAbc
	p_hat
	vec_stdev::Array{Float64, 2}
	mat_p::Array{Float64, 2}
	ratio_accept::Float64
	nbr_size_sample::Int64
end

struct ConfigurationAbc
	ll_p::LabelList
	srl_p::SamplingRuleList
	bl_p::BoundList
	size_sample::Int64
	nbr_it_max::Int64
    eta_function::Union{Label, Function}
	distance_function::Union{Label, Function}
	str_statistical_measure::Label
	epsilon::Float64
	ll_properties::LabelList
	dict_properties_values::Dict
    full_sim_eta::Bool
end

ConfigurationAbc(ll_p::LabelList, srl_p::SamplingRuleList, 
                 bl_p::BoundList, size_sample::Int64, nbr_it_max::Int64, 
                 eta_function::Union{Label, Function}, distance_function::Union{Label, Function},
                 str_statistical_measure::Label, epsilon::Float64) =
ConfigurationAbc(ll_p, srl_p, bl_p, size_sample, nbr_it_max, eta_function,
                 distance_function, str_statistical_measure, epsilon, Array{Label,1}(), Dict(), false)


ConfigurationAbc(ll_p::LabelList, srl_p::SamplingRuleList, 
                 bl_p::BoundList, size_sample::Int64, nbr_it_max::Int64, 
                 eta_function::Union{Label, Function}, distance_function::Union{Label, Function},
                 str_statistical_measure::Label, epsilon::Float64,
                full_sim_eta::Bool) =
ConfigurationAbc(ll_p, srl_p, bl_p, size_sample, nbr_it_max, eta_function,
                 distance_function, str_statistical_measure, epsilon, Array{Label,1}(), Dict(), full_sim_eta)

ConfigurationAbc(ll_p::LabelList, srl_p::SamplingRuleList, 
                         bl_p::BoundList, size_sample::Int64, nbr_it_max::Int64, 
                         eta_function::Union{Label, Function}, distance_function::Union{Label, Function},
                         str_statistical_measure::Label, epsilon::Float64,
                         ll_properties::LabelList, dict_properties_values::Dict) =
ConfigurationAbc(ll_p, srl_p, bl_p, size_sample, nbr_it_max, eta_function,
                         distance_function, str_statistical_measure, epsilon, ll_properties, dict_properties_values, false)



function mode(vec::Vector{Float64})
	d = Dict()
	res = 0.0
	for k in vec
		if haskey(d, k)
			d[k] += 1
		else
			d[k] = 1
		end
	end
	max_counts = maximum(collect(values(d)))
	for k in keys(d)
		if d[k] == max_counts
			res = k
			break
		end
	end
	return res
end

function get_p_hat(str_sm::Label, mat_p)
	nb_p = size(mat_p)[1]
	vec_p = zeros(nb_p)
	if str_sm == "mean"
		for i = 1:nb_p
			vec_p[i] = mean(mat_p[i, :])
		end
	elseif str_sm == "mode"
		for i = 1:nb_p
			vec_p[i] = mode(mat_p[i, :])
		end
	else	
		error("Wrong name of statistical measure")
	end
	return vec_p
end

function check_cfg(cfg::ConfigurationAbc)
	check_distance_function(cfg.distance_function)
	check_eta_function(cfg.eta_function)
	if cfg.epsilon < 0.0
		error("Epsilon must be non-negative")
	end
	if cfg.nbr_it_max < cfg.size_sample
		error("Max number of iterations can't be less than the desired size sample")
	end
	if cfg.size_sample <= 0
		error("Number of samples must be positive")
	end
	if cfg.nbr_it_max <= 0
		error("Max number of iterations msut be positive")
	end
end

function abc(f, g, x0, u, p, mn, on, so_exp, cfg; eta_so_exp = eta_so_exp)
	check_cfg(cfg)
	function search_param(f::Function, g_exp::Array{pygmalion.Observer, 1}, 
                          so_exp::SystemObservation, eta_so_exp::Array{Float64, 1}, 
                          x0::State, p::Parameters, 
                          u::Control, mn::ModellingNoise, on::ObservationNoise, 
                          dist::Function, eta::Function, srl_p::SamplingRuleList, 
                          bl_p::BoundList, 
                          epsilon::Float64, full_sim_eta::Bool) where {State, Parameters, Control, 
                                                   ModellingNoise,ObservationNoise}
        first_step = true
        so_sim = nothing
        eta_so_sim = [Inf for i = 1:length(eta_so_exp)]
        p_prime = nothing
        bool_properties = true
        tml_exp = get_merged_timeline(g_exp)
        while first_step || (dist(eta_so_sim, eta_so_exp) > epsilon) || !bool_properties
            p_prime = sample(p, srl_p)
			#check_bounds!(p_prime, bl_p)
            if !check_bounds(p_prime, bl_p)
                continue
            end
            so_sim_full = nothing
            try
                so_sim_full = simulate(f, g_exp, x0, u, p_prime; mn = mn, on = on, full_timeline = true)
                if full_sim_eta
                    eta_so_sim = eta(so_sim_full)
                else
                    eta_so_sim = eta(filter(so_sim_full, so_sim_full.oml, tml_exp))
                end
            catch err
                if isa(err, DomainError)
                    @warn "Computation error while simulating the model"
                    continue
                end
            end
            bool_properties = check_properties(so_sim_full, cfg.ll_properties, cfg.dict_properties_values)
            first_step = false
		end
		return p_prime
	end

	# Get distance and summary statistics functions
	dist = distance_function(cfg.distance_function)
	eta = eta_function(cfg.eta_function)
	tml = get_merged_timeline(so_exp)
	g_exp = create_observation_function([ObserverModel(join(so_exp.oml,","),collect(tml))])
    eta_so_exp = (eta_so_exp != nothing) ? eta_so_exp : eta(so_exp)
    nbr_sim = 1
	nbr_size_sample = 0
	nbr_p = length(cfg.ll_p)
	mat_p = zeros(nbr_p, cfg.size_sample)
    
	for i in 1:cfg.size_sample
		if mod(i, div(cfg.nbr_it_max,10)) == 0 println("$(Int(floor(100 * i / cfg.nbr_it_p)))%")  end
		p_prime = search_param(f, g_exp, so_exp, eta_so_exp, x0, p, u, mn, on, dist, eta, cfg.srl_p, cfg.bl_p, cfg.epsilon, cfg.full_sim_eta)
		vec_p_prime = to_vec(p_prime, cfg.ll_p)
		mat_p[:, i] = deepcopy(vec_p_prime)
	end

	# ratio = cfg.size_sample / nbr_sim
	ratio = - 1.0
	vec_mean, vec_stdev = mean_and_stdev(mat_p)

	p_hat = create_from_subset(p, get_p_hat(cfg.str_statistical_measure, mat_p), cfg.ll_p)
	r = ResultAbc(p_hat, vec_stdev, mat_p, ratio, cfg.size_sample)
	return r
end

