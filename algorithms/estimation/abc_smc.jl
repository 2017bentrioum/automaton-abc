
@everywhere import StatsBase: cov, ProbabilityWeights
@everywhere import NearestNeighbors: KDTree, knn 
@everywhere using DistributedArrays
@everywhere using Hungarian
using Roots
using NLsolve

if VERSION >= v"0.7.0"
    @everywhere using LinearAlgebra
    using SharedArrays
    import Distributed: @distributed
    import Statistics: quantile
end

@everywhere include(ENV["PYGMALION_DIR"] * "algorithms/automata/period_automaton.jl")
@everywhere include(ENV["PYGMALION_DIR"] * "algorithms/automata/helpers/checkers.jl")
@everywhere include(ENV["PYGMALION_DIR"] * "algorithms/estimation/helpers/metrics.jl")

struct ResultAbcSmc
	p_hat
	vec_stdev::Array{Float64, 2}
	mat_p_end::Array{Float64, 2}
    nbr_sim::Int64
    epsilon::Float64
    epsilon2::Float64
    weights::Array{Float64, 1}
end

struct ConfigurationAbcSmc
	ll_p::LabelList
	srl_p::SamplingRuleList
	bl_p::BoundList
	nbr_particles::Int64
    eta_function::Union{Label, Function}
	distance_function::Union{Label, Function}
	str_statistical_measure::Label
	epsilon::Float64
    alpha::Float64
	NT::Float64
    M::Int64
    kernel_type::Label
    ll_properties::LabelList
	dict_properties_values::Dict
    full_sim_eta::Bool
    eta_function2::Union{Label, Function}
	epsilon2::Float64
end

ConfigurationAbcSmc(ll_p::LabelList, srl_p::SamplingRuleList, 
					  bl_p::BoundList, nbr_particles::Int64,
                      eta_function::Union{Label, Function}, 
                      distance_function::Union{Label, Function}, 
					  str_statistical_measure::Label, epsilon::Float64, 
                      alpha::Float64, NT::Float64, M::Int64, 
                      kernel_type::Label) =
ConfigurationAbcSmc(ll_p, srl_p, 
					  bl_p, nbr_particles,
					  eta_function, distance_function, 
                      str_statistical_measure, epsilon, alpha, NT, M, kernel_type, 
                      Array{Label,1}(), Dict(), false, "default_eta", Inf)

ConfigurationAbcSmc(ll_p::LabelList, srl_p::SamplingRuleList, 
					  bl_p::BoundList, nbr_particles::Int64,
                      eta_function::Union{Label, Function}, 
                      distance_function::Union{Label, Function}, 
					  str_statistical_measure::Label, epsilon::Float64,
                      alpha::Float64, NT::Float64, M::Int64,
                      kernel_type::Label, 
                      eta_function2::Union{Label, Function}, epsilon2::Float64) =
                    ConfigurationAbcSmc(ll_p, srl_p, 
					  bl_p, nbr_particles,
					  eta_function, distance_function, 
                      str_statistical_measure, epsilon, alpha, NT, M, kernel_type, 
                      Array{Label,1}(), Dict(), false, eta_function2, epsilon2)

ConfigurationAbcSmc(ll_p::LabelList, srl_p::SamplingRuleList, 
					  bl_p::BoundList, nbr_particles::Int64,
                      eta_function::Union{Label, Function}, 
                      distance_function::Union{Label, Function}, 
					  str_statistical_measure::Label, epsilon::Float64,
					  alpha::Float64, NT::Float64, M::Int64, kernel_type::Label,
                      full_sim_eta::Bool) = 
                    ConfigurationAbcSmc(ll_p, srl_p, 
					  bl_p, nbr_particles,
					  eta_function, distance_function, 
					  str_statistical_measure, epsilon, alpha, NT, M, kernel_type, 
                      Array{Label,1}(), Dict(), full_sim_eta, "default_eta", Inf)

ConfigurationAbcSmc(ll_p::LabelList, srl_p::SamplingRuleList, 
					  bl_p::BoundList, nbr_particles::Int64,
                      eta_function::Union{Label, Function}, 
                      distance_function::Union{Label, Function}, 
					  str_statistical_measure::Label, epsilon::Float64,
					  alpha::Float64, NT::Float64, M::Int64,
                      kernel_type::Label, ll_properties::LabelList,
                      dict_properties_values::Dict) =  
                    ConfigurationAbcSmc(ll_p, srl_p, 
					  bl_p, nbr_particles,
					  eta_function, distance_function, 
					  str_statistical_measure, epsilon, alpha, NT, M, kernel_type, 
                      ll_properties, dict_properties_values, false, "default_eta", Inf)

function check_cfg(cfg::ConfigurationAbcSmc)
	check_distance_function(cfg.distance_function)
	check_eta_function(cfg.eta_function)
    if cfg.alpha < 0.0 || cfg.alpha > 1.0
        error("Wrong value for alpha")
    end
	if cfg.nbr_particles < 1
		error("Number of particles must be positive")
	end
end

function get_p_hat(str_sm::Label, mat_p)
	nb_p = size(mat_p)[1]
	vec_p = zeros(nb_p)
	if str_sm == "mean"
		for i = 1:nb_p
			vec_p[i] = mean(mat_p[i, :])
		end
	else	
		error("Wrong name of statistical measure")
	end
	return vec_p
end

function abc_smc(f::Function, g::ObserverList, 
                    x0::State, u::Control, p::Parameters, 
                    mn::ModellingNoise, on::ObservationNoise, 
                    so_exp::SystemObservation, cfg::ConfigurationAbcSmc; 
                    eta_so_exp = nothing) where
                        {State, Parameters, Control, 
                         ModellingNoise, ObservationNoise}

	file_cfg = open(get_last_results_directory() * "cfg_abc_smc.txt", "w")
	write(file_cfg, "Number of parameters : $(length(cfg.ll_p)) \n")
	write(file_cfg, "Number of particles : $(cfg.nbr_particles) \n")
    write(file_cfg, "Epsilons : $(cfg.epsilon) \n")
    if on != nothing
        write(file_cfg, "On : $(on) \n")
    else
        write(file_cfg, "No on \n")
    end
    write(file_cfg, "alpha : $(cfg.alpha) \n")
    write(file_cfg, "ll_p : $(cfg.ll_p) \n")
	write(file_cfg, "srl_p : $(cfg.srl_p) \n")
	write(file_cfg, "kernel type : $(cfg.kernel_type) \n")
	close(file_cfg)
	if nprocs() == 1
	    error("Please run again with at least one thread")
    end
	println("Begin parallel ABC SMC")
    return par_abc_smc_darray(f, g, x0, u, p, mn, on, so_exp, cfg; eta_so_exp = eta_so_exp)
end	

@everywhere function compute_distance(mat_p::Array{Float64,2}, 
                          f::Function, 
                          g_exp::Array{pygmalion.Observer, 1}, 
                          eta_so_exp::Array{Float64, 1}, 
                          x0::State, p::Parameters, u::Control, 
                          mn::ModellingNoise, 
                          on::ObservationNoise, 
                          dist::Function, eta::Function, 
                          srl_p::SamplingRuleList, 
                          bl_p::BoundList, 
                          ll_p::LabelList,
                          full_sim_eta::Bool) where 
                            {State, Parameters, Control, 
                            ModellingNoise, ObservationNoise}
    nbr_particles = size(mat_p)[2]
    vec_dist = zeros(nbr_particles)
    for i = 1:nbr_particles
        p_i = create_from_subset(p, mat_p[:,i], ll_p)
        if full_sim_eta
            so_sim_i = simulate(f, g_exp, x0, u, p_i; mn = mn, on = on, full_timeline = true)
        else
            so_sim_i = simulate(f, g_exp, x0, u, p_i; mn = mn, on = on)
        end
        vec_dist[i] = dist(eta(so_sim_i), eta_so_exp)
    end
    return vec_dist
end

@everywhere function task_worker!(old_mat_p::Array{Float64, 2}, 
                                  old_mat_eta_sim::Array{Float64, 2}, 
                                  f::Function, g_exp::Array{pygmalion.Observer, 1}, 
                                  so_exp::SystemObservation, 
                                  eta_so_exp::Array{Float64, 1}, eta_so_exp2::Array{Float64, 1}, 
                                  x0::State, p::Parameters, u::Control, 
                                  mn::ModellingNoise, on::ObservationNoise, 
                                  dist::Function, eta::Function, eta2::Function,  
                                  srl_p::SamplingRuleList, bl_p::BoundList, 
                                  ll_p::LabelList,
                                  epsilon::Float64, epsilon2::Float64, ll_properties::LabelList, 
                                  dict_properties_values::Dict, 
                                  wl_old::Array{Float64, 1},
                                  old_mat_p_full::Array{Float64, 2}, 
                                  full_sim_eta::Bool,
                                  kernel_type::Label) where 
                                {State, Parameters, Control, 
                                ModellingNoise, ObservationNoise}

    mat_cov = zeros(0, 0)
    if kernel_type == "mv_normal"
        mat_cov = 2 * cov(old_mat_p_full, ProbabilityWeights(wl_old), 2; corrected=false)
        if myid() == 2
			@show diag(mat_cov)
        end
        if det(mat_cov) == 0.0
            @show det(mat_cov)
            @show rank(mat_cov)
            @show effective_sample_size(wl_old)
            error("Bad inv mat cov")
        end
    end
    tree_mat_p = KDTree(zeros(1,1))
    if kernel_type == "knn_mv_normal"
        tree_mat_p = KDTree(old_mat_p_full)
    end
    local_mat_p = zeros(size(old_mat_p))
    local_nbr_particles = size(local_mat_p)[2]
    local_mat_eta_sim = zeros(size(old_mat_eta_sim))
    M = Int(size(local_mat_eta_sim)[2] / local_nbr_particles)
    nbr_p = size(local_mat_p)[1]
    nbr_total_sim = 0
    for i = 1:local_nbr_particles
        ind_eta_sim = ((i-1)*M+1):(i*M)
        local_mat_p[:,i], local_mat_eta_sim[:,ind_eta_sim], nbr_sim =
            update_param(f, g_exp, so_exp, eta_so_exp, eta_so_exp2, 
                         x0, p, u, mn, on, dist, eta, eta2, srl_p, bl_p, ll_p, 
                         epsilon, epsilon2, ll_properties, dict_properties_values, 
                         old_mat_p[:,i], old_mat_eta_sim[:,ind_eta_sim], 
                         tree_mat_p, mat_cov, wl_old, old_mat_p_full, 
                         full_sim_eta, kernel_type)
        nbr_total_sim += nbr_sim
    end
    return nbr_total_sim, local_mat_p, local_mat_eta_sim
end

@everywhere function get_param_kernel(kernel_type::Label, vec_p_star::Array{Float64, 1},
                                       vec_mat_p_old::Array{Float64, 2}, 
                                       wl_old::Array{Float64, 1},
                                       mat_cov::Array{Float64, 2}, 
                                       tree_mat_p::Union{KDTree,Nothing})
    if kernel_type == "mv_normal"
        d_mvnormal = MvNormal(vec_p_star, mat_cov)
        return d_mvnormal
    elseif kernel_type == "knn_mv_normal"
        k = Int(round(0.25 * size(vec_mat_p_old)[2]))
        idxs, dist = knn(tree_mat_p, vec_p_star, k, true)
        knn_mat_cov = 2 * cov(vec_mat_p_old[:,idxs], ProbabilityWeights(wl_old[idxs]), 2; corrected=false)
        d_mvnormal = MvNormal(vec_p_star, knn_mat_cov)
        return d_mvnormal
    else
        error("Unknown specified kernel")
    end
end

@everywhere function update_param(f::Function, 
                                  g_exp::Array{pygmalion.Observer, 1}, 
                                  so_exp::SystemObservation, 
                                  eta_so_exp::Array{Float64, 1}, eta_so_exp2::Array{Float64, 1},
                                  x0::State, p::Parameters, u::Control, 
                                  mn::ModellingNoise, on::ObservationNoise, 
                                  dist::Function, eta::Function, eta2::Function,  
                                  srl_p::SamplingRuleList, bl_p::BoundList, 
                                  ll_p::LabelList,
                                  epsilon::Float64, epsilon2::Float64,
                                  ll_properties::LabelList, 
                                  dict_properties_values::Dict,
                                  vec_p_current::Array{Float64, 1}, 
                                  old_mat_eta_sim_current::Array{Float64, 2}, 
                                  tree_mat_p::KDTree, 
                                  mat_cov::Array{Float64, 2}, 
                                  wl_old::Array{Float64, 1},
                                  vec_mat_p_old::Array{Float64, 2},
                                  full_sim_eta::Bool, kernel_type::Label) where 
                                    {State, Parameters, Control, 
                                    ModellingNoise, ObservationNoise}
    so_sim_full, p_prime = nothing, nothing
    tml_exp = get_merged_timeline(g_exp)
    M = size(old_mat_eta_sim_current)[2]
    vec_p_prime = nothing
    nbr_sim = 0
    p_current = create_from_subset(p, vec_p_current, ll_p)
    mat_eta_sim_prime = zeros(length(eta_so_exp), M)
    while so_sim_full == nothing
        d_kernel = get_param_kernel(kernel_type, vec_p_current, vec_mat_p_old, wl_old, mat_cov, tree_mat_p)
        vec_p_prime = rand(d_kernel)
        p_prime = create_from_subset(p, vec_p_prime, ll_p)
        if !check_bounds(p_prime, bl_p)
            continue
        end
        so_sim_full = nothing
        eta_so_sim = nothing
        try
            so_sim_full = simulate(f, g_exp, x0, u, p_prime; mn = mn, on = on, full_timeline = true)
            nbr_sim += 1
            if full_sim_eta
                eta_so_sim = eta(so_sim_full)
            else
                eta_so_sim = eta(filter(so_sim_full, so_sim_full.oml, tml_exp))
            end
        catch err
            if isa(err, DomainError)
                #@warn "Computation error while simulating the model"
                continue
            end
            throw(err)
        end
        mat_eta_sim_prime[:,1] = eta_so_sim
        for m = 2:M
            so_sim_i = simulate(f, g_exp, x0, u, p_prime; mn = mn, on = on, 
                                full_timeline = true)
            nbr_sim += 1
            mat_eta_sim_prime[:,m] = (full_sim_eta) ? eta(so_sim_full) :
                                        eta(filter(so_sim_full, so_sim_full.oml, tml_exp))
        end
        d_reverse_kernel = get_param_kernel(kernel_type, vec_p_prime, vec_mat_p_old, wl_old, mat_cov, tree_mat_p)
        r = exp(log_pdf(p_prime, srl_p))*pdf(d_reverse_kernel, vec_p_current) /
        (exp(log_pdf(p_current, srl_p))*pdf(d_kernel, vec_p_prime))
        num = 0.0
        denom = 0.0
        for m = 1:M
            num += Float64(dist(eta_so_exp, mat_eta_sim_prime[:,m]) <= epsilon)
            denom += Float64(dist(eta_so_exp, old_mat_eta_sim_current[:,m]) <= epsilon)
        end
        r *= (num/denom)
        u = rand(Uniform(0,1))
        if u > r
            vec_p_prime = vec_p_current
            mat_eta_sim_prime = old_mat_eta_sim_current
        end
    end
    return (vec_p_prime, mat_eta_sim_prime, nbr_sim)
end

function new_wl_update(eps::Float64, old_eps::Float64, wl_old::Array{Float64, 1}, 
                       old_mat_eta_sim::Array{Float64, 2}, eta_so_exp::Array{Float64, 1}, dist::Function)  
    if eps == old_eps
        return wl_old
    end
    nbr_pa = length(wl_old)
    new_wl = zeros(nbr_pa)
    M = Int(size(old_mat_eta_sim)[2] / nbr_pa)
    for i = 1:nbr_pa
        ind_begin_mat = (i-1)*M + 1
        ind_end_mat = (i)*M
        new_wl[i] = update_weight(eps, old_eps, wl_old[i], old_mat_eta_sim[:,ind_begin_mat:ind_end_mat], eta_so_exp, dist)
    end
    return new_wl
end

@everywhere function update_weight(eps::Float64,
                                   eps_old::Float64, 
                                   wl_i::Float64, 
                                   mat_eta_sim_i::Array{Float64, 2}, 
                                   eta_so_exp::Array{Float64, 1}, 
                                   dist::Function) 
    new_wl_i = wl_i
    num = 0.0
    denom = 0.0
    M = size(mat_eta_sim_i, 2)
    for m in 1:M
        num += Float64(dist(eta_so_exp, mat_eta_sim_i[:,m]) <= eps)
        denom += Float64(dist(eta_so_exp, mat_eta_sim_i[:,m]) <= eps_old)
    end
    if denom == 0.0
        return 0.0
    end
    if denom == 0.0
        @show num, denom, eps_old, dist(eta_so_exp, mat_eta_sim_i[:,1])
    end
    new_wl_i *= (num / denom)
    return new_wl_i 
end


function par_abc_smc_darray(f::Function, g::ObserverList, 
                         x0::State, u::Control, p::Parameters, 
                         mn::ModellingNoise, on::ObservationNoise, 
                         so_exp::SystemObservation, cfg::ConfigurationAbcSmc; 
                         eta_so_exp = nothing, eta_so_exp2 = nothing) where 
                                {State, Parameters, Control, 
                                ModellingNoise, ObservationNoise}

	check_cfg(cfg)
    str_dir_ess = get_last_results_directory() * "ess/"
    mkdir(str_dir_ess)
    # Get distance and summary statistics functions
	dist = distance_function(cfg.distance_function)
	eta = eta_function(cfg.eta_function)
	eta2 = eta_function(cfg.eta_function2)
    name_eta2 = string(eta2)
    tml = get_merged_timeline(so_exp)
	g_exp = create_observation_function([ObserverModel(join(so_exp.oml,","),collect(tml))])
    eta_so_exp = (eta_so_exp != nothing) ? eta_so_exp : eta(so_exp)
    eta_so_exp2 = (eta_so_exp2 != nothing) ? eta_so_exp2 : eta2(so_exp)
    nbr_p = length(cfg.ll_p)
	ll_p = cfg.ll_p
	bl_p = cfg.bl_p
	srl_p = cfg.srl_p
    nbr_particles = cfg.nbr_particles
    ll_properties = cfg.ll_properties
    dict_properties_values = deepcopy(cfg.dict_properties_values)
    wl_old = ones(cfg.nbr_particles)
    normalize!(wl_old, 1)
    full_sim_eta = cfg.full_sim_eta
    kernel_type = cfg.kernel_type

    # Choice of epsilons
    first_epsilon = Inf
    last_epsilon = cfg.epsilon
    first_epsilon2 = Inf
    last_epsilon2 = cfg.epsilon2
    @show first_epsilon, last_epsilon
    @show first_epsilon2, last_epsilon2
	epsilon = first_epsilon
	epsilon2 = first_epsilon2
    # Init. Iteration 1
	if haskey(dict_properties_values, "periodicity")
		dict_properties_values["periodicity"][1] = 1
	end
    
    old_mat_p = zeros(nbr_p, cfg.nbr_particles)
    old_mat_eta_sim = zeros(length(eta_so_exp), cfg.M * cfg.nbr_particles)
    i = 1
    nbr_sim_init = 0
    while i <= nbr_particles
        p_i = sample(p, srl_p) 
        old_mat_p[:,i] = to_vec(p_i, ll_p)
        so_sim_init = nothing
        nbr_sim_init += 1
        try
            so_sim_init = simulate(f, g_exp, x0, u, p_i; mn = mn, on = on, full_timeline = true)
        catch err
            if isa(err, DomainError)
                continue
            end
        end
        old_mat_eta_sim[:,(i-1)*cfg.M + 1] = eta(so_sim_init)
        for m = 2:cfg.M
            so_sim_init = simulate(f, g_exp, x0, u, p_i; mn = mn, on = on, full_timeline = true)
            old_mat_eta_sim[:,(i-1)*cfg.M + m] = eta(so_sim_init)
        end
        i += 1
    end
    @show name_eta2
	@show effective_sample_size(wl_old)
    t = 1
    nbr_tot_sim = nbr_sim_init
	is_wasserstein_dist = false
    old_epsilon = epsilon
    while epsilon > last_epsilon || epsilon2 > last_epsilon2
        t += 1
        println("----------------")
        # Step : find new epsilon
        @show old_epsilon
        @show effective_sample_size(new_wl_update(old_epsilon, old_epsilon, wl_old, old_mat_eta_sim, eta_so_exp, dist))
        @show effective_sample_size(wl_old)
        @show effective_sample_size(new_wl_update(0.9*last_epsilon, old_epsilon, wl_old, old_mat_eta_sim, eta_so_exp, dist))
        inf_int = 0.9*last_epsilon
        sup_int = old_epsilon
        @show inf_int, sup_int
        root_ess(eps::Float64) = (effective_sample_size(new_wl_update(eps, old_epsilon, wl_old, old_mat_eta_sim, eta_so_exp, dist)) -
                                   cfg.alpha * effective_sample_size(wl_old))
        @show root_ess(inf_int), root_ess(sup_int)
        if root_ess(inf_int) > 0.0
            epsilon = last_epsilon
        else
            epsilon = find_zero(eps -> effective_sample_size(new_wl_update(eps, old_epsilon, wl_old, old_mat_eta_sim, eta_so_exp, dist)) -
                            cfg.alpha * effective_sample_size(wl_old), (inf_int, sup_int), Bisection())
            if epsilon == sup_int
                epsilon = quantile([inf_int, sup_int], cfg.alpha)
            end
        end
        fig = pygmalion_plot()
        title("ESS tour $t")
        sup_x = (sup_int == Inf) ? 1.5 * epsilon : sup_int
        x_ess = collect(inf_int:0.01:sup_x)
        y_ess = root_ess.(x_ess)
        plot(x_ess, y_ess, "ro--", linewidth = 1.0, ms = 1.0)
        savefig(str_dir_ess * "ess_$t.eps")
        close()
        @show epsilon, root_ess(epsilon)
        @show sup_int, root_ess(sup_int)
        wl_current = new_wl_update(epsilon, old_epsilon, wl_old, old_mat_eta_sim, eta_so_exp, dist)
        normalize!(wl_current, 1)
        @show wl_old[1:5], wl_current[1:5]
        @show old_mat_p[:,1:5]
        dist_eta_sim = (eta_sim -> dist(eta_so_exp, eta_sim)).([old_mat_eta_sim[:,i] for i=1:size(old_mat_eta_sim)[2]])
        @show sort(dist_eta_sim[1:5])
        @show length(unique(dist_eta_sim))
        #@show dist_eta_sim[[(d > old_epsilon) for d in dist_eta_sim]] 
        #@show dist_eta_sim[[(d > epsilon) for d in dist_eta_sim]] 
        @show length(dist_eta_sim[[(d > old_epsilon) for d in dist_eta_sim]])
        @show length(dist_eta_sim[[(d > epsilon) for d in dist_eta_sim]])
        println("##################")
        if haskey(dict_properties_values, "periodicity")
		    dict_properties_values["periodicity"][1] = t
        end
        # Step : resampling
        if effective_sample_size(wl_current) < cfg.NT
            println("Resampling..")
            d_weights = pygmalion.Categorical(Array{Float64, 1}(wl_current))
            ind_w = rand(d_weights, nbr_particles)
            ind_mat_eta_sim = zeros(Int, cfg.M * nbr_particles)
            for i = 1:nbr_particles
                ind_begin, ind_end = (ind_w[i]-1)*cfg.M+1, ind_w[i]*cfg.M
                ind_mat_eta_sim[((i-1)*cfg.M+1):(i*cfg.M)] = ind_begin:ind_end        
            end
            #@show ind_mat_eta_sim, ind_w
            old_mat_p = old_mat_p[:,ind_w]
            old_mat_eta_sim = old_mat_eta_sim[:,ind_mat_eta_sim]
            wl_current = ones(nbr_particles)
            normalize!(wl_current, 1)
            println("End")
        end
        # Step : moving
        l_nbr_sim = zeros(nworkers())
        current_mat_p = zeros(nbr_p, cfg.nbr_particles)
        current_mat_eta_sim = zeros(length(eta_so_exp), cfg.M * cfg.nbr_particles)
        pa_per_worker = div(nbr_particles, nworkers())
        @sync for id_w in workers()
            t_id_w = id_w - workers()[1] + 1
            ind_begin_pa = (t_id_w-1) * pa_per_worker + 1
            ind_end_pa = (t_id_w) * pa_per_worker
            ind_begin_mat_eta = (t_id_w-1) * pa_per_worker * cfg.M + 1
            ind_end_mat_eta = (t_id_w) * pa_per_worker * cfg.M
            if id_w == workers()[end]
                ind_end_pa = nbr_particles
                ind_end_mat_eta = nbr_particles * cfg.M
            end
            ind_pa = ind_begin_pa:ind_end_pa
            ind_eta = ind_begin_mat_eta:ind_end_mat_eta
            @async l_nbr_sim[t_id_w], current_mat_p[:,ind_pa], current_mat_eta_sim[:,ind_eta] = 
            remotecall_fetch(() -> task_worker!(old_mat_p[:,ind_pa], old_mat_eta_sim[:,ind_eta], 
                                                f, g_exp, so_exp, eta_so_exp, eta_so_exp2, 
                                                x0, p, u, mn, on, dist, eta, eta2, 
                                                srl_p, bl_p, ll_p, epsilon, epsilon2, 
                                                ll_properties, dict_properties_values, 
                                                wl_current, old_mat_p, full_sim_eta, kernel_type), 
                                                id_w)
        end
        nbr_tot_sim += sum(l_nbr_sim)
        # Wasserstein distance
        if is_wasserstein_dist
            mat_weights = zeros(nbr_particles, nbr_particles)
            for i = 1:nbr_particles
                for j = 1:nbr_particles
                    mat_weights[i,j] = sqrt(euclidean_distance(old_mat_p[:,i], current_mat_p[:,j]))
                end
            end
            assignement, w_dist = hungarian(mat_weights)
            w_dist /= nbr_particles
            @show w_dist
        end
        @show abs(old_epsilon - epsilon) / old_epsilon
        println("Computations after current iteration")
		@show effective_sample_size(wl_current)
        old_mat_p = current_mat_p
        old_mat_eta_sim = current_mat_eta_sim
        old_epsilon = epsilon
		wl_old = wl_current
	end

	mat_p = old_mat_p
	p_hat = create_from_subset(p, get_p_hat(cfg.str_statistical_measure, mat_p), cfg.ll_p)
    vec_mean, vec_stdev = mean_and_stdev(mat_p)
    r = ResultAbcSmc(p_hat, vec_stdev, old_mat_p, nbr_tot_sim, epsilon, epsilon2, wl_old)
    return r
end

