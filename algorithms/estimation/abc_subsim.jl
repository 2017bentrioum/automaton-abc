
import StatsBase: cov, ProbabilityWeights
@everywhere using DistributedArrays

if VERSION >= v"0.7.0"
    using LinearAlgebra
    using SharedArrays
    import Distributed: @distributed
end

@everywhere include(ENV["PYGMALION_DIR"] * "algorithms/estimation/helpers/metrics.jl")
@everywhere include(ENV["PYGMALION_DIR"] * "algorithms/automata/helpers/checkers.jl")

struct ResultAbcSubSim
	p_hat
	vec_stdev::Array{Float64, 2}
	mat_p::Array{Float64, 2}
    l_epsilon::Array{Float64, 1}
    nbr_sim::Int64
end

struct ConfigurationAbcSubSim
	ll_p::LabelList
	srl_p::SamplingRuleList
	bl_p::BoundList
	nbr_particles::Int64
    eta_function::Union{Label, Function}
    distance_function::Union{Label, Function}
	str_statistical_measure::Label
    epsilon::Float64
    P0::Float64
    std_prop::Float64
    ll_properties::LabelList
	dict_properties_values::Dict
    full_sim_eta::Bool
end

ConfigurationAbcSubSim(ll_p::LabelList, srl_p::SamplingRuleList, 
					  bl_p::BoundList, nbr_particles::Int64,
                      eta_function::Union{Label, Function}, 
                      distance_function::Union{Label, Function}, 
					  str_statistical_measure::Label, 
                      epsilon::Float64, P0::Float64, std_prop::Float64) = 
                    ConfigurationAbcSubSim(ll_p, srl_p, 
					  bl_p, nbr_particles,
					  eta_function, distance_function, 
					  str_statistical_measure, epsilon,
                      P0, std_prop,
                      Array{Label,1}(), Dict(), false)

ConfigurationAbcSubSim(ll_p::LabelList, srl_p::SamplingRuleList, 
					  bl_p::BoundList, nbr_particles::Int64,
                      eta_function::Union{Label, Function}, 
                      distance_function::Union{Label, Function}, 
					  str_statistical_measure::Label, 
                      epsilon::Float64, P0::Float64, std_prop::Float64,
                      full_sim_eta::Bool) = 
                    ConfigurationAbcSubSim(ll_p, srl_p, 
					  bl_p, nbr_particles,
					  eta_function, distance_function, 
					  str_statistical_measure, epsilon,
                      P0, std_prop,
                      Array{Label,1}(), Dict(), full_sim_eta)

ConfigurationAbcSubSim(ll_p::LabelList, srl_p::SamplingRuleList, 
					  bl_p::BoundList, nbr_particles::Int64,
                      eta_function::Union{Label, Function}, 
                      distance_function::Union{Label, Function}, 
					  str_statistical_measure::Label, 
                      epsilon::Float64, P0::Float64, std_prop::Float64,
                      ll_properties::LabelList, dict_properties_values::Dict) = 
                    ConfigurationAbcSubSim(ll_p, srl_p, 
					  bl_p, nbr_particles,
					  eta_function, distance_function, 
					  str_statistical_measure, epsilon,
                      P0, std_prop,
                      ll_properties, dict_properties_values, false)


function get_p_hat(str_sm::Label, mat_p)
	nb_p = size(mat_p)[1]
	vec_p = zeros(nb_p)
	if str_sm == "mean"
		for i = 1:nb_p
			vec_p[i] = mean(mat_p[i, :])
		end
	elseif str_sm == "mode"
		for i = 1:nb_p
			vec_p[i] = mode(mat_p[i, :])
		end
	else	
		error("Wrong name of statistical measure")
	end
	return vec_p
end

function check_cfg(cfg::ConfigurationAbcSubSim)
	check_distance_function(cfg.distance_function)
	check_eta_function(cfg.eta_function)
	if cfg.nbr_particles < 1
		error("Number of particles must be positive")
	end
end

function abc_subsim(f::Function, g::Array{pygmalion.Observer, 1}, x0::State, u::Control, 
                    p::Parameters, mn::ModellingNoise, on::ObservationNoise, 
                    so_exp::SystemObservation, cfg::ConfigurationAbcSubSim; 
                    eta_so_exp = nothing) where 
                    {State, Parameters, Control, 
                    ModellingNoise, ObservationNoise}
    check_cfg(cfg)
    file_cfg = open(get_last_results_directory() * "cfg_abc_subsim.txt", "w")
	write(file_cfg, "Number of parameters : $(length(ll_p)) \n")
	write(file_cfg, "Number of particles : $(cfg.nbr_particles) \n")
    write(file_cfg, "Priors : $(cfg.srl_p)")
    if on != nothing
        write(file_cfg, "On : $(on) \n")
    else
        write(file_cfg, "No on \n")
    end
    write(file_cfg, "P0 : $(cfg.P0) \n")
    write(file_cfg, "mma std : $(cfg.std_prop) \n")
	close(file_cfg)
    return _abc_subsim2(f, g, x0, u, p, mn, on, so_exp, cfg; eta_so_exp = eta_so_exp)
end

## Without darray

# Faut-il mettre dedans la condition logique ?
@everywhere function prior_sim_worker(nbr_pa_per_worker::Int64,
                                      f::Function, 
                                      g_exp::Array{pygmalion.Observer, 1}, 
                                      eta_so_exp::Array{Float64, 1}, 
                                      x0::State, p::Parameters, u::Control, 
                                      mn::ModellingNoise, 
                                      on::ObservationNoise, 
                                      dist::Function, eta::Function, 
                                      srl_p::SamplingRuleList, 
                                      bl_p::BoundList, ll_p::LabelList) where 
                                        {State, Parameters, Control, 
                                         ModellingNoise, ObservationNoise}
    nbr_p = length(ll_p)
    local_vec_dist = zeros(nbr_pa_per_worker)
    local_mat_p = zeros(nbr_p, nbr_pa_per_worker)
    nbr_sim = 0
    for i = 1:nbr_pa_per_worker
        so_sim_i = nothing
        p_i = nothing
        while so_sim_i == nothing
            p_i = sample(p, srl_p)
            check_bounds!(p_i, bl_p)
            try
                so_sim_i = simulate(f, g_exp, x0, u, p_i; mn = mn, on = on)
            catch err
                if isa(err, DomainError)
                    #@warn "Computation error while simulating the model"
                    continue
                end
                throw(err)
            end
            nbr_sim += 1
        end
        local_mat_p[:,i] = to_vec(p_i, ll_p)::Array{Float64, 1}
        local_vec_dist[i] = dist(eta_so_exp, eta(so_sim_i))
    end
    return nbr_sim, local_vec_dist, local_mat_p
end

#=
@everywhere function compute_distance_worker(mat_p::Array{Float64,2}, 
                                             f::Function, 
                                             g_exp::Array{pygmalion.Observer, 1}, 
                                             eta_so_exp::Array{Float64, 1}, 
                                             x0::State, p::Parameters, u::Control, 
                                             mn::ModellingNoise, 
                                             on::ObservationNoise, 
                                             dist::Function, eta::Function, 
                                             srl_p::SamplingRuleList, 
                                             bl_p::BoundList, 
                                             ll_p::LabelList,
                                             full_sim_eta::Bool) where 
                                            {State, Parameters, Control, 
                                             ModellingNoise, ObservationNoise}
    local_nbr_particles = size(mat_p)[2]
    local_vec_dist = zeros(local_nbr_particles)
    for i = 1:local_nbr_particles
        p_i = create_from_subset(p, mat_p[:,i], ll_p)
        if full_sim_eta
            so_sim_i = simulate(f, g_exp, x0, u, p_i; mn = mn, on = on, full_timeline = true)
        else
            so_sim_i = simulate(f, g_exp, x0, u, p_i; mn = mn, on = on)
        end
        local_vec_dist[i] = dist(eta(so_sim_i), eta_so_exp)
    end
    return local_vec_dist
end
=#

@everywhere function mma_worker(mat_p_seeds::Array{Float64, 2}, vec_dist::Array{Float64, 1}, 
                                nbr_particles::Int64, P0::Float64, 
                                lgth_total_pa::Int64, 
                                epsilon_j::Float64, std_prop::Float64, f::Function, 
                                g_exp::Array{pygmalion.Observer, 1}, 
                                eta_so_exp::Array{Float64, 1}, 
                                x0::State, p::Parameters, u::Control, 
                                mn::ModellingNoise, on::ObservationNoise, 
                                dist::Function, eta::Function, 
                                srl_p::SamplingRuleList, bl_p::BoundList, 
                                ll_p::LabelList, ll_properties::LabelList,
                                dict_properties_values::Dict, full_sim_eta::Bool) where 
                        {State, Parameters, Control, 
                        ModellingNoise, ObservationNoise}
    nbr_seeds = size(mat_p_seeds)[2]
    nbr_p = size(mat_p_seeds)[1]
    lgth_chain = Int(1/P0)
    local_vec_dist = zeros(lgth_total_pa)
    local_mat_p = zeros(nbr_p, lgth_total_pa)
    tml_exp = get_merged_timeline(g_exp)
    nbr_accept = 0
    for k = 1:nbr_seeds
        if (k-1)*lgth_chain+1 > lgth_total_pa
            return local_mat_p
        end
        vec_p_old = mat_p_seeds[:,k]
        dist_p_old = vec_dist[k]
        local_mat_p[:,(k-1)*lgth_chain+1] = vec_p_old
        local_vec_dist[(k-1)*lgth_chain+1] = dist_p_old
        for j = 2:lgth_chain
            if (k-1)*lgth_chain+j > lgth_total_pa
                return local_mat_p
            end
            p_old = create_from_subset(p, vec_p_old, ll_p)
            p_prime = deepcopy(p_old)
            for sr in srl_p
                for name_single_p in sr.ll
                    single_p_old = getfield(p_old, Symbol(name_single_p))
                    std_univ = std_prop * single_p_old
					if std_univ < 10e-5 std_univ = 10e-5 end
					univ_prop = Normal(single_p_old, std_univ)
                    single_p_prime = rand(univ_prop)
                    std_sym_univ = std_prop * single_p_prime
					if std_sym_univ < 10e-5 std_sym_univ = 10e-5 end
                    sym_univ_prop = Normal(single_p_prime, std_sym_univ)
                    pdf_prior_prime = exp(log_pdf(single_p_prime, sr))
                    pdf_prior_old = exp(log_pdf(single_p_old, sr))
					r = (pdf_prior_prime * pdf(sym_univ_prop, single_p_old)) / 
						(pdf_prior_old * pdf(univ_prop, single_p_prime))
                    if rand(Uniform(0,1)) <= r
                        setfield!(p_prime, Symbol(name_single_p), single_p_prime)
                    end
                end
            end
			check_bounds!(p_prime, bl_p)
			so_prime_full = nothing
            try 
                so_prime_full = simulate(f, g_exp, x0, u, p_prime; 
                                     mn = mn, on = on, full_timeline = true)
            catch err
                if !isa(err, DomainError)
                    throw(err)
                end
            end
            # Ajouter automate etc..
            eta_so_sim = nothing
            if so_prime_full != nothing
                bool_properties = check_properties(so_prime_full, ll_properties, dict_properties_values)
                if full_sim_eta
                    eta_so_sim = eta(so_prime_full)
                else
                    eta_so_sim = eta(filter(so_prime_full, so_prime_full.oml, tml_exp))
                end
            end
            dist_p_prime = dist(eta_so_sim, eta_so_exp) 
            if so_prime_full != nothing &&
                dist_p_prime <= epsilon_j && bool_properties
                nbr_accept += 1
                vec_p_prime = to_vec(p_prime, ll_p)
                local_mat_p[:,(k-1)*lgth_chain+j] = vec_p_prime
                local_vec_dist[(k-1)*lgth_chain+j] = dist_p_prime
                vec_p_old = vec_p_prime
            else
                local_mat_p[:,(k-1)*lgth_chain+j] = vec_p_old
                local_vec_dist[(k-1)*lgth_chain+j] = dist_p_old
            end
        end
    end
    return (nbr_accept, local_vec_dist, local_mat_p)
end

function _abc_subsim2(f::Function, g::Array{pygmalion.Observer, 1}, x0::State, u::Control, 
                      p::Parameters, mn::ModellingNoise, on::ObservationNoise, 
                      so_exp::SystemObservation, cfg::ConfigurationAbcSubSim; 
                      eta_so_exp = nothing)  where 
                        {State, Parameters, Control, 
                        ModellingNoise, ObservationNoise}
    is_adapted_mma_std = true
    # Get distance and summary statistics functions
    dist = distance_function(cfg.distance_function)
	eta = eta_function(cfg.eta_function)
	tml = get_merged_timeline(so_exp)
	g_exp = create_observation_function([ObserverModel(join(so_exp.oml,","),collect(tml))])
    eta_so_exp = (eta_so_exp != nothing) ? eta_so_exp : eta(so_exp)
    nbr_p = length(cfg.ll_p)
	ll_p = cfg.ll_p
	bl_p = cfg.bl_p
	srl_p = cfg.srl_p
	nbr_particles = cfg.nbr_particles
    P0 = cfg.P0
    std_prop = cfg.std_prop
    ll_properties = cfg.ll_properties
    dict_properties_values = cfg.dict_properties_values
    full_sim_eta = cfg.full_sim_eta
    lgth_chain = Int(1/P0)
    nbr_seeds = Int(nbr_particles * P0)
    nbr_total_sim = 0
    l_nbr_sim = zeros(nworkers())
    println("Final epsilon : $(cfg.epsilon)")
    
    # Init with prior sim
    vec_dist = zeros(nbr_particles)
    mat_p = zeros(nbr_p, nbr_particles)
    @sync for id_w in workers()
        t_id_w = id_w - workers()[1] + 1
        ind_begin = (t_id_w-1) * div(nbr_particles, nworkers()) + 1
        ind_end = t_id_w * div(nbr_particles, nworkers())
        if id_w == workers()[end]
            ind_end = nbr_particles
        end
        nbr_pa_per_worker = ind_end - ind_begin + 1
        @async l_nbr_sim[t_id_w], vec_dist[ind_begin:ind_end], mat_p[:,ind_begin:ind_end] = remotecall_fetch(() -> prior_sim_worker(nbr_pa_per_worker, f, g_exp, eta_so_exp, x0, p, u, mn, on, dist, eta, srl_p, bl_p, ll_p), id_w)
    end
    nbr_total_sim += sum(l_nbr_sim)
    epsilon_j = Inf
    l_epsilon = convert(Array{Float64, 1}, [])
    while epsilon_j > cfg.epsilon
        @sync for id_w in workers()
            t_id_w = id_w - workers()[1] + 1
            ind_begin = (t_id_w-1) * div(nbr_particles, nworkers()) + 1
            ind_end = t_id_w * div(nbr_particles, nworkers()) + 1
            if id_w == workers()[end]
                ind_end = nbr_particles
            end
        end
        ind_sorted_vec_dist = sortperm(vec_dist)
        epsilon_j = 0.5(vec_dist[ind_sorted_vec_dist[nbr_seeds]]
                        +vec_dist[ind_sorted_vec_dist[nbr_seeds + 1]])
        println("Epsilon : $epsilon_j")
        new_mat_p = zeros(nbr_p, nbr_particles)
        l_nbr_accept = zeros(nworkers())
        @sync begin
            per_worker = div(nbr_seeds, nworkers()) 
            ind_begin = 0
            ind_end = 0
            for id_w in workers()
                t_id_w = id_w - workers()[1] + 1
                ind_begin_pa = (t_id_w-1) * lgth_chain * per_worker + 1
                ind_end_pa = (t_id_w) * lgth_chain * per_worker
                ind_begin_seeds = (t_id_w-1) * per_worker + 1
                ind_end_seeds = (t_id_w) * per_worker
                if id_w == workers()[end]
                    ind_end_pa = nbr_particles
                    ind_end_seeds = nbr_seeds
                end
                lgth_total_pa = ind_end_pa - ind_begin_pa + 1
                @async l_nbr_accept[t_id_w], vec_dist[ind_begin_pa:ind_end_pa], new_mat_p[:,ind_begin_pa:ind_end_pa] = remotecall_fetch(() -> mma_worker(mat_p[:,ind_sorted_vec_dist[ind_begin_seeds:ind_end_seeds]], vec_dist[ind_sorted_vec_dist[ind_begin_seeds:ind_end_seeds]], nbr_particles, P0, lgth_total_pa, epsilon_j, std_prop, f, g_exp, eta_so_exp, x0, p, u, mn, on, dist, eta, srl_p, bl_p, ll_p, ll_properties, dict_properties_values, full_sim_eta), id_w)
            end
        end
        nbr_accept_total = sum(l_nbr_accept)
        nbr_total_sim += nbr_particles
        ratio_accept = nbr_accept_total / nbr_particles
        if is_adapted_mma_std && ratio_accept < 0.05
            std_prop *= 0.9
        end
        @show ratio_accept
        @show std_prop
        mat_p = new_mat_p
        l_epsilon = vcat(l_epsilon, epsilon_j)
    end

	p_hat = create_from_subset(p, get_p_hat(cfg.str_statistical_measure, mat_p), cfg.ll_p)
	vec_mean, vec_stdev = mean_and_stdev(mat_p)
    return ResultAbcSubSim(p_hat, vec_stdev, mat_p, l_epsilon, nbr_total_sim)
end


## With darray

#===

@everywhere function prior_sim_worker!(d_mat_p::DArray, p::Parameters, srl_p::SamplingRuleList, 
                           bl_p::BoundList, ll_p::LabelList) where Parameters
    local_mat_p = localpart(d_mat_p)
    for i = 1:size(local_mat_p)[2]
        p_i = sample(p, srl_p)
        check_bounds!(p_i, bl_p)
        local_mat_p[:,i] = to_vec(p_i, ll_p)::Array{Float64, 1}
    end
end

@everywhere function compute_distance_worker!(d_vec_dist::DArray, d_mat_p::DArray, 
                                 f::Function, g_exp::Array{pygmalion.Observer, 1}, 
                                 eta_so_exp::Array{Float64, 1}, 
                                 x0::State, p::Parameters, u::Control, 
                                 mn::ModellingNoise, on::ObservationNoise, 
                                 dist::Function, eta::Function, 
                                 srl_p::SamplingRuleList, bl_p::BoundList, 
                                 ll_p::LabelList) where 
                                    {State, Parameters, Control, 
                                    ModellingNoise, ObservationNoise}

    local_mat_p = localpart(d_mat_p)
    local_vec_dist = localpart(d_vec_dist)
    local_nbr_particles = length(local_vec_dist)
    for i = 1:local_nbr_particles
        p_i = create_from_subset(p, local_mat_p[:,i], ll_p)
        so_sim_i = simulate(f, g_exp, x0, u, p_i; mn = mn, on = on)
        local_vec_dist[i] = dist(eta(so_sim_i), eta_so_exp)
    end
end

function _abc_subsim(f, g, x0, u, p, mn, on, so_exp, cfg; eta_so_exp = nothing)
	# Get distance and summary statistics functions
	dist = distance_function(cfg.str_distance_function)
	eta = eta_function(cfg.eta_function)
	tml = get_merged_timeline(so_exp)
	g_exp = create_observation_function([ObserverModel(join(so_exp.oml,","),collect(tml))])
    eta_so_exp = (eta_so_exp != nothing) ? eta_so_exp : eta(so_exp)
    nbr_p = length(cfg.ll_p)
	ll_p = cfg.ll_p
	bl_p = cfg.bl_p
	srl_p = cfg.srl_p
	nbr_particles = cfg.nbr_particles
    P0 = cfg.P0
    std_prop = cfg.std_prop
    lgth_chain = Int(1/P0)
    nbr_seeds = Int(nbr_particles * P0)
    println("Final epsilon : $(cfg.epsilon)")

    # Init with prior sim
    d_mat_p = dzeros(nbr_p, cfg.nbr_particles)
    @sync for id_w in workers()
        @async remotecall_fetch(() -> prior_sim_worker!(d_mat_p, p, srl_p, bl_p, ll_p), id_w)
    end
    d_vec_dist = dzeros(cfg.nbr_particles)
    epsilon_j = Inf
    l_epsilon = convert(Array{Float64, 1}, [])
    while epsilon_j > cfg.epsilon
        mat_p = convert(Array, d_mat_p)
        @sync for id_w in workers()
            @async remotecall_fetch(() -> compute_distance_worker!(d_vec_dist, d_mat_p, f, g_exp, eta_so_exp, x0, p, u, mn, on, dist, eta, srl_p, bl_p, ll_p), id_w)
        end
        vec_dist = convert(Array, d_vec_dist)
        ind_sorted_vec_dist = sortperm(vec_dist)
        epsilon_j = 0.5(vec_dist[ind_sorted_vec_dist[nbr_seeds]]
                        +vec_dist[ind_sorted_vec_dist[nbr_seeds + 1]])
        println("Epsilon : $epsilon_j")
        new_mat_p = zeros(nbr_p, nbr_particles)
        @sync begin
            per_worker = div(nbr_seeds, nworkers()) 
            ind_begin = 0
            ind_end = 0
            for id_w in workers()
                t_id_w = id_w - workers()[1] + 1
                ind_begin_pa = (t_id_w-1) * lgth_chain * per_worker + 1
                ind_end_pa = (t_id_w) * lgth_chain * per_worker
                ind_begin_seeds = (t_id_w-1) * per_worker + 1
                ind_end_seeds = (t_id_w) * per_worker
                if id_w == workers()[end]
                    ind_end_pa = nbr_particles
                    ind_end_seeds = nbr_seeds
                end
                lgth_total_pa = ind_end_pa - ind_begin_pa + 1
                @show ind_begin_pa
                @show ind_end_pa
                @show lgth_total_pa
                @async new_mat_p[:,ind_begin_pa:ind_end_pa] = remotecall_fetch(() -> mma_worker(mat_p[:,ind_sorted_vec_dist[ind_begin_seeds:ind_end_seeds]], nbr_particles, P0, lgth_total_pa, epsilon_j, std_prop, f, g_exp, eta_so_exp, x0, p, u, mn, on, dist, eta, srl_p, bl_p, ll_p), id_w)
            end
        end
        d_mat_p = distribute(new_mat_p)
        l_epsilon = vcat(l_epsilon, epsilon_j)
    end

    mat_p = convert(Array, d_mat_p)
	p_hat = create_from_subset(p, get_p_hat(cfg.str_statistical_measure, mat_p), cfg.ll_p)
	vec_mean, vec_stdev = mean_and_stdev(mat_p)
    return ResultAbcSubSim(p_hat, vec_stdev, mat_p, l_epsilon)
end

===#

