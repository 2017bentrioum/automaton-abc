## Distance functions
# Require : Array{Float64, 1}
# Guarantee : Float64

function euclidean_distance(vec_sim::Union{Array{Float64,1}, Nothing}, vec_exp::Array{Float64,1})
    if vec_sim == nothing return Inf end
	if (length(vec_sim) != length(vec_exp))
		error("Distance : vectors are of different size")
	end
	res = 0.0
	for i = 1:length(vec_sim)
		res += (vec_sim[i] - vec_exp[i])^2
	end
    if isnan(res) return Inf end
    return sqrt(res)
end

const l_distance_function = ["euclidean_distance"]

function check_distance_function(name)
    if (typeof(name) <: String) && !(name in l_distance_function)
		error("Wrong name of distance function")
	end
end

function distance_function(name)
    if typeof(name) <: String
        if name == "euclidean_distance"
            return euclidean_distance
        end
    elseif(typeof(name) <: Function)
        return name
    else
        error("Wrong name of distance function")
    end
end

## Eta functions 
# Require : SystemObservation or Nothing
# Guarantee : Array{Float64, 1}

const l_eta_function = ["identity", "default_eta"]

function check_eta_function(name)
    if (typeof(name) <: String) && !(name in l_eta_function)
		error("Wrong name of eta function")
	end
end

function eta_function(name)
    if typeof(name) <: String
        if name == "identity"
            return identity
        elseif name == "periodicity"
            return periodicity
        elseif name == "default_eta"
            return default_eta
        else
            error("Wrong name of eta function")
        end
    elseif typeof(name) <: Function
        return name
    else 
        error("Wrong argument : eta function")
    end
end

function identity(so::Union{SystemObservation,Nothing}) where SystemObservation
    if so == nothing return  nothing end
    size_y_vec = 0
    for om in so.oml
        size_y_vec += length(get_timeline(so, om))
    end
    if VERSION >= v"0.7.0" vec_so = Array{Float64, 1}(undef, size_y_vec)
    else vec_so = Array{Float64, 1}(size_y_vec)
    end
    begin_index = 1
    end_index = 0
    for om in so.oml
        lgth_vec = length(get_timeline(so, om))
        end_index += lgth_vec
        vec_so[begin_index:end_index] = to_vec(so, om)
        begin_index += lgth_vec
    end
    return vec_so
end

function default_eta(so::Union{SystemObservation,Nothing}) where SystemObservation
    return [0.0]
end
