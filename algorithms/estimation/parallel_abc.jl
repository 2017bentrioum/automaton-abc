
if VERSION >= v"0.7.0"
    @everywhere using LinearAlgebra
    @everywhere using DistributedArrays
end

@everywhere include(ENV["PYGMALION_DIR"] * "algorithms/automata/helpers/checkers.jl")
@everywhere include(ENV["PYGMALION_DIR"] * "algorithms/estimation/helpers/metrics.jl")

struct ResultParallelAbc
    p_hat
    vec_stdev::Array{Float64, 2}
    mat_p::Array{Float64, 2}
    ratio_accept::Float64
    nbr_size_sample::Int64
    nbr_sim::Int64
    vec_dist::Array{Float64, 1}
    vec_dist2::Array{Float64, 1}
end

struct ConfigurationParallelAbc
    ll_p::LabelList
    srl_p::SamplingRuleList
    bl_p::BoundList
    size_sample::Int64
    nbr_it_max::Int64
    eta_function::Union{Label, Function}
    distance_function::Union{Label, Function}
    str_statistical_measure::Label
    epsilon::Float64
    ll_properties::LabelList
    dict_properties_values::Dict
    full_sim_eta::Bool
    eta_function2::Union{Label, Function}
    epsilon2::Float64
    distance_function2::Union{Label, Function}
end

ConfigurationParallelAbc(ll_p::LabelList, srl_p::SamplingRuleList, 
                         bl_p::BoundList, size_sample::Int64, nbr_it_max::Int64, 
                         eta_function::Union{Label, Function}, 
                         distance_function::Union{Label, Function},
                         str_statistical_measure::Label, epsilon::Float64) =
ConfigurationParallelAbc(ll_p, srl_p, bl_p, size_sample, nbr_it_max, eta_function,
                         distance_function, str_statistical_measure, 
                         epsilon, Array{Label,1}(), Dict(), false, "default_eta", Inf,
                         euclidean_distance)

ConfigurationParallelAbc(ll_p::LabelList, srl_p::SamplingRuleList, 
                         bl_p::BoundList, size_sample::Int64, nbr_it_max::Int64, 
                         eta_function::Union{Label, Function}, 
                         distance_function::Union{Label, Function},
                         str_statistical_measure::Label, epsilon::Float64,
                         eta_function2::Union{Label, Function}, epsilon2::Float64,
                         distance_function2::Union{Label, Function}) =
ConfigurationParallelAbc(ll_p, srl_p, bl_p, size_sample, nbr_it_max, eta_function,
                         distance_function, str_statistical_measure, 
                         epsilon, Array{Label,1}(), Dict(), false, eta_function2, epsilon2,
                         distance_function2)


ConfigurationParallelAbc(ll_p::LabelList, srl_p::SamplingRuleList, 
                         bl_p::BoundList, size_sample::Int64, nbr_it_max::Int64, 
                         eta_function::Union{Label, Function}, 
                         distance_function::Union{Label, Function},
                         str_statistical_measure::Label, epsilon::Float64,
                         full_sim_eta::Bool) =
ConfigurationParallelAbc(ll_p, srl_p, bl_p, size_sample, nbr_it_max, eta_function,
                         distance_function, str_statistical_measure, epsilon, 
                         Array{Label,1}(), Dict(), full_sim_eta, "default_eta", Inf,
                         euclidean_distance)

ConfigurationParallelAbc(ll_p::LabelList, srl_p::SamplingRuleList, 
                         bl_p::BoundList, size_sample::Int64, nbr_it_max::Int64, 
                         eta_function::Union{Label, Function}, 
                         distance_function::Union{Label, Function},
                         str_statistical_measure::Label, epsilon::Float64,
                         ll_properties::LabelList, dict_properties_values::Dict) =
ConfigurationParallelAbc(ll_p, srl_p, bl_p, size_sample, nbr_it_max, eta_function,
                         distance_function, str_statistical_measure, epsilon,
                         ll_properties_values, dict_properties_values, 
                         false, "default_eta", Inf, euclidean_distance)

function mode(vec::Vector{Float64})
	d = Dict()
	res = 0.0
	for k in vec
		if haskey(d, k)
			d[k] += 1
		else
			d[k] = 1
		end
	end
	max_counts = maximum(collect(values(d)))
	for k in keys(d)
		if d[k] == max_counts
			res = k
			break
		end
	end
	return res
end

function get_p_hat(str_sm::Label, mat_p)
	nb_p = size(mat_p)[1]
	vec_p = zeros(nb_p)
	if str_sm == "mean"
		for i = 1:nb_p
			vec_p[i] = mean(mat_p[i, :])
		end
	elseif str_sm == "mode"
		for i = 1:nb_p
			vec_p[i] = mode(mat_p[i, :])
		end
	else	
		error("Wrong name of statistical measure")
	end
	return vec_p
end

function parallel_abc(f::Function, g::ObserverList, 
                      x0::State, u::Control, p::Parameters,
                      mn::ModellingNoise, on::ObservationNoise, 
                      so_exp::SystemObservation, cfg::ConfigurationParallelAbc; 
                      eta_so_exp = nothing, eta_so_exp2 = nothing) where 
                       {State, Parameters, Control, 
                        ModellingNoise, ObservationNoise}
    if nprocs() == 1
        return par2_abc_threads(f, g, x0, u, p, mn, on, so_exp, cfg; eta_so_exp = eta_so_exp)
    end
    println("Begin parallel ABC with " * string(nworkers()) * " workers")
    return par2_abc(f, g, x0, u, p, mn, on, so_exp, cfg; eta_so_exp = eta_so_exp, eta_so_exp2 = eta_so_exp2)
end

@everywhere function task_worker!(d_mat_p::DArray, 
                                  d_vec_dist::DArray, d_vec_dist2::Union{Nothing,DArray},   
                                f::Function, 
                                g_exp::Array{pygmalion.Observer, 1}, 
                                so_exp::SystemObservation, 
                                eta_so_exp::Array{Float64, 1}, eta_so_exp2::Array{Float64, 1},
                                tml_exp::Array{Int, 1}, 
                                x0::State, p::Parameters,u::Control, 
                                mn::ModellingNoise, on::ObservationNoise, 
                                dist::Function, dist2::Function, 
                                eta::Function, eta2::Function, 
                                ll_p::LabelList, 
                                srl_p::SamplingRuleList, bl_p::BoundList, 
                                epsilon::Float64, epsilon2::Float64, 
                                ll_properties::LabelList, 
                                dict_properties_values::Dict,
                                full_sim_eta::Bool) where 
                                {State, Parameters, Control, 
                                 ModellingNoise, ObservationNoise}
    local_mat = localpart(d_mat_p)
	local_vec_dist = localpart(d_vec_dist)
    local_vec_dist2 = localpart(d_vec_dist2)
    nbr_vec_param = size(local_mat)[2]
    l_nbr_sim = zeros(nbr_vec_param)
    Threads.@threads for i = 1:nbr_vec_param
        if d_vec_dist2 == nothing 
            local_mat[:,i], local_vec_dist[i], _, nbr_sim = search_param(f, g_exp, so_exp, eta_so_exp, eta_so_exp2, tml_exp, x0, p, u, mn, on, dist, dist2, eta, eta2, ll_p, srl_p, bl_p, epsilon, epsilon2, ll_properties, dict_properties_values, full_sim_eta)
        else 
            local_mat[:,i], local_vec_dist[i], local_vec_dist2[i], nbr_sim = search_param(f, g_exp, so_exp, eta_so_exp, eta_so_exp2, tml_exp, x0, p, u, mn, on, dist, dist2, eta, eta2, ll_p, srl_p, bl_p, epsilon, epsilon2, ll_properties, dict_properties_values, full_sim_eta)
        end
        l_nbr_sim[i] = nbr_sim
    end
    return sum(l_nbr_sim)
end


@everywhere function search_param(f::Function, 
                                g_exp::Array{pygmalion.Observer, 1}, 
                                so_exp::SystemObservation, 
                                eta_so_exp::Array{Float64, 1}, eta_so_exp2::Array{Float64, 1},
                                tml_exp::Array{Int, 1}, 
                                x0::State, p::Parameters,u::Control, 
                                mn::ModellingNoise, on::ObservationNoise, 
                                dist::Function, dist2::Function, 
                                eta::Function, eta2::Function, ll_p::LabelList, 
                                srl_p::SamplingRuleList, bl_p::BoundList, 
                                epsilon::Float64, epsilon2::Float64,
                                ll_properties::LabelList, 
                                dict_properties_values::Dict, full_sim_eta::Bool) where 
                                {State, Parameters, Control, 
                                 ModellingNoise, ObservationNoise}
    so_sim_full, p_prime = nothing, nothing
    is_eta2 = (string(eta2) != "default_eta")
    bool_properties = true
    dist_sim = Inf
    dist_sim2 = Inf
    nbr_sim = 0
    while so_sim_full == nothing || dist_sim > epsilon || dist_sim2 > epsilon2 || !bool_properties
        p_prime = sample(p, srl_p)
        #check_bounds!(p_prime, bl_p)
        if !check_bounds(p_prime, bl_p)
            continue
        end
        so_sim_full = nothing
        eta_so_sim, eta_so_sim2 = nothing, nothing
        try
            so_sim_full = simulate(f, g_exp, x0, u, p_prime; mn = mn, on = on, full_timeline = true)
            if full_sim_eta
                eta_so_sim = eta(so_sim_full)
                eta_so_sim2 = eta2(so_sim_full)
            else
                eta_so_sim = eta(filter(so_sim_full, so_sim_full.oml, tml_exp))
                eta_so_sim2 = eta2(filter(so_sim_full, so_sim_full.oml, tml_exp))
            end
        catch err
            if isa(err, DomainError)
                #@warn "Computation error while simulating the model"
                continue
            end
            throw(err)
        end
        dist_sim = dist(eta_so_sim, eta_so_exp)
        dist_sim2 = is_eta2 ? dist2(eta_so_sim2, eta_so_exp2) : Inf
        bool_properties = check_properties(so_sim_full, ll_properties, dict_properties_values)
        nbr_sim += 1
    end
    vec_p_prime = to_vec(p_prime, ll_p)::Array{Float64, 1}
    return (vec_p_prime, dist_sim, dist_sim2, nbr_sim)
end

function par2_abc(f::Function, g::ObserverList, 
                  x0::State, u::Control, p::Parameters,
                  mn::ModellingNoise, on::ObservationNoise, 
                  so_exp::SystemObservation, cfg::ConfigurationParallelAbc; 
                  eta_so_exp = nothing, eta_so_exp2 = nothing) where 
                   {State, Parameters, Control, 
                   ModellingNoise, ObservationNoise}
    # Get distance and summary statistics functions
    dist = distance_function(cfg.distance_function)
    dist2 = distance_function(cfg.distance_function2)
    eta = eta_function(cfg.eta_function)
    eta2 = eta_function(cfg.eta_function2)
	tml = get_merged_timeline(so_exp)
    g_exp = create_observation_function([ObserverModel(join(so_exp.oml,","),collect(tml))])
    eta_so_exp = (eta_so_exp != nothing) ? eta_so_exp : eta(so_exp)
    eta_so_exp2 = (eta_so_exp2 != nothing) ? eta_so_exp2 : eta2(so_exp)
    ll_p, srl_p, bl_p = cfg.ll_p, cfg.srl_p, cfg.bl_p
    epsilon = cfg.epsilon
    epsilon2 = cfg.epsilon2
    ll_properties = cfg.ll_properties
    dict_properties_values = cfg.dict_properties_values
    nbr_sim = 1
    nbr_size_sample = 0
    nbr_p = length(cfg.ll_p)
    d_mat_p = distribute(zeros(nbr_p, cfg.size_sample); dist=(1, nworkers()))
    d_vec_dist = dzeros(cfg.size_sample)
    d_vec_dist2 = (string(eta2) != "default_eta") ? dzeros(cfg.size_sample) : nothing
    full_sim_eta = cfg.full_sim_eta
    l_nbr_sim = zeros(nworkers())
    @sync for id_w in workers()
        t_id_w = id_w - workers()[1] + 1
		@async l_nbr_sim[t_id_w] = remotecall_fetch(() -> task_worker!(d_mat_p, d_vec_dist, d_vec_dist2, f, g_exp, so_exp, eta_so_exp, eta_so_exp2, tml, x0, p, u, mn, on, dist, dist2, eta, eta2, ll_p, srl_p, bl_p, epsilon, epsilon2, ll_properties, dict_properties_values, full_sim_eta), id_w)
    end

    mat_p = convert(Array, d_mat_p)
    vec_dist = convert(Array, d_vec_dist)
    vec_dist2 = (d_vec_dist2 == nothing) ? zeros(0) : convert(Array, d_vec_dist2)
    ratio = - 1.0
    vec_mean, vec_stdev = mean_and_stdev(mat_p)
    p_hat = create_from_subset(p, get_p_hat(cfg.str_statistical_measure, mat_p), cfg.ll_p)
    r = ResultParallelAbc(p_hat, vec_stdev, mat_p, ratio, cfg.size_sample, sum(l_nbr_sim), vec_dist, vec_dist2)
    return r
end

function par2_abc_threads(f::Function, g::ObserverList, 
                  x0::State, u::Control, p::Parameters,
                  mn::ModellingNoise, on::ObservationNoise, 
                  so_exp::SystemObservation, cfg::ConfigurationParallelAbc; 
                  eta_so_exp = nothing, eta_so_exp2 = nothing) where 
                   {State, Parameters, Control, 
                   ModellingNoise, ObservationNoise}
    # Get distance and summary statistics functions
    dist = distance_function(cfg.distance_function)
    dist2 = distance_function(cfg.distance_function2)
    eta = eta_function(cfg.eta_function)
    eta2 = eta_function(cfg.eta_function2)
	tml = get_merged_timeline(so_exp)
    g_exp = create_observation_function([ObserverModel(join(so_exp.oml,","),collect(tml))])
    eta_so_exp = (eta_so_exp != nothing) ? eta_so_exp : eta(so_exp)
    eta_so_exp2 = (eta_so_exp2 != nothing) ? eta_so_exp2 : eta2(so_exp)
    ll_p, srl_p, bl_p = cfg.ll_p, cfg.srl_p, cfg.bl_p
    epsilon = cfg.epsilon
    epsilon2 = cfg.epsilon2
    ll_properties = cfg.ll_properties
    dict_properties_values = cfg.dict_properties_values
    nbr_size_sample = 0
    nbr_p = length(cfg.ll_p)
    mat_p = zeros(nbr_p, cfg.size_sample)
    vec_dist = zeros(cfg.size_sample)
    vec_dist2 = (string(eta2) != "default_eta") ? zeros(cfg.size_sample) : nothing
    full_sim_eta = cfg.full_sim_eta
    l_nbr_sim = zeros(cfg.size_sample)
    Threads.@threads for i = 1:cfg.size_sample
        if vec_dist2 == nothing
            mat_p[:,i], vec_dist[i], _, l_nbr_sim[i] = search_param(f, g_exp, so_exp, eta_so_exp, eta_so_exp2, tml, x0, p, u, mn, on, dist, dist2, eta, eta2, ll_p, srl_p, bl_p, epsilon, epsilon2, ll_properties, dict_properties_values, full_sim_eta)
        else
            mat_p[:,i], vec_dist[i], vec_dist2[i], l_nbr_sim[i] = search_param(f, g_exp, so_exp, eta_so_exp, eta_so_exp2, tml, x0, p, u, mn, on, dist, dist2, eta, eta2, ll_p, srl_p, bl_p, epsilon, epsilon2, ll_properties, dict_properties_values, full_sim_eta)
        end
    end
    ratio = - 1.0
    vec_mean, vec_stdev = mean_and_stdev(mat_p)
    vec_dist2 = (string(eta2) != "default_eta") ? vec_dist2 : zeros(0) 
    p_hat = create_from_subset(p, get_p_hat(cfg.str_statistical_measure, mat_p), cfg.ll_p)
    r = ResultParallelAbc(p_hat, vec_stdev, mat_p, ratio, cfg.size_sample, sum(l_nbr_sim), vec_dist, vec_dist2)
    return r
end

