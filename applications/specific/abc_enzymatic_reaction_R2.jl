
if VERSION >= v"0.7.0"
    @everywhere using Distributed
    @everywhere using Random
    using DelimitedFiles
end
@everywhere using pygmalion
include(ENV["PYGMALION_DIR"] * "etc/plot.jl")
@everywhere include(ENV["PYGMALION_DIR"] * "algorithms/automata/region_automaton_F_v2.jl")
include(ENV["PYGMALION_DIR"] * "algorithms/estimation/abc_pop_mc.jl")
pa = pyimport("matplotlib.patches")

function redirect_to_files(dofunc, outfile, errfile)
	open(outfile, "w") do out
		open(errfile, "w") do err
			redirect_stdout(out) do
				redirect_stderr(err) do
					dofunc()
				end
			end
		end
	end
end

@everywhere str_m = "enzymatic_reaction"
str_d = "abc_enzymatic_reaction_R2"
str_dir =  create_results_directory(; title = str_d)
@everywhere load_model(str_m)

on = nothing
mn = nothing

str_oml = "E,S,ES,P,R,time"
ll_om = split(str_oml, ",")

# Set-up
@everywhere global x0 = State(100, 100, 0, 0, 0, 0.0)
p_true = Parameters(1.0, 1.0, 1.0)
tml = 1:400
g_all = create_observation_function([ObserverModel(str_oml, tml)])
 
@everywhere global A_x1 = 50.0
@everywhere global A_x2 = 75.0
@everywhere global A_t1 = 0.05
@everywhere global A_t2 = 0.075
u = Control(A_t2)

so_exp = simulate(f, g_all, x0, u, p_true; on = on, full_timeline = true)

@everywhere function region_automaton_ss2(so::Union{SystemObservation, Nothing})
    if so == nothing
        return [Inf]
    end
    A = RegionAutomatonF_v2(A_x1, A_x2, A_t1, A_t2)
    S = read_so(A, so; x0 = x0)
    if S.loc != "l2"
       return [Inf]
    end
    if S.d == Inf
        @show "wtf"
        @show S
    end
    return [S.d]
end

# Config ABC
cfg_ll_p = ["k3"]
cfg_srl_p = [
             SamplingRule(["k3"], "uniform", (0.0, 100.0))
            ]
cfg_bl_p = [
            Bound("k3", 0.0, 100.0)
           ]
cfg_nbr_particles = 10000
cfg_eta_function = region_automaton_ss2
cfg_distance_function = "euclidean_distance"
cfg_str_statistical_measure = "mean"
cfg_l_epsilon = [0.0]
cfg_alpha = 0.5
cfg_kernel_type = "mv_normal"
cfg_eta_so_exp = [0.0]


redirect_to_files(str_dir * "abc_pmc.log", str_dir * "abc_pmc.err") do
    @show region_automaton_ss2(so_exp)
	@show cfg_ll_p
	@show cfg_srl_p
	@show A_x1, A_x2, A_t1, A_t2
    @show p_true

    cfg_abc = ConfigurationAbcPopMc(cfg_ll_p, cfg_srl_p, cfg_bl_p, cfg_nbr_particles, cfg_eta_function, cfg_distance_function, cfg_str_statistical_measure, cfg_l_epsilon, cfg_alpha, cfg_kernel_type, true)
    @timev r_abc = abc_pop_mc(f, g_all, x0, u, p_true, mn, on, so_exp, cfg_abc; eta_so_exp = cfg_eta_so_exp)
    @show r_abc.nbr_sim

    for (i, param) in enumerate(cfg_ll_p)
        fig2 = pygmalion_plot()
        title("Normed histogram $(param)")
        plt.hist(r_abc.mat_p_end[i,:], bins = 100, color = "red", density = true, label = "Hist. ABC")
        xlabel(param, fontsize = 14)
        PyPlot.locator_params(axis = "x", nbins = 4)
        savefig(str_dir * "abc_end_posterior_$(param).png", dpi=480)
        close()
    end

	for (i, param) in enumerate(cfg_ll_p)
        fig2 = pygmalion_plot()
        title("Normed histogram $(param)")
        plt.hist(r_abc.mat_p_end[i,:], bins = 50, color = "red", density = true, label = "Hist. ABC")
        xlabel(param, fontsize = 14)
        PyPlot.locator_params(axis = "x", nbins = 4)
        savefig(str_dir * "abc_end_posterior_$(param)_2.png", dpi=480)
        close()
    end


	cfg_nbr_p = length(cfg_ll_p)
	for i = 1:cfg_nbr_p
		for j = (i+1):cfg_nbr_p
			fig2 = pygmalion_plot()
			title("ABC posterior")
			plt.hist2d(r_abc.mat_p_end[i,:], r_abc.mat_p_end[j,:], bins = (50, 50))
			plt.colorbar()
			xlabel(cfg_ll_p[i], fontsize = 14)
			ylabel(cfg_ll_p[j], fontsize = 14)
			savefig(str_dir * "abc_end_$(i)_$(j)_posterior.png", dpi=480)
			close()

			fig2 = pygmalion_plot()
			title("ABC posterior")
			plt.hist2d(r_abc.mat_p_end[i,:], r_abc.mat_p_end[j,:], bins = (25, 25))
			plt.colorbar()
			xlabel(cfg_ll_p[i], fontsize = 14)
			ylabel(cfg_ll_p[j], fontsize = 14)
			savefig(str_dir * "abc_end_$(i)_$(j)_posterior_2.png", dpi=480)
			close()
		end
	end

    nbr_sim_rand = 10
    makedir(str_dir * "p_rand")
    A_R1 = RegionAutomatonF_v2(A_x1, A_x2, A_t1, A_t2)
    for i = 1:nbr_sim_rand
        id_p_rand = rand(1:cfg_nbr_particles)
        vec_p_rand = r_abc.mat_p_end[:,id_p_rand]
        p_rand = create_from_subset(p_true, vec_p_rand, cfg_ll_p)
        @timev so_abc_p_rand = simulate(f, g_all, x0, u, p_rand; mn = mn, on = on, full_timeline = true)
		@timev eta_so_rand = region_automaton_ss2(so_abc_p_rand)
        @show eta_so_rand
		makedir(str_dir * "p_rand/p$i")
        for om in so_abc_p_rand.oml
            fig = pygmalion_plot()
            title(string(p_rand))
            x = to_vec(so_abc_p_rand, "time")
            y = to_vec(so_abc_p_rand, om)
            plt.step(x, y, "ro--", marker="x", linewidth = 1.0)
            current_axis = fig.axes[1]
            rect = pa.Rectangle((A_R1.t1,A_R1.x1),(A_R1.t2-A_R1.t1),(A_R1.x2-A_R1.x1),linewidth=1.0,edgecolor="black",facecolor="none")
            current_axis.add_patch(rect)
            savefig(str_dir * "p_rand/p$i/" * om * "_sim.png", dpi=480)
            close()
        end
    end
end

