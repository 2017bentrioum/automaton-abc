
if VERSION >= v"0.7.0"
    @everywhere using Distributed
    @everywhere using Random
    using DelimitedFiles
end
@everywhere using pygmalion
include(ENV["PYGMALION_DIR"] * "etc/plot.jl")
@everywhere include(ENV["PYGMALION_DIR"] * "algorithms/automata/region_automaton_G_F.jl")
@everywhere include(ENV["PYGMALION_DIR"] * "algorithms/automata/helpers/probability_estimator.jl")
include(ENV["PYGMALION_DIR"] * "algorithms/estimation/abc_pop_mc.jl")
pa = pyimport("matplotlib.patches")

function redirect_to_files(dofunc, outfile, errfile)
	open(outfile, "w") do out
		open(errfile, "w") do err
			redirect_stdout(out) do
				redirect_stderr(err) do
					dofunc()
				end
			end
		end
	end
end

@everywhere str_m = "sir_ctmc"
str_d = "abc_sir_ctmc"
str_dir =  create_results_directory(; title = str_d)
@everywhere load_model(str_m)

on = nothing
mn = nothing

str_oml = "X_S,X_I,X_R,R,time"
ll_om = split(str_oml, ",")

# Set-up
@everywhere N_x0 = 100.0
@everywhere s0 = 0.95
@everywhere S_0 = floor(N_x0 * s0)
@everywhere global x0 = State(S_0, N_x0 - S_0, 0.0, 0.0, 0.0)
@show x0
p_true = Parameters(0.12 / N_x0, 0.05)
tml = 1:400
g_all = create_observation_function([ObserverModel(str_oml, tml)])
 
@everywhere global A_x1 = 1.0
@everywhere global A_x2 = 100.0
@everywhere global A_x3 = 0.0
@everywhere global A_x4 = 0.0
@everywhere global A_t1 = 0.0
@everywhere global A_t2 = 100.0
@everywhere global A_t3 = 100.0
@everywhere global A_t4 = 120.0
u = Control(A_t4)

so_exp = simulate(f, g_all, x0, u, p_true; on = on, full_timeline = true)

@everywhere function region_automaton_ss3(so::Union{SystemObservation, Nothing})
    if so == nothing
        return [Inf]
    end
    A = RegionAutomatonG_F(A_x1, A_x2, A_x3, A_x4, A_t1, A_t2, A_t3, A_t4)
    S = read_so(A, so; x0 = x0, om_G = "X_I", om_F = "X_I")
    if S.loc != "l2_F"
       return [Inf]
    end
    if S.d == Inf
        @show "wtf"
        @show S
    end
    return [S.dtotal]
end

# Config ABC
#cfg_ll_p = ["ki", "kr"]
cfg_ll_p = ["kr"]
cfg_srl_p = [
             #SamplingRule(["ki"], "uniform", (0.005 / N_x0, 0.3 / N_x0))
             SamplingRule(["kr"], "uniform", (0.005, 0.2))
            ]
cfg_bl_p = [
            #Bound("ki", 0.0, 0.3 / N_x0)
            Bound("kr", 0.0, 0.2)
           ]
cfg_nbr_particles = 1000
cfg_eta_function = region_automaton_ss3
cfg_distance_function = "euclidean_distance"
cfg_str_statistical_measure = "mean"
cfg_l_epsilon = [0.0]
cfg_alpha = 0.6
cfg_kernel_type = "mv_normal"
cfg_eta_so_exp = [0.0]

# Test on one simulation
p_test = p_true
so_test = simulate(f, g_all, x0, u, p_test; on = on, full_timeline = true)

A_GF = RegionAutomatonG_F(A_x1, A_x2, A_x3, A_x4, A_t1, A_t2, A_t3, A_t4)
A_F = RegionAutomatonF_v2(A_x3, A_x4, A_t3, A_t4)
A_G = RegionAutomatonG_v2(A_x1, A_x2, A_t1, A_t2)

for om in so_test.oml
    fig = pygmalion_plot()
    title(string(p_test))
    x = to_vec(so_test, "time")
    y = to_vec(so_test, om)
    plt.step(x, y, "ro--", marker="x", linewidth = 1.0)
    current_axis = fig.axes[1]
    if om == "X_I"
        rect = pa.Rectangle((A_GF.t1,A_GF.x1),(A_GF.t2-A_GF.t1),(A_GF.x2-A_GF.x1),linewidth=1.0,edgecolor="blue",facecolor="none")
        current_axis.add_patch(rect)
    end
    if om == "X_I"
        rect2 = pa.Rectangle((A_GF.t3,A_GF.x3),(A_GF.t4-A_GF.t3),(A_GF.x4-A_GF.x3),linewidth=1.0,edgecolor="green",facecolor="none")
        current_axis.add_patch(rect2)
    end
    ylim(0, 100)
    xlim(0, 1.01*A_GF.t4)
    savefig(str_dir * om * "_sim.png", dpi=480)
    close()
end

verb = false

@show read_so(A_GF, so_test; om_G = "X_I", om_F = "X_I", verbose = verb)
@show read_so(A_G, so_test; om = "X_I", verbose = verb)
@show read_so(A_F, so_test; om = "X_I", verbose = verb)

redirect_to_files(str_dir * "abc_pmc.log", str_dir * "abc_pmc.err") do
    @show region_automaton_ss3(so_exp)
	@show cfg_ll_p
	@show cfg_srl_p
	@show A_x1, A_x2, A_t1, A_t2
    @show p_true

    cfg_abc = ConfigurationAbcPopMc(cfg_ll_p, cfg_srl_p, cfg_bl_p, cfg_nbr_particles, cfg_eta_function, cfg_distance_function, cfg_str_statistical_measure, cfg_l_epsilon, cfg_alpha, cfg_kernel_type, true)
    @timev r_abc = abc_pop_mc(f, g_all, x0, u, p_true, mn, on, so_exp, cfg_abc; eta_so_exp = cfg_eta_so_exp)
    @show r_abc.nbr_sim
    for (i, param) in enumerate(cfg_ll_p)
        fig2 = pygmalion_plot()
        title("Normed histogram $(param)")
        plt.hist(r_abc.mat_p_end[i,:], bins = 100, color = "red", density = true, label = "Hist. ABC", weights = r_abc.weights)
        xlabel(param, fontsize = 14)
        PyPlot.locator_params(axis = "x", nbins = 4)
        savefig(str_dir * "abc_end_posterior_$(param).png", dpi=480)
        close()
    end

	for (i, param) in enumerate(cfg_ll_p)
        fig2 = pygmalion_plot()
        title("Normed histogram $(param)")
        plt.hist(r_abc.mat_p_end[i,:], bins = 50, color = "red", density = true, label = "Hist. ABC", weights = r_abc.weights)
        xlabel(param, fontsize = 14)
        PyPlot.locator_params(axis = "x", nbins = 4)
        savefig(str_dir * "abc_end_posterior_$(param)_2.png", dpi=480)
        close()
    end


	cfg_nbr_p = length(cfg_ll_p)
	for i = 1:cfg_nbr_p
		for j = (i+1):cfg_nbr_p
			fig2 = pygmalion_plot()
			title("ABC posterior")
			plt.hist2d(r_abc.mat_p_end[i,:], r_abc.mat_p_end[j,:], bins = (50, 50), weights = r_abc.weights)
			plt.colorbar()
			xlabel(cfg_ll_p[i], fontsize = 14)
			ylabel(cfg_ll_p[j], fontsize = 14)
			savefig(str_dir * "abc_end_$(i)_$(j)_posterior.png", dpi=480)
			close()

			fig2 = pygmalion_plot()
			title("ABC posterior")
			plt.hist2d(r_abc.mat_p_end[i,:], r_abc.mat_p_end[j,:], bins = (25, 25), weights = r_abc.weights)
			plt.colorbar()
			xlabel(cfg_ll_p[i], fontsize = 14)
			ylabel(cfg_ll_p[j], fontsize = 14)
			savefig(str_dir * "abc_end_$(i)_$(j)_posterior_2.png", dpi=480)
			close()
		end
	end

    nbr_sim_rand = 10
    makedir(str_dir * "p_rand")
    for i = 1:nbr_sim_rand
        id_p_rand = rand(1:cfg_nbr_particles)
        vec_p_rand = r_abc.mat_p_end[:,id_p_rand]
        p_rand = create_from_subset(p_true, vec_p_rand, cfg_ll_p)
        so_abc_p_rand = simulate(f, g_all, x0, u, p_rand; mn = mn, on = on, full_timeline = true)
		eta_so_rand = region_automaton_ss3(so_abc_p_rand)
        @show eta_so_rand
		makedir(str_dir * "p_rand/p$i")
        for om in so_abc_p_rand.oml
            fig = pygmalion_plot()
            title(string(p_rand))
            x = to_vec(so_abc_p_rand, "time")
            y = to_vec(so_abc_p_rand, om)
            plt.step(x, y, "ro--", marker="x", linewidth = 1.0)
            current_axis = fig.axes[1]
            if om == "X_I"
                rect = pa.Rectangle((A_GF.t1,A_GF.x1),(A_GF.t2-A_GF.t1),(A_GF.x2-A_GF.x1),linewidth=1.0,edgecolor="blue",facecolor="none")
                current_axis.add_patch(rect)
            end
            if om == "X_I"
                rect2 = pa.Rectangle((A_GF.t3,A_GF.x3),(A_GF.t4-A_GF.t3),(A_GF.x4-A_GF.x3),linewidth=1.0,edgecolor="green",facecolor="none")
                current_axis.add_patch(rect2)
            end
            ylim(0, 100)
            xlim(0, 1.01*A_GF.t4)
            savefig(str_dir * "p_rand/p$i/" * om * "_sim.png", dpi=480)
            close()
        end
    end
end

