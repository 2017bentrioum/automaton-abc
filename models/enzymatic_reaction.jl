
import Distributions: Exponential
import LinearAlgebra: normalize!

mutable struct State
    E::Int64
    S::Int64
    ES::Int64
    P::Int64
    R::Int64
    time::Float64
end

State() = State(0, 0, 0, 0, 0, 0.0)
Base.copy(x::State) = State(x.E, x.S, x.ES, x.P, x.R, x.time)

mutable struct Parameters
    k1::Float64
    k2::Float64
    k3::Float64
end

struct Control
    ctmc_time_end::Float64
end

state2vec(x::State) = [x.E, x.S, x.ES, x.P]
function update_x_nu!(x::State, nu)
    x.E += nu[1]
    x.S += nu[2]
    x.ES += nu[3]
    x.P += nu[4]
end

function f(n::Int, xn::State, u::Control, p::Parameters)
    a1 = p.k1 * xn.E * xn.S
    a2 = p.k2 * xn.ES
    a3 = p.k3 * xn.ES
    l_a = [a1, a2, a3]
    asum = sum(l_a)
    if asum == 0.0
        xnplus1 = copy(xn)
        #xnplus1.time += 0.01 * u.ctmc_time_end
        xnplus1.time = u.ctmc_time_end
        xnplus1.R = 0
        return xnplus1
    end
    l_nu = [[-1, -1, 1, 0], [1, 1, -1, 0], [1, 0, -1, 1]]
    u1, u2 = rand(), rand()
    tau = - log(u1) / asum
    b_inf = 0.0
    b_sup = a1
    reaction = 0
    for i = 1:3
        if b_inf < asum*u2 < b_sup
            reaction = i
            break
        end
        b_inf += l_a[i]
        b_sup += l_a[i+1]
    end
 
    xnplus1 = copy(xn)
    update_x_nu!(xnplus1, l_nu[reaction])
    xnplus1.R = reaction
    xnplus1.time += tau
    
    return xnplus1
end

@simple_ctmc_observation_function()

