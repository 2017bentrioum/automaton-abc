
import LinearAlgebra: normalize!

mutable struct State
    X_S::Float64
    X_I::Float64
    X_R::Float64
    R::Float64
    time::Float64
end

State() = State(0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
Base.copy(x::State) = State(x.X_S, x.X_I, x.X_R, x.R, x.time)
state2vec(x::State) = [x.X_S, x.X_I, x.X_R]

mutable struct Parameters
    ki::Float64
    kr::Float64
end

struct Control
    ctmc_time_end::Float64
end

struct ObservationNoise
end

struct ModellingNoise
end

function update_x_nu!(x::State, nu::Array{Float64, 1})
    x.X_S += nu[1]
    x.X_I += nu[2]
    x.X_R += nu[3]
end

function f(n::Int, xn::State, u::Control, p::Parameters)
    a1 = p.ki * xn.X_S * xn.X_I
    a2 = p.kr * xn.X_I
    l_a = [a1, a2]
    asum = sum(l_a)
    if asum == 0.0
        xnplus1 = copy(xn)
        xnplus1.time = u.ctmc_time_end
        xnplus1.R = 0
        return xnplus1
    end
    l_nu = [[-1.0, 1.0, 0.0], [0.0, -1.0, 1.0]]
    u1, u2 = rand(), rand()
    tau = - log(u1) / asum
    b_inf = 0.0
    b_sup = a1
    reaction = 0
    for i = 1:2
        if b_inf < asum*u2 < b_sup
            reaction = i
            break
        end
        b_inf += l_a[i]
        b_sup += l_a[i+1]
    end
 
    xnplus1 = copy(xn)
    update_x_nu!(xnplus1, l_nu[reaction])
    xnplus1.R = convert(Float64, reaction)
    xnplus1.time += tau
    
    return xnplus1
end

@simple_ctmc_observation_function()

