
import Distributions: Exponential
import LinearAlgebra: normalize!

mutable struct State
    nucleotides::Float64
    amino_acids::Float64
    G::Float64
    T::Float64
    S::Float64
    V::Float64
    R::Float64
    time::Float64
end

State() = State(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
Base.copy(x::State) = State(x.nucleotides, x.amino_acids, x.G, x.T, x.S, x.V, x.R, x.time)


mutable struct Parameters
    k1::Float64
    k2::Float64
    k3::Float64
    k4::Float64
    k5::Float64
    k6::Float64
    cn::Float64
    ca::Float64
end

struct Control
    ctmc_time_end::Float64
end

state2vec(x::State) = [x.nucleotides, x.amino_acids, x.G, x.T, x.S, x.V] 

function update_x_nu!(x::State, nu::Array{Float64, 1})
    x.nucleotides += nu[1] 
    x.amino_acids += nu[2] 
    x.G += nu[3] 
    x.T += nu[4] 
    x.S += nu[5] 
    x.V += nu[6] 
end

function f(n::Int, xn::State, u::Control, p::Parameters)
    a1 = p.k1 * xn.T * p.cn
    a2 = p.k2 * xn.G * p.cn
    a3 = p.k3 * xn.T * p.cn * p.ca
    a4 = p.k4 * xn.T
    a5 = p.k5 * xn.S
    a6 = p.k6 * xn.G * xn.S
    l_a = [a1, a2, a3, a4, a5, a6]
    asum = sum(l_a)
    if asum == 0.0
        xnplus1 = copy(xn)
        xnplus1.time = u.ctmc_time_end
        xnplus1.R = 0.0
        return xnplus1
    end
    l_nu = [[0.0, 0.0, 1.0, 0.0, 0.0, 0.0],
            [0.0, 0.0, -1.0, 1.0, 0.0, 0.0], 
            [0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0], # 3
            [0.0, 0.0, 0.0, -1.0, 0.0, 0.0],
            [0.0, 0.0, 0.0, 0.0, -1.0, 0.0],
            [0.0, 0.0, -1.0, 0.0, -1.0, 1.0]] # 6

    u1, u2 = rand(), rand()
    tau = - log(u1) / asum
    b_inf = 0.0
    b_sup = a1
    reaction = 0.0
    for i = 1:6
        if b_inf < asum*u2 < b_sup
            reaction = i
            break
        end
        b_inf += l_a[i]
        b_sup += l_a[i+1]
    end
    
    xnplus1 = copy(xn)
    update_x_nu!(xnplus1, l_nu[reaction])
    xnplus1.R = convert(Float64, reaction)
    xnplus1.time += tau
    
    return xnplus1
end

@observation_function()

