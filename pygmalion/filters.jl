
function effective_sample_size(wl::ValueList)
    n_eff = 0.0
    wls = sum(wl)
    if wls > 0.0
        n_eff = wls ^ 2 / sum(wl .^ 2)
    end
    return n_eff
end

function random_number_list(n::Int)
    lin = collect(linspace(1.0, n, n))
    return (lin - 0.5) / n
end

function resample_number_particles(wl::ValueList, rnl::ValueList)
    n = length(wl)
    nbr = zeros(n)
    iw = 1
    ip = 1
    wcum = wl[iw]
    while ip <= n
        if rnl[ip] <= wcum
            nbr[iw] += 1
            ip += 1
        else
            iw += 1
            if iw == n
                println("ip = $ip, n = $n")
            end
            wcum += wl[iw]
        end
    end
    return nbr
end

function create_indices(wl::ValueList)
    nbr_particles = length(wl)
    il = zeros(Int, nbr_particles)
    flg = zeros(nbr_particles)
    rnl = random_number_list(nbr_particles)
    nl = resample_number_particles(wl, rnl)
    for i = 1:nbr_particles
        if nl[i] != 0
            il[i] = i
            nl[i] -= 1
            flg[i] = 1
        end
    end
    ind = 1
    i = 1
    while i <= nbr_particles
        if nl[i] != 0
            while flg[ind] != 0
                ind += 1
            end
            il[ind] = i
            nl[i] -= 1
            flg[ind] = 1
        else
            i += 1
        end
    end
    return il
end

function update_from_indices!(xl, ll_x::LabelList, il)
    for i = 1:length(xl)
        vec_x_i = to_vec(xl[il[i]], ll_x)
        fill_with_subset!(xl[i], vec_x_i, ll_x)
    end
end

function update_from_indices!(xl, il)
    for i = 1:length(xl)
        xl[i] = deepcopy(xl[il[i]])
    end
end

function mask_matrix(nbr_p, nbr_x, type_cov)
    mat_mask = zeros(nbr_p + nbr_x, nbr_p + nbr_x)
    if type_cov == "diag"
        mat_mask = eye(nbr_p + nbr_x)
    elseif type_cov == "block"
        mat_mask = cat([1,2], ones(nbr_p, nbr_p), ones(nbr_x, nbr_x))
    elseif type_cov == "full"
        mat_mask = ones(nbr_p + nbr_x)
    elseif type_cov == "parameters"
        mat_mask = cat([1,2], ones(nbr_p, nbr_p), zeros(nbr_x, nbr_x))
    end
    return mat_mask
end

function bandwidth_cr(n, d)
    cx = (4.0 / (2.0 + d)) ^ (1.0 / (4.0 + d))
    cf = 1.0 / n ^ (1.0 / (4.0 + d))
    # return 0.3 * cf
    return cx * cf
end

function init_weights(method, ll_p, pl, srl_p, r_gls)
	nbr_particles = length(pl)
	if method == "uniform"
		wl = [1.0 for i = 1:nbr_particles]
	elseif method == "gls"
		p_gls = r_gls.p_hat
		vec_p_gls = to_vec(p_gls, ll_p)[1:(end-1)]
		mat_cov_p_gls = Array(Hermitian(r_gls.mat_cov_p))
		d = MvNormal(vec_p_gls, mat_cov_p_gls)
		wl = [reduce(*, map(sr -> pdf(p, sr), srl_p)) * (pdf(d, to_vec(p, ll_p)[1:(end-1)])) for p in pl]
	else
		error("Weight initialization method in CPF unknown")
	end
	#println("wl in init_w() = ")
	#println(wl[1:10])

	return wl
end

function create_weights(on, sol, so_exp, tf)
	log_wl_k = map(so -> log_pdf(on, so, so_exp, tf), sol)
	wl_k = map(log_wl_k_i -> exp(log_wl_k_i), log_wl_k)
	if sum(wl_k) == 0.0
		println("translate")
		translate_log_wl_k = maximum(log_wl_k)
		println("tr = ")
		println(translate_log_wl_k)
		println("log_wl_k = ")
		println(log_wl_k[1:10])
		log_wl_k = map(log_wl_k_i -> log_wl_k_i - translate_log_wl_k, log_wl_k)
		wl_k = map(log_wl_k_i -> exp(log_wl_k_i), log_wl_k)
	end
	return wl_k
end

function convolution!(mat_px::Matrix, ll_p::LabelList, ll_x::LabelList, cfg)
    nbr_p, nbr_x, nbr_particles = length(ll_p), length(ll_x), size(mat_px, 2)
    nbr_px = nbr_p + nbr_x
    println("nbr px = ")
	println(nbr_px)
	mat_px_cov = zeros(nbr_px, nbr_px)
    r = rank(mat_px)
    if r < size(mat_px, 1)
        println("rank insufficient")
        vec_px_mean = vec(mean(mat_px, 2))
        mat_px_cov = diagm(0.2 * vec_px_mean) .^ 2
        mat_cov = bandwidth_cr(nbr_particles, nbr_p + nbr_x) ^ 2 * mat_px_cov[1:nbr_p,1:nbr_p]
        # println(mat_cov)
        d = MvNormal(zeros(nbr_p), mat_cov)
        for i = 1:nbr_particles
            mat_px[1:nbr_p,i] += cfg.bandwidth_adjustment * rand(d)
        end

    else
        vec_px_mean, mat_px_cov = mean_and_covariance(mat_px)
        # vec_px_mean = mean(mat_px, 2)
        # mat_px_cov = cov(mat_px, 2)
        # println("isposdef(mat_px_cov)")
        # println(isposdef(mat_px_cov))
        # println("bandwidth_cr(nbr_particles, nbr_p + nbr_x) ^ 2")
        # println(bandwidth_cr(nbr_particles, nbr_p + nbr_x) ^ 2)
        mat_cov = bandwidth_cr(nbr_particles, nbr_p + nbr_x) ^ 2 * mat_px_cov
        # println("isposdef(mat_cov)")
        # println(isposdef(mat_cov))
        # println(mat_cov)
        d = MvNormal(zeros(nbr_p + nbr_x), mat_cov)
        for i = 1:nbr_particles
            mat_px[:,i] += cfg.bandwidth_adjustment * rand(d)
        end
    end
end

struct Bound
    label::Label
    lower::Value
    upper::Value
end

const BoundList = Array{Bound, 1}

function check_bounds!(x, bl::BoundList)
    for b in bl
        s = Symbol(b.label)
        v = getfield(x, s)
        if v > b.upper
            setfield!(x, s, b.upper)
        elseif v < b.lower
            setfield!(x, s, b.lower)
        end
    end
end

function check_bounds(x, bl::BoundList)
    for b in bl
        s = Symbol(b.label)
        v = getfield(x, s)
        if v > b.upper
            return false
        elseif v < b.lower
            return false
        end
    end
    return true
end

function check_bounds!(xl::T, bl::BoundList) where {T <: Array}
    for b in bl
        for i = 1:length(xl)
            s = Symbol(b.label)
            v = getfield(xl[i], s)
            if v > b.upper
                setfield!(xl[i], s, b.upper)
            elseif v < b.lower
                setfield!(xl[i], s, b.lower)
            end
        end
    end
end
