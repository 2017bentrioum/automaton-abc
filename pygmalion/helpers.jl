
function om_findfirst(om::AbstractString, oml::Array{AbstractString, 1})
    for i = 1:length(oml)
        if om == oml[i]
            return i
        end
    end
    return nothing
end

function retrieve_subset(x, ll::LabelList)
    nbr = length(ll)
    vec = Array{Value}(nbr)
    for i = 1:nbr
        vec[i] = getfield(x, Symbol(ll[i]))
    end
    return vec
end

function create_from_subset(x0, vec::Vector, ll::LabelList)
    nbr = length(ll)
    x = deepcopy(x0)
    for i = 1:nbr
        setfield!(x, Symbol(ll[i]), vec[i])
    end
    return x
end

function create_from_subset(x0, vec::Vector)
    x = deepcopy(x0)
    for i = 1:length(vec)
        setfield!(x, fieldnames(x)[i], vec[i])
    end
    return x
end

function create_from_subset(x0,
                            mat::Matrix,
                            ll::LabelList)
    nbr, nbr_samples = size(mat)
    xl = Array{typeof(x0)}(nbr_samples)
    for j = 1:nbr_samples
        xl[j] = create_from_subset(x0, mat[:,j], ll)
    end
    return xl
end

function copy_subset!(x_to,
                      x_from,
                      ll::LabelList)
    nbr = length(ll)
    for i = 1:nbr
        setfield!(x_to, Symbol(ll[i]), getfield(x_from, Symbol(ll[i])))
    end
end

function create_from_subset(p0,
                            x0,
                            vec::Array{Value, 1},
                            ll_p::LabelList,
                            ll_x::LabelList)
    nbr_p = length(ll_p)
    nbr_x = length(ll_x)
    p = deepcopy(p0)
    x = deepcopy(x0)
    for i = 1:nbr_p
        setfield!(p, Symbol(ll_p[i]), vec[i])
    end
    for i = 1:nbr_x
        setfield!(x, Symbol(ll_x[i]), vec[nbr_p + i])
    end
    return p, x
end

function create_from_subset(p0,
                            x0,
                            mat::Matrix,
                            ll_p::LabelList,
                            ll_x::LabelList)
    nbr_samples = size(mat, 2)
    pl = Array{typeof(p0)}(nbr_samples)
    xl = Array{typeof(x0)}(nbr_samples)
    for j = 1:nbr_samples
        pl[j], xl[j] = create_from_subset(p0, x0, mat[:,j], ll_p, ll_x)
    end
    return pl, xl
end

function create_from_subset(p0,
                            x0,
                            vecl::Array{Array{Value, 1}, 1},
                            ll_p::LabelList,
                            ll_x::LabelList)
    nbr_samples = length(vecl)
    pl = Array{typeof(p0)}(nbr_samples)
    xl = Array{typeof(x0)}(nbr_samples)
    for j = 1:nbr_samples
        pl[j], xl[j] = create_from_subset(p0, x0, vecl[j], ll_p, ll_x)
    end
    return pl, xl
end

function field_copy!(x1,
                     x2)
    for n in fieldnames(x2)
        setfield!(x1, n, getfield(x2, n))
    end
end

function fill_with_subset!(x,
                           vec::Vector,
                           ll::LabelList)
    nbr = length(ll)
    for i = 1:nbr
        setfield!(x, Symbol(ll[i]), vec[i])
    end
end

function fill_with_subset!(x,
                           v,
                           ll::LabelList)
    setfield!(x, Symbol(ll[1]), v)
end

function fill_with_subset!(x,
                           vec::Array{Value, 1},
                           ll::LabelList)
    nbr = length(ll)
    for i = 1:nbr
        setfield!(x, Symbol(ll[i]), vec[i])
    end
end

function fill_with_subset!(p,
                           x,
                           vec::Array{Value, 1},
                           ll_p::LabelList,
                           ll_x::LabelList)
    nbr_p = length(ll_p)
    nbr_x = length(ll_x)
    for i = 1:nbr_p
        setfield!(p, Symbol(ll_p[i]), vec[i])
    end
    for i = 1:nbr_x
        setfield!(x, Symbol(ll_x[i]), vec[nbr_p+i])
    end
end

function fill_with_subset!(pl,
                           xl,
                           vecl::Array{Array{Value, 1}, 1},
                           ll_p::LabelList,
                           ll_x::LabelList)
    for j = 1:length(pl)
        fill_with_subset!(pl[j], xl[j], vecl[j], ll_p, ll_x)
    end
end

function fill_with_subset!(xl,
                           vecl::Array{Array{Value, 1}, 1},
                           ll::LabelList)
    for j = 1:length(xl)
        fill_with_subset!(xl[j], vecl[j], ll)
    end
end

function fill_with_subset!(xl,
                           mat::Array{Value, 2},
                           ll::LabelList)
    for j = 1:length(xl)
        fill_with_subset!(xl[j], vec(mat[:,j]), ll)
    end
end

function fill_with_subset!(pl,
                           xl,
                           mat::Array{Value, 2},
                           ll_p::LabelList,
                           ll_x::LabelList)
    for j = 1:length(pl)
        fill_with_subset!(pl[j], xl[j], vec(mat[:,j]), ll_p, ll_x)
    end
end

function system_observation_list(edl::Array{Tuple})
    nbr_ed = length(edl)
    sol = Array(SystemObservation, nbr_ed)
    for i = 1:nbr_ed
        sol[i] = edl[i][3]
    end
    return sol
end

function vec_to_dict(vec::Vector,
                     ll::LabelList)
    d = OrderedDict{AbstractString, Value}()
    assert(length(vec) == length(ll))
    for i = 1:length(vec)
        d[ll[i]] = vec[i]
    end
    return d
end

function dict_to_vec(d::Dict)
    v = Array(Value, 0)
    for k in keys(d)
        push!(v, d[k])
    end
    return v
end

function add_backslashes(str)
    return replace(str, "_", "\\_")
end

function isrange(v::Timeline)
    return v == collect(v[1]:v[end])
end

function str2bool(v::AbstractString)
    return lowercase(v) in ["yes", "true", "t", "1"]
end

function to_str(a::Array{Value})
    str_a = ""
    for e in a
        str_a *= string(e) * ";"
    end
    return str_a[1:end-1]
end
