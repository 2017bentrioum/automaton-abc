import DelimitedFiles: readdlm

function save_to_csv(so::SystemObservation,
                     filename::AbstractString)
    file = open(filename, "w")
    nbr_om = length(so.oml)
    for i = 1:nbr_om
        nbr_obs = length(so.otll[i])
        for j = 1:nbr_obs
            write(file, so.oml[i] * "," * string(so.otll[i][j][1]) * "," * string(so.otll[i][j][2])[2:end-1] * "\n")
        end
    end
    close(file)
end

function save_to_csv(sol::Array{SystemObservation},
                     filename::AbstractString)
    nbr_so = length(sol)
    for i = 1:nbr_so
        save_to_csv(sol[i], filename[1:end-4] * "_$i.csv")
    end
end

function load_from_csv(filename::AbstractString)
    #file = open(filename, "r")
    table = readdlm(filename, ',')
    oml = convert(Array{AbstractString,1}, map(string, unique(table[:,1])))
    so = SystemObservation(oml)
    for i = 1:size(table,1)
        vl = Value[]
        j = 3
        while j <= length(table[i,:]) && isa(table[i,j], Real)
            push!(vl, table[i,j])
            j += 1
        end
        om = string(table[i,1])
        t = round(Int, table[i,2])
        so.set(om, t, vl)
    end
    #close(file)
    return so
end

function save_to_obs(so::SystemObservation,
                     filename::AbstractString)
    file = open(filename, "w")
    write(file, "CONTEXT:" * so.title * "\n")
    nbr_om = length(so.oml)
    for i = 1:nbr_om
        write(file, "--------------------------------------------------------------------------------" * "\n")
        write(file, "OBSERVER:" * so.oml[i] * "\n")
        write(file, "--------------------------------------------------------------------------------" * "\n")
        write(file, "t\t" * so.oml[i] * "\n")
        write(file, "--------------------------------------------------------------------------------" * "\n")
        nbr_obs = length(so.otll[i])
        for j = 1:nbr_obs
            write(file, string(so.otll[i][j][1]) * "\t")
            size = length(so.otll[i][j][2])
            for k = 1:size-1
                if isnan(so.otll[i][j][2][k])
                    write(file, @sprintf("%.3e", so.otll[i][j][2][k]), "\t\t")
                else
                    write(file, @sprintf("%.3e", so.otll[i][j][2][k]), "\t")
                end
            end
            write(file, @sprintf("%.3e", so.otll[i][j][2][end]), "\n")
        end
        write(file, "\n")
    end
    close(file)
end

function save_to_obs(sol::Array{SystemObservation},
                     filename::AbstractString)
    nbr_so = length(sol)
    for i = 1:nbr_so
        save_to_obs(sol[i], filename[1:end-4] * "_$i.obs")
    end
end

function load_from_obs(filename::AbstractString)
    base = basename(filename)
    title = base[1:end-4]
    file = open(filename, "r")
    oml = AbstractString[]
    for line in eachline(file)
        if length(line) > 8 && line[1:8] == "OBSERVER"
            push!(oml, string(Meta.parse(line[10:end])))
        end
    end
    close(file)
    file = open(filename, "r")
    so = SystemObservation(oml; title = title)
    om = ""
    t = 0
    vl = Value[]
    for line in eachline(file)
        if length(line) > 0
            if length(line) > 8 && line[1:8] == "OBSERVER"
                om = string(Meta.parse(line[10:end]))
            end
            if isdigit(line[1])
                list = readdlm(IOBuffer(line))
                t = round(Int, list[1])
                vl = list[2:end]
                so.set(om, t, vl)
            end
        end
    end
    close(file)
    return so
end

function get_path_separator()
    sep = ""
	if VERSION >= v"0.7.0"
		bool_unix = occursin("/", pwd())
		bool_win = occursin("\\", pwd()) 
    else
        bool_unix = contains(pwd(), "/")
        bool_win = contains(pwd(), "\\") 
    end
	if bool_unix
        sep = "/"
    elseif bool_win
        sep = "\\"
    end
    return sep
end

function realdirname(str_dir::AbstractString)
    sep = get_path_separator()
    str_path = realpath(str_dir)
    str_dir_real = ""
    if isdir(str_path * sep)
        str_dir_real = str_path * sep
    else
        str_dir_real = dirname(str_path) * sep
    end
    return str_dir_real
end

function realbasename(str_file::AbstractString)
    str_base_real = ""
    if isfile(str_file)
        str_base_real = basename(str_file)
    end
    return str_base_real
end

# Works everywhere if you update LOAD_PATH in ~/.juliarc.jl
function get_abs_pygmalion_dir()
	if haskey(ENV, "PYGMALION_DIR")
		return ENV["PYGMALION_DIR"]
	end
	for path in LOAD_PATH
		try
			readdir(path)
		catch y
			if isa(y, SystemError)
				continue
			end
		end
		if "pygmalion.jl" in readdir(path)
			return path * "/../"
		end
	end
end

function get_pygmalion_directory()
	try
		x = split(pwd(), get_path_separator())
		while x[end] != "pygmalion-julia"
			pop!(x)
		end
		str = ""
		for i = 1:length(x)
			str = str * x[i] * "/"
		end
		return str
	catch y 
		if isa(y, BoundsError)
			return get_abs_pygmalion_dir()
		end
	end
end

function makedir(str_dir)
    if !isdir(str_dir)
		# Not really elegant code..
		# I had issues about making compatible isdefined (julia 0.6) and @isdefined (julia 0.7)
		# It's the only thing that I found for not raising an error for both versions.
		is_mpi = false
		try
			is_mpi = isa(MPI, Module)
		catch err
			if isa(err, UndefVarError)
				is_mpi = false
			end
		end
		if is_mpi
            if Main.MPI.Comm_rank(Main.MPI.COMM_WORLD) == 0
                mkdir(str_dir)
            end
        else
            mkdir(str_dir)
        end
    end
end

function create_results_directory(; title::AbstractString = "")
    new_directory = ""
    if title != ""
        new_directory = get_pygmalion_directory() * "results/" * Libc.strftime("%Y-%m-%d_%Hh%Mm%Ss", floor(Int, time())) * "_" * title * "/"
    else
        new_directory = get_pygmalion_directory() * "results/" * Libc.strftime("%Y-%m-%d_%Hh%Mm%Ss", floor(Int, time())) * "/"
    end
    makedir(new_directory)
    return new_directory
end

function get_last_results_directory()
    path = get_pygmalion_directory() * "results/"
    return last(sort!(map(f -> (joinpath(path, f), Dates.unix2datetime(mtime(joinpath(path, f)))), filter(x -> isdir(path * x), readdir(path))), by = last))[1] * "/"
end

function get_observation_files(dir)
    return [f for f in filter(f -> endswith(f, ".obs"), readdir(dir))]
end

function copy(str_file::AbstractString,
              str_dir::AbstractString;
              name = "")
    str_base = ""
    if name == ""
        str_base = realbasename(str_file)
    else
        str_base = name
    end
    if !isfile(realdirname(str_dir) * str_base)
		# Not really elegant code..
		# I had issues about making compatible isdefined (julia 0.6) and @isdefined (julia 0.7)
		# It's the only thing that I found for not raising an error for both versions.
		is_mpi = false
		try
			is_mpi = isa(MPI, Module)
		catch err
			if isa(err, UndefVarError)
				is_mpi = false
			end
		end
        if is_mpi
            if Main.MPI.Comm_rank(Main.MPI.COMM_WORLD) == 0
                cp(str_file, realdirname(str_dir) * str_base)
            end
        else
            cp(str_file, realdirname(str_dir) * str_base)
        end
    end
end

function save(r, str_dir::AbstractString)
    str_file = str_dir * "result.txt"
    file = open(str_file, "w")
    str_dir = realdirname(str_file)
    iso = 1
    for s in fieldnames(typeof(r))
        if isa(getfield(r, s), Dict) || isa(getfield(r, s), OrderedDict)
            write(file, "\n")
            d = getfield(r, s)
            for k in keys(d)
                write(file, string(k) * " = " * string(d[k]) * "\n")
            end
        elseif string(typeof(getfield(r, s))) == "Parameters"
            write_parameters(getfield(r, s), str_dir * string(s) * ".jl")
        elseif isa(getfield(r, s), SystemObservation)
            save_to_obs(getfield(r, s), str_dir * getfield(r, s).title * ".obs")
            iso += 1
        elseif isa(getfield(r, s), Array{SystemObservation})
            for so in getfield(r, s)
                save_to_obs(so, str_dir * so.title * ".obs")
                iso += 1
            end
        else
            write(file, string(s) * " = " * string(getfield(r, s)) * "\n")
        end
    end
    close(file)
end

function save(d::Dict,
              str_file::AbstractString)
    file = open(str_file, "w")
    for k in keys(d)
        write(file, string(k) * " = " * string(d[k]) * "\n")
    end
    close(file)
end

function print_data(x::Float64)
    @printf("%.3e", x)
    println()
end

function print_data(x)
    ll_fn = map(string, fieldnames(typeof(x)))
    lengths = map(length, ll_fn)
    s = maximum(lengths)
    for i = 1:length(ll_fn)
        print(" $(ll_fn[i]) ")
        for j = 1:s-lengths[i]
            print(" ")
        end
        print("= ")
        field = getfield(x, Symbol(ll_fn[i]))
        for j = 1:length(field)-1
            @printf("%.3e", field[j])
            print(" ")
        end
        @printf("%.3e", field[end])
        print("\n")
    end
    println()
end

function print_data(x, ll)
    ll_fn = intersect(map(string, fieldnames(x)), ll)
    lengths = map(length, ll_fn)
    s = maximum(lengths)
    for i = 1:length(ll_fn)
        print(" $(ll_fn[i]) ")
        for j = 1:s-lengths[i]
            print(" ")
        end
        print("= ")
        @printf("%.3e", getfield(x, Symbol(ll_fn[i])))
        print("\n")
    end
    println()
end

function print_data(x::Array{Int,1}; mode = "col")
    nbr_elem = length(x)
    if mode == "row"
        for i = 1:nbr_elem
            if x[i] >= 0 print(" ") end
            print(x[i])
            if i < nbr_elem
                for k = 1:10-length(string(x[i]))
                    print(" ")
                end
            end
        end
        print("\n")
    elseif mode == "col"
        for i = 1:nbr_elem
            if x[i] >= 0 print(" ") end
            println(x[i])
        end
        print("\n")
    end
end

function print_data(x::Array{Value,1}; mode = "col")
    nbr_elem = length(x)
    if mode == "col"
        for i = 1:nbr_elem
            if x[i] >= 0 print(" ") end
            if isnan(x[i]) print("       NaN") else @printf("%.3e", x[i]) end
            print("\n")
        end
    elseif mode == "row"
        for i = 1:nbr_elem
            if x[i] >= 0 print(" ") end
            if isnan(x[i]) print("       NaN") else @printf("%.3e", x[i]) end
            if i < nbr_elem print(" ") end
        end
    else
        error("Wrong name of mode $mode.")
    end
    println()
end

function print_data(x::Matrix)
    nbr_rows = size(x, 1)
    nbr_cols = size(x, 2)
    for i = 1:nbr_rows
        for j = 1:nbr_cols
            if x[i,j] >= 0 print(" ") end
            if isnan(x[i,j]) print("       NaN") else @printf("%.3e", x[i,j]) end
            if j < nbr_cols
                print(" ")
            end
        end
        print("\n")
    end
    println()
end

function print_data(x::Array{Array{T,1},1}) where {T <: Real}
    nbr_rows = size(x, 1)
    for i = 1:nbr_rows
        nbr_cols = size(x[i], 1)
        for j = 1:nbr_cols
            if x[i][j] >= 0 print(" ") end
            if isnan(x[i][j]) print("       NaN") else @printf("%.3e", x[i][j]) end
            if j < nbr_cols
                print(" ")
            end
        end
        print("\n")
    end
    println()
end

function print_data(x::Array{Array{Int,1},1})
    nbr_rows = size(x, 1)
    for i = 1:nbr_rows
        nbr_cols = size(x[i], 1)
        for j = 1:nbr_cols
            if x[i][j] >= 0 print(" ") end
            print(x[i][j])
            if j < nbr_cols
                for k = 1:10-length(string(x[i][j]))
                    print(" ")
                end
            end
        end
        print("\n")
    end
    println()
end

function write_vector(x, filename)
    file = open(filename, "w")
    for i = 1:length(fieldnames(x))-1
        write(file, "$(getfield(x, fieldnames(x)[i])),")
    end
    write(file, "$(getfield(x, fieldnames(x)[end]))\n")
    close(file)
end

function load_vector(filename)
    return readdlm(filename, ',')
end

function write_parameters(p, filename; namevar = "p")
    file = open(filename, "w")
    write(file, "\n")
    for n in fieldnames(p)
        write(file, "$(string(n)) = $(getfield(p, n))\n")
    end
    write(file, "\n")
    write(file, "$namevar = Parameters(")
    for i = 1:length(fieldnames(p))-1
        write(file, "$(string(fieldnames(p)[i])), ")
    end
    write(file, "$(string(fieldnames(p)[end])))\n")
    close(file)
end

function write_state(x, filename)
    file = open(filename, "w")
    write(file, "\n")
    for n in fieldnames(x)
        write(file, "$(string(n)) = $(getfield(x, n))\n")
    end
    write(file, "\n")
    write(file, "x = State(")
    for i = 1:length(fieldnames(x))-1
        write(file, "$(string(fieldnames(x)[i])), ")
    end
    write(file, "$(string(fieldnames(x)[i])))\n")
    close(file)
end

function print_vec(rank::Int, l::Label, v::Vector)
    print("$rank,$l,$(string(vec(v))[2:end-1])\n")
end

function write_vec(file, l::Label, v::Vector)
    write(file, "$l,$(string(vec(v))[2:end-1])\n")
end

function load_model(str_m)
    include(get_pygmalion_directory() * "models/" * str_m * ".jl")
end

function load_parameters(str_m, str_x; dest = "")
    str_file = get_pygmalion_directory() * "database/" * "parameters/" * str_m * "/" * str_x * ".jl"
    str_dir_res = dest == "" ? get_last_results_directory() : dest
    include(str_file)
    copy(str_file, str_dir_res; name = "file-parameters.jl")
end

function load_state(str_m, str_x; dest = "")
    str_file = get_pygmalion_directory() * "database/" * "state/" * str_m * "/" * str_x * ".jl"
    str_dir_res = dest == "" ? get_last_results_directory() : dest
    include(str_file)
    copy(str_file, str_dir_res; name = "file-state.jl")
end

function load_control(str_m, str_x; dest = "")
    str_file = get_pygmalion_directory() * "database/" * "control/" * str_m * "/" * str_x * ".jl"
    str_dir_res = dest == "" ? get_last_results_directory() : dest
    include(str_file)
    copy(str_file, str_dir_res; name = "file-control.jl")
end

function load_observations(str_m, str_x; dest = "")
    str_file = get_pygmalion_directory() * "database/" * "observations/" * str_m * "/" * str_x * ".obs"
    str_dir_res = dest == "" ? get_last_results_directory() : dest
    copy(str_file, str_dir_res; name = "file-target.obs")
    return load_from_obs(str_file)
end

function load_configuration(str_a, str_m, str_x)
    str_est = ["amwg", "cpf", "enkf", "gls", 	"kf", "pmmh", "sir", "ukf", "wls"]
    str_sa = ["gsobol", "sobol", "sobol_gls"]
    str_type_alg = str_a in str_est ? "estimation" : "sensitivity-analysis"
    str_file = get_pygmalion_directory() * "database/" * "configuration/" * str_type_alg * "/" * str_a * "/" * str_m * "/" * str_x * ".jl"
    str_dir_res = get_last_results_directory()
    include(str_file)
    copy(str_file, str_dir_res; name = "file-configuration.jl")
end
