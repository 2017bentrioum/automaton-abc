
function log_uniform_pdf(x::Real, a::Real, b::Real)
    try
        return - log(b - a)
    catch
        error("x = $x a = $a b = $b")
    end
end

function log_normal_pdf(x::Real, mu::Real, sigma::Real)
    try
        return - log(sigma * sqrt(2 * pi)) - (x - mu) ^ 2 / (2 * sigma ^ 2)
    catch err
        error("x = $x mu = $mu sigma = $sigma")
    end
end

function log_mvnormal_pdf(vec_x::Array{T,1}, vec_mu::Array{T,1}, mat_sigma::Array{T,2}) where T <: Real
    n = length(vec_x)
    try
        return - 0.5 * (n * log(2 * pi) + log(abs(det(mat_sigma))) + ((vec_x - vec_mu)' * inv(mat_sigma) * (vec_x - vec_mu))[1])
    catch
        error("vec_x = $vec_x vec_mu = $vec_mu mat_sigma = $mat_sigma")
    end
end

function log_lognormal_pdf(x::Real, mu::Real, sigma::Real)
    if x == 0.0
        return - Inf
    end
    try
        return - log(x * sigma * sqrt(2 * pi)) - (log(x) - mu) ^ 2 / (2 * sigma ^ 2)
    catch
        error("x = $x mu = $mu sigma = $sigma")
    end
end

function inverse_gamma_pdf(x::Real, alpha::Real, beta::Real)
    try
        return beta ^ alpha * x ^ (- alpha - 1) / gamma(alpha) * exp(- beta / x)
    catch
        error("x = $x alpha = $alpha beta = $beta")
    end
end

function chi_squared_pdf(x::Real, k::Real; a = 1)
    try
        return (a * x) ^ (k / 2 - 1) * exp(- a * x / 2) / 2 ^ (k / 2) / gamma(k / 2)
    catch
        error("x = $x k = $k a = $a")
    end
end

uniform_pdf(x::Real, a::Real, b::Real) = exp(log_uniform_pdf(x, a, b))

normal_pdf(x::Real, mu::Real, sigma::Real) = exp(log_normal_pdf(x, mu, sigma))

mvnormal_pdf(vec_x::Array{T,1}, vec_mu::Array{T,1}, mat_sigma::Array{T,2}) where T <: Real = exp(log_mvnormal_pdf(vec_x, vec_mu, mat_sigma)) 

lognormal_pdf(x::Real, mu::Real, sigma::Real) = exp(log_lognormal_pdf(x, mu, sigma))

uniform_kernel(x::Real ; bandwidth = 1.0) = 1 / bandwidth * uniform_pdf(x / bandwidth, -1.0, 1.0)

gaussian_kernel(x::Real ; bandwidth = 1.0) = 1 / bandwidth * normal_pdf(x / bandwidth, 0.0, 1.0)

function gaussian_kernel(vec_x::Array{T,1}; bandwidth = 1.0) where T <: Real
    dim = length(vec_x)
    return 1 / bandwidth ^ dim * mvnormal_pdf(vec_x / bandwidth, zeros(dim), eye(dim))
end

function gaussian_kernel(vec_x::Array{T,1}, mat_bandwidth::Array{T,2}) where T <: Real
    dim = length(vec_x)
    mat_sqrt = sqrtm(mat_bandwidth)
    return 1 / det(mat_sqrt) * mvnormal_pdf(mat_sqrt * vec_x, zeros(dim), eye(dim))
end

function to_vec(p)
    fn = fieldnames(p)
    x = zeros(length(fn))
    for i = 1:length(fn)
        x[i] = getfield(p, fn[i])
    end
    return x
end

function autodiff(f::Function, g::ObserverList, x0, u, p::Parameters, ll_p::LabelList; mn = nothing, on = nothing) where Parameters
    m(x::Vector) = to_vec(simulate(f, g, x0, u, create_from_subset(p, x, ll_p); mn = mn, on = on))
    return ForwardDiff.jacobian(m, to_vec(p, ll_p))
end

function jacobian(f::Function,
                  g::ObserverList,
                  x0::State,
                  u::Control,
                  p::Parameters,
                  ll_p::LabelList,
                  delta::Value;
                  mn = nothing,
                  on = nothing) where {State, Control, Parameters}
    so = simulate(f, g, x0, u, p; mn = mn, on = on)
    vec_so = to_vec(so)
    p_j_plus = p
    p_j_minus = p
    vec_p = to_vec(p, ll_p)
    vec_p_j_plus = to_vec(p_j_plus, ll_p)
    vec_p_j_minus = to_vec(p_j_minus, ll_p)
    nbr_obs = length(vec_so)
    nbr_p = length(vec_p)
    mat_j = zeros(nbr_obs, nbr_p)
    for j = 1:nbr_p
        vec_p_j_plus[j] *= 1 + delta
        vec_p_j_minus[j] *= 1 - delta
        p_j_plus = create_from_subset(p, vec_p_j_plus, ll_p)
        p_j_minus = create_from_subset(p, vec_p_j_minus, ll_p)
        so_j_plus = simulate(f, g, x0, u, p_j_plus; mn = mn, on = on)
        so_j_minus = simulate(f, g, x0, u, p_j_minus; mn = mn, on = on)
        vec_so_j_plus = to_vec(so_j_plus)
        vec_so_j_minus = to_vec(so_j_minus)
        for i = 1:nbr_obs
            mat_j[i,j] = (vec_so_j_plus[i] - vec_so_j_minus[i]) / (2 * delta * vec_p[j])
        end
        vec_p_j_plus[j] /= 1 + delta
        vec_p_j_minus[j] /= 1 - delta
    end
    return mat_j
end

function jacobian(f::Function,
                  g::ObserverList,
                  x0::State,
                  u::Control,
                  p::Parameters,
                  ll_p::LabelList,
                  delta::Value,
                  vec_timeline::Array{Array{Int,1}};
                  mn = nothing,
                  on = nothing) where {State, Control, Parameters}
    so_sim = simulate(f, g, x0, u, p; mn = mn, on = on)
    vec_timeline_sim, vec_data_sim = to_mat(so_sim, vec_timeline)
    vec_so_sim = vcat(vec_data_sim...)
    p_j_plus = p
    p_j_minus = p
    vec_p = to_vec(p, ll_p)
    vec_p_j_plus = to_vec(p_j_plus, ll_p)
    vec_p_j_minus = to_vec(p_j_minus, ll_p)
    nbr_val = length(vec_so_sim)
    nbr_p = length(vec_p)
    mat_j = zeros(nbr_val, nbr_p)
    for j = 1:nbr_p
        vec_p_j_plus[j] *= 1 + delta
        vec_p_j_minus[j] *= 1 - delta
        p_j_plus = create_from_subset(p, vec_p_j_plus, ll_p)
        p_j_minus = create_from_subset(p, vec_p_j_minus, ll_p)
        so_j_plus = simulate(f, g, x0, u, p_j_plus; mn = mn, on = on)
        so_j_minus = simulate(f, g, x0, u, p_j_minus; mn = mn, on = on)
        vec_timeline_j_plus, vec_data_j_plus = to_mat(so_j_plus, vec_timeline)
        vec_timeline_j_minus, vec_data_j_minus = to_mat(so_j_minus, vec_timeline)
        vec_so_j_plus = vcat(vec_data_j_plus...)
        vec_so_j_minus = vcat(vec_data_j_minus...)
        for i = 1:nbr_val
            mat_j[i,j] = (vec_so_j_plus[i] - vec_so_j_minus[i]) / (2 * delta * vec_p[j])
        end
        vec_p_j_plus[j] /= 1 + delta
        vec_p_j_minus[j] /= 1 - delta
    end
    return mat_j
end

function jacobian(f::Function,
                  edl::Array{Tuple},
                  p::Parameters,
                  ll_p::LabelList,
                  delta::Value;
                  mn = nothing,
                  on = nothing) where Parameters
    p_j_plus = p
    p_j_minus = p
    vec_p = to_vec(p, ll_p)
    vec_p_j_plus = to_vec(p_j_plus, ll_p)
    vec_p_j_minus = to_vec(p_j_minus, ll_p)
    nbr_p = length(vec_p)
    nbr_obs = get_total_number_of_observations(edl)
    mat_j = zeros(nbr_obs, nbr_p)
    for j = 1:nbr_p
        vec_p_j_plus[j] *= 1 + delta
        vec_p_j_minus[j] *= 1 - delta
        p_j_plus = create_from_subset(p, vec_p_j_plus, ll_p)
        p_j_minus = create_from_subset(p, vec_p_j_minus, ll_p)
        vec_so_j_plus = []
        vec_so_j_minus = []
        for ed in edl
            x0, u, so_exp, g = ed
            so_j_plus = simulate(f, g, x0, u, p_j_plus; mn = mn, on = on)
            so_j_minus = simulate(f, g, x0, u, p_j_minus; mn = mn, on = on)
            vec_so_j_plus = [vec_so_j_plus; to_vec(so_j_plus)]
            vec_so_j_minus = [vec_so_j_minus; to_vec(so_j_minus)]
        end
        for i = 1:nbr_obs
            mat_j[i,j] = (vec_so_j_plus[i] - vec_so_j_minus[i]) / (2 * delta * vec_p[j])
        end
        vec_p_j_plus[j] /= 1 + delta
        vec_p_j_minus[j] /= 1 - delta
    end
    return mat_j
end

function make_positive!(x)
    for i = 1:length(x) x[i] = max(0, x[i]) end
end

function weighted_mean(wl::Array{T,1}, vec::Array{T,1}) where {T <: Real}
    assert(length(wl) == length(vec))
    s = length(wl)
    m = 0.0
    for j = 1:s
        m += wl[j] * vec[j]
    end
    return m
end

function weighted_mean(wl::Array{T,1}, mat::Array{T,2}) where T <: Real
    assert(length(wl) == size(mat, 2))
    nbr = size(mat, 1)
    vec_mean = zeros(nbr)
    for i = 1:nbr
        vec_mean[i] = weighted_mean(wl, vec(mat[i,:]))
    end
    return vec_mean
end

function mean_and_variance(mat::Array{T,2}) where T <: Real
    if VERSION >= v"0.7.0"
        vec_mean = mean(mat, dims=2)
        vec_var = mean(mat .^ 2, dims=2) - vec_mean .^ 2
    else
        vec_mean = mean(mat, 2)
        vec_var = mean(mat .^ 2, 2) - vec_mean .^ 2
    end
    return vec(vec_mean), vec_var
end

function mean_and_variance(vec_l::Array{Array{T,1},1}) where {T <: Real}
    vec_mean = mean(vec_l)
    vec_var = mean(vec_l .^ 2) - vec_mean .^ 2
    return vec(vec_mean), vec_var
end

function mean(sol::Array{SystemObservation})
    so_mean = sol[1]
    so_mean.title = "estimated_states_mean"
    nbr_so = length(sol)
    for i = 2:nbr_so
        so_mean += sol[i]
    end
    so_mean /= nbr_so
    return so_mean
end

function mean_and_variance(sol::Array{SystemObservation})
    so_mean = mean(sol)
    so_var = mean(sol ^ 2.0) - so_mean ^ 2.0
    make_positive!(so_var)
    so_var.title = "estimated_states_variance"
    return so_mean, so_var
end

function weighted_mean_and_variance(wl::Array{T,1}, mat::Array{T}) where {T <: Real}
    vec_mean = weighted_mean(wl, mat)
    vec_var = weighted_mean(wl, mat .^ 2) - vec_mean .^ 2
    return vec_mean, vec_var
end

function mean_and_stdev(mat::Array{T}) where {T <: Real}
    vec_mean, vec_var = mean_and_variance(mat)
    make_positive!(vec_var)
    return vec_mean, sqrt.(vec_var)
end

function mean_and_stdev(sol::Array{SystemObservation})
    so_mean, so_var = mean_and_variance(sol)
    so_var.title = "estimated_states_stdev"
    return so_mean, sqrt.(so_var)
end

function weighted_mean_and_stdev(wl::Array{T,1}, mat::Array{T}) where {T <: Real}
    vec_mean, vec_var = weighted_mean_and_variance(wl, mat)
    return vec_mean, sqrt.(vec_var)
end

function mean_and_covariance(mat::Array{T}) where {T <: Real}
    vec_mean = mean(mat, 2)
    s1, s2 = size(mat)
    mat_cov = zeros(s1, s1)
    for j = 1:s2
        mat_cov += (mat[:,j] - vec_mean) * (mat[:,j] - vec_mean)'
    end
    mat_cov /= (s2 - 1)
    return vec(vec_mean), mat_cov
end

function weighted_mean_and_covariance(wl_mean::Array{T,1}, wl_cov::Array{T,1}, mat::Array{T}) where {T <: Real}
    assert(length(wl_mean) == size(mat, 2))
    vec_mean = weighted_mean(wl_mean, mat)
    s1, s2 = size(mat)
    mat_cov = zeros(s1, s1)
    for j = 1:s2
        mat_cov += wl_cov[j] * (mat[:,j] - vec_mean) * (mat[:,j] - vec_mean)'
    end
    return vec_mean, mat_cov
end

function correlation(mat_1::Array{T,2}, mat_2::Array{T,2}) where {T <: Real}
    assert(size(mat_1, 2) == size(mat_2, 2))
    nbr_elem = size(mat_1, 2)
    s1 = size(mat_1, 1)
    s2 = size(mat_2, 1)
    vec_mean_1 = mean(mat_1, 2)
    vec_mean_2 = mean(mat_2, 2)
    mat_cor = zeros(s1, s2)
    for j = 1:nbr_elem
        mat_cor += (mat_1[:,j] - vec_mean_1) * (mat_2[:,j] - vec_mean_2)'
    end
    mat_cor /= (nbr_elem - 1)
    return mat_cor
end

function weighted_correlation(wl_mean::Array{T,1}, wl_cov::Array{T,1}, mat_1::Array{T,2}, mat_2::Array{T,2}) where {T <: Real}
    assert(size(mat_1, 2) == size(mat_2, 2))
    nbr_elem = size(mat_1, 2)
    s1 = size(mat_1, 1)
    s2 = size(mat_2, 1)
    vec_mean_1 = weighted_mean(wl_mean, mat_1)
    vec_mean_2 = weighted_mean(wl_mean, mat_2)
    mat_cor = zeros(s1, s2)
    for j = 1:nbr_elem
        mat_cor += wl_cov[j] * (mat_1[:,j] - vec_mean_1) * (mat_2[:,j] - vec_mean_2)'
    end
    return mat_cor
end

function sigma_points_and_weights(vec_mean::Array{T,1}, mat_cov::Array{T,2}) where {T <: Real}
    dim = length(vec_mean)
    ## kappa = 0.0
    kappa = 3.0 - dim
    alpha = 1.0
    beta = 2.0
    lambda = max(alpha ^ 2 * (dim + kappa) - dim, 0.0)
    assert(lambda >= 0 && dim + lambda > 0)
    mat_sqrt = real(sqrtm((dim + lambda) * mat_cov))
    vec_sigma_points = [zeros(dim) for i = 1:2*dim+1]
    vec_weights_mean = zeros(2 * dim + 1)
    vec_weights_cov = zeros(2 * dim + 1)
    vec_sigma_points[1] = vec_mean
    vec_weights_mean[1] = lambda / (dim + lambda)
    vec_weights_cov[1] = lambda / (dim + lambda) + (1 - alpha ^ 2 + beta)
    for j = 1:dim
        vec_sigma_points[1+j] = vec_mean + mat_sqrt[:,j]
        vec_weights_mean[1+j] = 0.5 / (dim + lambda)
        vec_weights_cov[1+j] = 0.5 / (dim + lambda)
        vec_sigma_points[1+dim+j] = vec_mean - mat_sqrt[:,j]
        vec_weights_mean[1+dim+j] = 0.5 / (dim + lambda)
        vec_weights_cov[1+dim+j] = 0.5 / (dim + lambda)
    end
    return vec_sigma_points, vec_weights_mean, vec_weights_cov
end

function rmsep(so1::SystemObservation, om1::Label, so2::SystemObservation, om2::Label)
    i1 = get_index(so1, om1)
    i2 = get_index(so2, om2)
    msep = 0.0
    nbr_obs = 0
    for j1 = 1:length(so1.otll[i1])
        t = so1.otll[i1][j1][1]
        for j2 = 1:length(so2.otll[i2])
            if so2.otll[i2][j2][1] == t
                msep += (so1.otll[i1][j1][2][1] - so2.otll[i2][j2][2][1]) ^ 2
                nbr_obs += 1
            end
        end
    end
    return sqrt(msep / nbr_obs)
end

function rmsep(so1::SystemObservation, so2::SystemObservation, om::Label)
    return rmsep(so1, om, so2, om)
end

function ef(so1::SystemObservation, om1::Label, so2::SystemObservation, om2::Label)
    i1 = get_index(so1, om1)
    i2 = get_index(so2, om2)
    mean = 0.0
    num = 0.0
    den = 0.0
    nbr_obs = 0
    for j1 = 1:length(so1.otll[i1])
        t = so1.otll[i1][j1][1]
        for j2 = 1:length(so2.otll[i2])
            if so2.otll[i2][j2][1] == t
                num += (so1.otll[i1][j1][2][1] - so2.otll[i2][j2][2][1]) ^ 2
                mean += so2.otll[i2][j2][2][1]
                nbr_obs += 1
            end
        end
    end
    mean /= nbr_obs
    for j1 = 1:length(so1.otll[i1])
        t = so1.otll[i1][j1][1]
        for j2 = 1:length(so2.otll[i2])
            if so2.otll[i2][j2][1] == t
                den += (so1.otll[i1][j1][2][1] - mean) ^ 2
            end
        end
    end
    return 1 - num / den
end

function symmetrize!(m::Matrix)
    for i = 2:size(m,1)
        for j = 1:i-1
            if abs((m[i,j] - m[j,i]) / m[i,j]) < 1e-5
                m[i,j] = m[j,i]
            elseif m[i,j] != 0
                warn("Matrix highly unsymmetric: m[$i,$j] = $(m[i,j]), m[$j,$i] = $(m[j,i])")
            end
        end
    end
end
