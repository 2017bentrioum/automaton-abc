
struct Noise
    ll_det::LabelList
    ll_sto::LabelList
    kind::Label
    vl::Tuple
end

const NoiseList = Array{Noise}

function noise!(x::State,
                n::Noise) where State
    if n.kind == "additive-normal"
        setfield!(x, Symbol(n.ll_sto[1]), getfield(x, Symbol(n.ll_det[1])) + rand(Normal(n.vl[1], n.vl[2])))
    elseif n.kind == "multiplicative-normal"
        setfield!(x, Symbol(n.ll_sto[1]), getfield(x, Symbol(n.ll_det[1])) * (1 + rand(Normal(n.vl[1], n.vl[2]))))
    elseif n.kind == "multiplicative-lognormal"
        setfield!(x, Symbol(n.ll_sto[1]), getfield(x, Symbol(n.ll_det[1])) * exp(rand(Normal(n.vl[1], n.vl[2]))))
    elseif n.kind == "additive-uniform"
        setfield!(x, Symbol(n.ll_sto[1]), getfield(x, Symbol(n.ll_det[1])) + rand(Uniform(n.vl[1], n.vl[2])))
    elseif n.kind == "multiplicative-uniform"
        setfield!(x, Symbol(n.ll_sto[1]), getfield(x, Symbol(n.ll_det[1])) * (1 + rand(Uniform(n.vl[1], n.vl[2]))))
    else
        error("Wrong name of kind in noise.")
    end
end

function noise(x::State,
               n::Noise) where State
    vec = Value[]
    if n.kind == "additive-normal"
        push!(vec, getfield(x, Symbol(n.ll_det[1])) + rand(Normal(n.vl[1], n.vl[2])))
    elseif n.kind == "multiplicative-normal"
        # push!(vec, getfield(x, Symbol(n.ll_det[1])) * (1 + rand(Normal(n.vl[1], n.vl[2]))))
        x_det = getfield(x, Symbol(n.ll_det[1]))
        vec = x_det .* (1 .+ rand(Normal(n.vl[1], n.vl[2]), length(x_det)))
    elseif n.kind == "multiplicative-lognormal"
        push!(vec, getfield(x, Symbol(n.ll_det[1])) * exp(rand(Normal(n.vl[1], n.vl[2]))))
    elseif n.kind == "additive-uniform"
        push!(vec, getfield(x, Symbol(n.ll_det[1])) + rand(Uniform(n.vl[1], n.vl[2])))
    elseif n.kind == "multiplicative-uniform"
        push!(vec, getfield(x, Symbol(n.ll_det[1])) * (1 + rand(Uniform(n.vl[1], n.vl[2]))))
    elseif n.kind == "additive-mvnormal"
        z = getfield(x, Symbol(n.ll_det[1]))
        ns = rand(Normal(n.vl[1], n.vl[2]), length(z))
        vec = z .+ ns
    elseif n.kind == "multiplicative-mvnormal"
        z = getfield(x, Symbol(n.ll_det[1]))
        ns = rand(Normal(n.vl[1], n.vl[2]), length(z))
        vec = z .* (1 + ns)
    else
        error("Wrong name of kind in noise.")
    end
    return vec
end

function has_noise(n::Noise)
    epsilon = 1e-10
    bool_noise = false
    if n.kind in ["multiplicative-normal", "additive-normal", "multiplicative-lognormal"]
        bool_noise = n.vl[1] != 0.0 || n.vl[2] > epsilon
    elseif n.kind in ["multiplicative-uniform", "additive-uniform"]
        bool_noise = abs(n.vl[1]) > epsilon || abs(n.vl[2]) > epsilon
    end
    return bool_noise
end

function has_noise(nl)
    epsilon = 1e-10
    if (nl == Nothing || nl == nothing) || length(fieldnames(nl)) == 0
        return false
    else
        for n in fieldnames(nl)
            if has_noise(getfield(nl, n))
                return true
            end
        end
    end
    return false
end

has_modelling_noise() = has_noise(Main.mn)
has_observation_noise() = has_noise(Main.on)

function zero_noise(mn0)
    epsilon = 1e-100
    mn = deepcopy(mn0)
    for n in fieldnames(mn)
        if getfield(mn, n).kind in ["multiplicative-normal", "additive-normal", "multiplicative-lognormal", "additive-mvnormal", "multiplicative-mvnormal"]
            getfield(mn, n).vl = (0.0, epsilon)
        elseif getfield(mn, n).kind in ["multiplicative-uniform", "additive-uniform"]
            getfield(mn, n).vl = (-epsilon, epsilon)
        else
            error("Wrong name of kind in noise.")
        end
    end
    return mn
end

function extract_noise(on,
                       om::Label)
    for n in fieldnames(on)
        if om in getfield(on, n).ll_sto
            return getfield(on, n)
        end
    end
    error("The observation model $om does not correspond to any Noise in the ObservationNoise.")
end

function extract_noise(on,
                       om_det::Label,
                       om_sto::Label)
    for n in fieldnames(on)
        if om_det in getfield(on, n).ll_det && om_sto in getfield(on, n).ll_sto
            return getfield(on, n)
        end
    end
    error("The observation models om_det = $om_det and om_sto = $om_sto do not correspond to any Noise in the ObservationNoise.")
end

function covariance(noise)
    s = length(fieldnames(noise))
    mat_cov = zeros(s,s)
    i = 1
    for n in fieldnames(noise)
        assert(contains(getfield(noise, n).kind, "normal"))
        mat_cov[i,i] = getfield(noise, n).vl[2] ^ 2
        i += 1
    end
    return mat_cov
end

function find_noise(on, om::Label)
    for n in fieldnames(on)
        if om in getfield(on, n).ll_sto
            return getfield(on, n)
        end
    end
    error("Observation model $om not found in ObservationNoise.")
end

function get_stdev(on, so::SystemObservation, om::Label, t::Time)
    n = find_noise(on, om)
    mu = to_vec(so, om, t)[1]
    stdev = 0.0
    if n.kind == "multiplicative-normal"
        stdev = mu * n.vl[2]
    elseif n.kind == "additive-normal"
        stdev = n.vl[2]
    end
    return stdev
end

function set_stdev!(on, vec_stdev::Vector)
    s = length(fieldnames(on))
    assert(length(vec_stdev) == s)
    for i = 1:s
        n = getfield(on, fieldnames(on)[i])
        assert(contains(n.kind, "normal"))
        n.vl = 0.0, vec_stdev[i]
    end
end
