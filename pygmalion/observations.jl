
function get_simulation_observer_model_list(so_exp::SystemObservation,
                                            on::ObservationNoise) where {ObservationNoise}
    if VERSION >= v"0.7.0" ol = Array{ObserverModel}(undef, 0)
    else ol = Array{ObserverModel}(0)
    end 
    tml = get_merged_timeline(so_exp)
    for om in so_exp.oml
        f = fieldnames(ObservationNoise)
        i = findfirst(j -> om in getfield(on, f[j]).ll_sto, 1:length(f))
        i = (i == nothing) ? 0 : i # because of the convention before 0.7
        n = f[i]
        if VERSION >= v"0.7.0" tml_n = Array{Time}(undef, 0)
        else tml_n = Array{Time}(0)
        end
        for t in tml
            noise_observed = true
            for l in getfield(on, n).ll_sto
                if !is_observed(so_exp, l, t)
                    noise_observed = false
                end
            end
            if noise_observed
                push!(tml_n, t)
            end
        end
        for l in getfield(on, n).ll_det
            push!(ol, ObserverModel(l, tml_n))
        end
    end
    return ol
end

function is_observed(so::SystemObservation,
                     om::Label,
                     t::Time)
    i = get_index(so, om)
    nbr_obs = length(so.otll[i])
    j = 0
    for k = 1:nbr_obs
        if so.otll[i][k][1] == t
            j = k
            break
        end
    end
    return j > 0
end

function observed_variables(so::SystemObservation, t::Time)
    ll = Label[]
    for om in so.oml
        if is_observed(so, om, t)
            push!(ll, om)
        end
    end
    return ll
end

function get_observation_model_list(so::SystemObservation)
    return so.oml
end

function get_index(so::SystemObservation,
                   om::Label)
    if VERSION >= v"0.7.0" 
        ind = findfirst(isequal(om), so.oml)
        # Because in julia >= v0.7, findfirst returns nothing when it's not found
        if ind == nothing
            return 0
        end
        return ind
    else
        return findfirst(so.oml, om)
    end
end

function get_timeline(so::SystemObservation,
                      om::Label)
    if VERSION >= v"0.7.0" tml = Array{Time}(undef, 0)
    else tml = Array{Time}(0)
    end
    i = get_index(so, om)
    if i == 0
        return []
    end
    nbr_obs = length(so.otll[i])
    for j = 1:nbr_obs
        push!(tml, so.otll[i][j][1])
    end
    return tml
end

function get_merged_timeline(so::SystemObservation)
    if VERSION >= v"0.7.0" tml = Array{Time}(undef, 0)
    else tml = Array{Time}(0)
    end
    for om in so.oml
        tml = unique(sort!([tml; get_timeline(so, om)]))
    end
    return tml
end

function get_observation_function(so::SystemObservation)
    str = "["
    nbr_om = length(so.oml)
    for i = 1:nbr_om
        str = str * "ObserverModel(\"" * so.oml[i] * "\","
        tml = get_timeline(so, so.oml[i])
        str = str * string(tml) * "),"
    end
    str = str[1:end-1]
    str = str * "]"
    return str
end

function get_simulation_observation_model_list(on::ObservationNoise) where {ObservationNoise}
    oml_sim = Label[]
    for n in fieldnames(ObservationNoise)
        for l in getfield(on, n).ll_det
            push!(oml_sim, l)
        end
    end
    return oml_sim
end

function get_experimental_observation_model_list(on::ObservationNoise) where {ObservationNoise}
    oml_exp = Label[]
    for n in fieldnames(ObservationNoise)
        for l in getfield(on, n).ll_sto
            push!(oml_exp, l)
        end
    end
    return oml_exp
end

function create_observation_function_string(oml::LabelList,
                                            tml::Timeline)
    str = "["
    nbr_om = length(oml)
    for i = 1:nbr_om
        str = str * "ObserverModel(\"" * oml[i] * "\","
        tml = get_timeline(so, so.oml[i])
        str = str * string(tml) * "),"
    end
    str = str[1:end-1]
    str = str * "]"
    return str
end

function get_maximum_time(g::ObserverList)
   t_max = 0
   for o in g
       t_max = max(t_max, o.tml[end])
    end
    return t_max
end

function get_merged_timeline(g::ObserverList)
    if VERSION >= v"0.7.0" tml = Array{Int}(undef, 0)
    else tml = Array{Int}(0)
    end
    for i = 1:length(g)
        tml = unique(sort!([tml; g[i].tml]))
    end
    return tml
end

function get_number_of_observations(so::SystemObservation,
                                    om::Label)
    i = get_index(so, om)
    nbr_obs = length(so.otll[i])
    return nbr_obs
end

function get_number_of_observation_models(so::SystemObservation)
    nbr_om = 0
    for om in so.oml
        i = get_index(so, om)
        if length(so.otll[i]) > 0
            nbr_om += 1
        end
    end
    return nbr_om
end

function get_number_of_values(x::Vector)
    n = 0
    for i = 1:length(x)
        if !isnan(x[i]) n += 1 end
    end
    return n
end

function get_number_of_values(so::SystemObservation,
                              om::Label)
    i = get_index(so, om)
    nbr_val = 0
    for j = 1:length(so.otll[i])
        nbr_val += get_number_of_values(so.otll[i][j][2])
    end
    return nbr_val
end

function get_total_number_of_observations(so::SystemObservation)
    nbr_obs_total = 0
    nbr_om = length(so.oml)
    for i = 1:nbr_om
        nbr_obs_total += get_number_of_observations(so, so.oml[i])
    end
    return nbr_obs_total
end

function get_total_number_of_values(so::SystemObservation)
    nbr_val_total = 0
    nbr_om = length(so.oml)
    for i = 1:nbr_om
        nbr_val_total += get_number_of_values(so, so.oml[i])
    end
    return nbr_val_total
end

function get_total_number_of_observations(sol::SystemObservationList)
    nbr_obs_total = 0
    for so in sol
        nbr_om = length(so.oml)
        for i = 1:nbr_om
            nbr_obs_total += get_number_of_observations(so, so.oml[i])
        end
    end
    return nbr_obs_total
end

function get_total_number_of_observations(edl::Array{Tuple})
    nbr_obs_total = 0
    for ed in edl
        nbr_obs_total += get_total_number_of_observations(ed[3])
    end
    return nbr_obs_total
end

function register!(so::SystemObservation,
                   om::Label,
                   t::Time,
                   v::Value)
    so.set(om, t, v)
end

function register!(so::SystemObservation,
                   oml::LabelList,
                   t::Time,
                   vl::ValueList)
    for i = 1:length(oml)
        so.set(oml[i], t, vl[i])
    end
end

function register!(so::SystemObservation,
                   oml::LabelList,
                   t::Time,
                   vl::Array{Array{Value}})
    for i = 1:length(oml)
        so.set(oml[i], t, vl[i])
    end
end

function get_observation_model_maximum_size(so::SystemObservation,
                                            om::Label)
    s = 0
    i = get_index(so, om)
    for o in so.otll[i]
        s = max(s, length(o[2]))
    end
    return s
end

function is_vector_observation(so::SystemObservation,
                               om::Label)
    return get_observation_model_maximum_size(so, om) > 1
end
