
function log_pdf(p::Parameters,
                 sr::SamplingRule) where {Parameters}
    lp = 0.0
    if sr.law == "normal"
        lp = log_normal_pdf(getfield(p, Symbol(sr.ll[1])), sr.vl[1], sr.vl[2])
    elseif sr.law == "uniform"
        lp = log_uniform_pdf(getfield(p, Symbol(sr.ll[1])), sr.vl[1], sr.vl[2])
    elseif sr.law == "mvnormal"
        lp = log_mvnormal_pdf(to_vec(p, sr.ll), sr.vl[1], sr.vl[2])
    elseif sr.law == "inverse_gamma"
        lp = log(inverse_gamma_pdf(getfield(p, Symbol(sr.ll[1])), sr.vl[1], sr.vl[2]))
    else
        error("Wrong name of law in sampling rule.")
    end
    if isnan(lp)
        lp = 0.0
    end
    return lp
end

function log_pdf(p::Parameters,
                 sr::CustomSamplingRule) where {Parameters}
    vec_p = to_vec(p, sr.ll)
    if length(vec_p) == 1 vec_p = vec_p[1] end
    lp = log(pdf(sr.law, vec_p))
    if isnan(lp) lp = 0.0 end
    return lp
end

function log_pdf(val::Float64,
                 sr::SamplingRule) where {Parameters}
    lp = 0.0
    if sr.law == "normal"
        lp = log_normal_pdf(val, sr.vl[1], sr.vl[2])
    elseif sr.law == "uniform"
        lp = log_uniform_pdf(val, sr.vl[1], sr.vl[2])
    elseif sr.law == "inverse_gamma"
        lp = log(inverse_gamma_pdf(val, sr.vl[1], sr.vl[2]))
    else
        error("Wrong name of law in sampling rule.")
    end
    if isnan(lp)
        lp = 0.0
    end
    return lp
end

function log_pdf(val::Float64,
                 sr::CustomSamplingRule) where {Parameters}
    lp = log(pdf(sr.law, val))
    if isnan(lp) lp = 0.0 end
    return lp
end

# For both types CustomSR and SR in the list
function log_pdf(p::Parameters,
                 srl::SamplingRuleList) where {Parameters}
    lp = 0.0
    for sr in srl
        lp += log_pdf(p, sr)
    end
    if isnan(lp)
        lp = 0.0
    end
    return lp
end

function log_pdf_mn(mn::ModellingNoise,
                    sr::SamplingRule) where {ModellingNoise}
    lp = 0.0
    n = getfield(mn, Symbol(sr.ll[1]))
    if sr.law == "inverse_gamma"
        lp = log(inverse_gamma_pdf(n.vl[2], sr.vl[1], sr.vl[2]))
    else
        error("Wrong name of law in sampling rule.")
    end
    if isnan(lp)
        lp = 0.0
    end
    return lp
end

function log_pdf_mn(mn::ModellingNoise,
                    srl::SamplingRuleList) where {ModellingNoise}
    lp = 0.0
    for sr in srl
        lp += log_pdf(mn, sr)
    end
    if isnan(lp)
        lp = 0.0
    end
    return lp
end

function log_pdf(x::State,
                 n::Noise) where {State}
    lp = 0.0
    if n.kind == "additive-normal"
        x_det = getfield(x, Symbol(n.ll_det[1]))
        x_sto = getfield(x, Symbol(n.ll_sto[1]))
        lp = log_normal_pdf(x_sto, x_det + n.vl[1], n.vl[2])
    elseif n.kind == "multiplicative-normal"
        x_det = getfield(x, Symbol(n.ll_det[1]))
        x_sto = getfield(x, Symbol(n.ll_sto[1]))
        lp = log_normal_pdf(x_sto, x_det * (1 + n.vl[1]), abs(x_det * n.vl[2]))
    elseif n.kind == "multiplicative-lognormal"
        x_det = getfield(x, Symbol(n.ll_det[1]))
        x_sto = getfield(x, Symbol(n.ll_sto[1]))
        lp = log_lognormal_pdf(x_sto, log(x_det) + n.vl[1], n.vl[2])
    elseif n.kind == "additive-uniform"
        x_det = getfield(x, Symbol(n.ll_det[1]))
        x_sto = getfield(x, Symbol(n.ll_sto[1]))
        lp = log_uniform_pdf(x_sto, x_det + n.vl[1], x_det + n.vl[2])
    elseif n.kind == "multiplicative-uniform"
        x_det = getfield(x, Symbol(n.ll_det[1]))
        x_sto = getfield(x, Symbol(n.ll_sto[1]))
        lp = log_uniform_pdf(x_sto, x_det * (1 + n.vl[1]), x_det * (1 + n.vl[2]))
    else
        error("Wrong name of kind in noise")
    end
    if isnan(lp)
        lp = 0.0
    end
    return lp
end

## CAUTION: doesn't work if so_exp has to be compared to so_sim, this implies to pass 2 SystemObservation's
function log_pdf(so::SystemObservation,
                 n::Noise)
    lp = 0.0
    if n.kind == "additive-normal"
        x_det = to_vec(so, n.ll_det[1], t)[1]
        x_sto = to_vec(so, n.ll_sto[1], t)[1]
        lp = log_normal_pdf(x_sto, x_det + n.vl[1], n.vl[2])
    elseif n.kind == "multiplicative-normal"
        x_det = to_vec(so, n.ll_det[1], t)[1]
        x_sto = to_vec(so, n.ll_sto[1], t)[1]
        lp = log_normal_pdf(x_sto, x_det * (1 + n.vl[1]), abs(x_det * n.vl[2]))
    elseif n.kind == "multiplicative-lognormal"
        x_det = to_vec(so, n.ll_det[1], t)[1]
        x_sto = to_vec(so, n.ll_sto[1], t)[1]
        lp = log_lognormal_pdf(x_sto, log(x_det) + n.vl[1], n.vl[2])
    elseif n.kind == "additive-uniform"
        x_det = to_vec(so, n.ll_det[1], t)[1]
        x_sto = to_vec(so, n.ll_sto[1], t)[1]
        lp = log_uniform_pdf(x_sto, x_det + n.vl[1], x_det + n.vl[2])
    elseif n.kind == "multiplicative-uniform"
        x_det = to_vec(so, n.ll_det[1], t)[1]
        x_sto = to_vec(so, n.ll_sto[1], t)[1]
        lp = log_uniform_pdf(x_sto, x_det * (1 + n.vl[1]), x_det * (1 + n.vl[2]))
    else
        error("Wrong name of kind in noise")
    end
    if isnan(lp)
        lp = 0.0
    end
    return lp
end

function log_transition_pdf(x::State,
                            mn::ModellingNoise) where {State, ModellingNoise}
    log_transition = 0.0
    for n in fieldnames(ModellingNoise)
        log_transition += log_pdf(x, getfield(mn, n))
    end
    return log_transition
end

function log_pdf(x::State,
                 n::Noise,
                 so::SystemObservation,
                 t::Time) where {State}
    lp = 0.0
    if n.kind == "additive-normal"
        x_det = getfield(x, Symbol(n.ll_det[1]))
        x_sto = to_vec(so, n.ll_sto[1], t)[1]
        lp = log_normal_pdf(x_sto, x_det + n.vl[1], n.vl[2])
    elseif n.kind == "multiplicative-normal"
        x_det = getfield(x, Symbol(n.ll_det[1]))
        x_sto = to_vec(so, n.ll_sto[1], t)[1]
        lp = log_normal_pdf(x_sto, x_det * (1 + n.vl[1]), abs(x_det * n.vl[2]))
    elseif n.kind == "multiplicative-lognormal"
        x_det = getfield(x, Symbol(n.ll_det[1]))
        x_sto = to_vec(so, n.ll_sto[1], t)[1]
        lp = log_lognormal_pdf(x_sto, log(x_det) + n.vl[1], n.vl[2])
    elseif n.kind == "additive-uniform"
        x_det = getfield(x, Symbol(n.ll_det[1]))
        x_sto = to_vec(so, n.ll_sto[1], t)[1]
        lp = log_uniform_pdf(x_sto, x_det + n.vl[1], x_det + n.vl[2])
    elseif n.kind == "multiplicative-uniform"
        x_det = getfield(x, Symbol(n.ll_det[1]))
        x_sto = to_vec(so, n.ll_sto[1], t)[1]
        lp = log_uniform_pdf(x_sto, x_det * (1 + n.vl[1]), x_det * (1 + n.vl[2]))
    else
        error("Wrong name of kind in noise")
    end
    if isnan(lp)
        lp = 0.0
    end
    return lp
end

function log_pdf(so_sim::SystemObservation,
                 n::Noise,
                 so_exp::SystemObservation,
                 t::Time)
    lp = 0.0
    if n.kind == "additive-normal"
        x_det = to_vec(so_sim, n.ll_det[1], t)[1]
        x_sto = to_vec(so_exp, n.ll_sto[1], t)[1]
        lp = log_normal_pdf(x_sto, x_det + n.vl[1], n.vl[2])
    elseif n.kind == "multiplicative-normal"
        x_det = to_vec(so_sim, n.ll_det[1], t)[1]
        x_sto = to_vec(so_exp, n.ll_sto[1], t)[1]
        lp = log_normal_pdf(x_sto, x_det * (1 + n.vl[1]), abs(x_det * n.vl[2]))
    elseif n.kind == "multiplicative-lognormal"
        x_det = to_vec(so_sim, n.ll_det[1], t)[1]
        x_sto = to_vec(so_exp, n.ll_sto[1], t)[1]
        lp = log_lognormal_pdf(x_sto, log(x_det) + n.vl[1], n.vl[2])
    elseif n.kind == "additive-uniform"
        x_det = to_vec(so_sim, n.ll_det[1], t)[1]
        x_sto = to_vec(so_exp, n.ll_sto[1], t)[1]
        lp = log_uniform_pdf(x_sto, x_det + n.vl[1], x_det + n.vl[2])
    elseif n.kind == "multiplicative-uniform"
        x_det = to_vec(so_sim, n.ll_det[1], t)[1]
        x_sto = to_vec(so_exp, n.ll_sto[1], t)[1]
        lp = log_uniform_pdf(x_sto, x_det * (1 + n.vl[1]), x_det * (1 + n.vl[2]))
    elseif n.kind == "multiplicative-mvnormal"
        x_det = to_vec(so_sim, n.ll_det[1], t)
        x_sto = to_vec(so_exp, n.ll_sto[1], t)
        lp = 0.0
        for i = 1:length(x_det)
            if x_det[i] != 0 && x_sto[i] != 0
                lp += log_normal_pdf(x_det[i], x_sto[i], n.vl[2] * x_det[i])
            end
        end
    else
        error("Wrong name of kind in noise")
    end
    if isnan(lp)
        lp = 0.0
    end
    return lp
end

# function log_pdf(n::Noise,
#                  so1::SystemObservation,
#                  so2::SystemObservation,
#                  t::Time)
#     lp = 0.0
#     x_det = to_vec(so1, n.ll_sto[1], t)[1]
#     x_sto = to_vec(so2, n.ll_sto[1], t)[1]
#     if n.kind == "additive-normal"
#         lp = log_normal_pdf(x_sto, x_det + n.vl[1], n.vl[2])
#     elseif n.kind == "multiplicative-normal"
#         lp = log_normal_pdf(x_sto, x_det * (1 + n.vl[1]), abs(x_det * n.vl[2]))
#     elseif n.kind == "multiplicative-lognormal"
#         lp = log_lognormal_pdf(x_sto, log(x_det) + n.vl[1], n.vl[2])
#     elseif n.kind == "additive-uniform"
#         lp = log_uniform_pdf(x_sto, x_det + n.vl[1], x_det + n.vl[2])
#     elseif n.kind == "multiplicative-uniform"
#         lp = log_uniform_pdf(x_sto, x_det * (1 + n.vl[1]), x_det * (1 + n.vl[2]))
#     else
#         error("Wrong name of kind in noise")
#     end
#     if isnan(lp)
#         lp = 0.0
#     end
#     return lp
# end

function log_pdf(n::Noise,
                 so1::SystemObservation,
                 so2::SystemObservation,
                 t::Time)
    lp = 0.0
    x_det = to_vec(so1, n.ll_det[1], t)[1]
    x_sto = to_vec(so2, n.ll_sto[1], t)[1]
    if n.kind == "additive-normal"
        lp = log_normal_pdf(x_sto, x_det + n.vl[1], n.vl[2])
    elseif n.kind == "multiplicative-normal"
        lp = log_normal_pdf(x_sto, x_det * (1 + n.vl[1]), abs(x_det * n.vl[2]))
    elseif n.kind == "multiplicative-lognormal"
        lp = log_lognormal_pdf(x_sto, log(x_det) + n.vl[1], n.vl[2])
    elseif n.kind == "additive-uniform"
        lp = log_uniform_pdf(x_sto, x_det + n.vl[1], x_det + n.vl[2])
    elseif n.kind == "multiplicative-uniform"
        lp = log_uniform_pdf(x_sto, x_det * (1 + n.vl[1]), x_det * (1 + n.vl[2]))
    else
        error("Wrong name of kind in noise")
    end
    if isnan(lp)
        lp = 0.0
    end
    return lp
end

# function log_pdf{ObservationNoise}(on::ObservationNoise,
#                                    so1::SystemObservation,
#                                    so2::SystemObservation,
#                                    t::Time)
#     log_observation = 0.0
#     for n in fieldnames(on)
#         if getfield(on, n).vl[2] > 1e-10
#             if t in get_timeline(so1, getfield(on, n).ll_sto[1])
#                 log_observation += log_pdf(getfield(on, n), so1, so2, t)
#             end
#         end
#     end
#     return log_observation
# end

function log_pdf(on::ObservationNoise,
                 so1::SystemObservation,
                 so2::SystemObservation,
                 t::Time) where {ObservationNoise}
    log_observation = 0.0
    for n in fieldnames(ObservationNoise)
        if getfield(on, n).vl[2] > 1e-10
            if t in get_timeline(so1, getfield(on, n).ll_det[1])
                log_observation += log_pdf(getfield(on, n), so1, so2, t)
            end
        end
    end
    return log_observation
end

function log_observation_pdf(x::State,
                             on::ObservationNoise,
                             so::SystemObservation,
                             t::Time) where {State, ObservationNoise}
    log_observation = 0.0
    for n in fieldnames(ObservationNoise)
        if t in get_timeline(so, getfield(on, n).ll_sto[1])
            log_observation += log_pdf(x, getfield(on, n), so, t)
        end
    end
    return log_observation
end

function log_observation_pdf(so_sim::SystemObservation,
                             on::ObservationNoise,
                             so_exp::SystemObservation,
                             t::Time) where {ObservationNoise}
    log_observation = 0.0
    for n in fieldnames(ObservationNoise)
        if t in get_timeline(so_exp, getfield(on, n).ll_sto[1])
            log_observation += log_pdf(so_sim, getfield(on, n), so_exp, t)
        end
    end
    return log_observation
end

function log_likelihood_observation_pdf(so_sim::SystemObservation, on::ObservationNoise, so_exp::SystemObservation, tml::Timeline) where {ObservationNoise}
    log_likelihood_observation = 0.0
    for t in tml
        log_likelihood_observation += log_observation_pdf(so_sim, on, so_exp, t)
    end
    return log_likelihood_observation
end

function log_likelihood_observation_pdf(so_sim::SystemObservation, on::ObservationNoise, so_exp::SystemObservation) where {ObservationNoise}
    log_likelihood_observation = 0.0
    tml = get_merged_timeline(so_exp)
    for t in tml
        log_likelihood_observation += log_observation_pdf(so_sim, on, so_exp, t)
    end
    return log_likelihood_observation
end

# function log_likelihood_pdf{State,ModellingNoise,ObservationNoise}(xl::Array{State}, so_exp::SystemObservation; mn = Void, on = Void)
#     log_likelihood = 0.0
#     tml = get_merged_timeline(so_exp)
#     for t in tml
#         log_likelihood += log_observation_pdf(xl[t], on, so_exp, t)
#     end
#     for t = 1:length(xl)
#         log_likelihood += log_transition_pdf(xl[t], mn)
#     end
#     return log_likelihood
# end

pdf(p::Parameters, sr::SamplingRule) where {Parameters} = exp(log_pdf(p, sr)) 
pdf(p::Parameters, srl::SamplingRuleList) where {Parameters} = exp(log_pdf(p, srl))
pdf(x::State, n::Noise) where {State} = exp(log_pdf(x, n))
pdf(so::SystemObservation, n::Noise) = exp(log_pdf(so, n))
transition_pdf(x::State, mn::ModellingNoise) where {State, ModellingNoise} = exp(log_transition_pdf(x, mn))
pdf(x::State, n::Noise, so::SystemObservation, t::Time) where {State} = exp(log_pdf(x, n, so, t))
pdf(so_sim::SystemObservation, n::Noise, so_exp::SystemObservation, t::Time) = exp(log_pdf(so_sim, n, so_exp, t))
observation_pdf(x::State, on::ObservationNoise, so::SystemObservation, t::Time) where {State, ObservationNoise} = exp(log_observation_pdf(x, on, so, t))
observation_pdf(so_sim::SystemObservation, on::ObservationNoise, so_exp::SystemObservation, t::Time) where {ObservationNoise} = exp(log_observation_pdf(so_sim, on, so_exp, t))
likelihood_observation_pdf(so_sim::SystemObservation, on::ObservationNoise, so_exp::SystemObservation, tml::Timeline) where {ObservationNoise} = exp(log_likelihood_observation_pdf(so_sim, on, so_exp, tml))
