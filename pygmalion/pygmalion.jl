
module pygmalion

# Patch-up job for making pygmalion compatible with versions lower / greater than 0.7
if VERSION < v"0.7.0"
    Nothing = Base.Void
    macro distributed(args...)
        :(@parallel($(map(esc, args)...)))
    end
    macro warn(args...)
        :(warn($(map(esc, args)...)))
    end
	export Nothing, @distributed, @warn 
    import Base: .+, .-, .*, ./ 
end

import Base: +, -, *, ^, /, ==
import Base: copy, exp, log, sqrt, filter, split
#import Distributions: Sampleable, ContinuousMultivariateDistribution, DiscreteUniform, Uniform, Normal, MvNormal, Gamma, InverseGamma, Wishart, InverseWishart, Chisq, pdf, MixtureModel, Categorical, mean, var
using Distributions
export DiscreteUniform, Uniform, Normal, MvNormal, Gamma, InverseGamma, Wishart, InverseWishart, Chisq, pdf, MixtureModel, Categorical, mean, var
import DataStructures: OrderedDict
export OrderedDict
if VERSION >= v"0.7.0"
    import Printf: @printf, @sprintf
    import Dates
    import LinearAlgebra: det, diagm
    import Statistics: mean, max, cov
    import SpecialFunctions: gamma
    import DelimitedFiles: readdlm
else
    import Base: mean, max
end

include("simulation.jl")
export Time, Timeline, Label, LabelList, Value, ValueList, ObservationTuple, ObservationTupleList
export ObserverModel, ObserverModelList, Observer, ObserverList, Observation, SystemObservation, SystemObservationList
export simulate, simulate_state_list, next_prediction!, observe, @observation_function

include("noise.jl")
export Noise
export noise!, noise, deterministic_model, stochastic_model, zero_noise, has_noise, has_modelling_noise, has_observation_noise, find_noise, get_stdev, set_stdev!

include("vectorize.jl")
export to_vec, to_mat

include("math.jl")
export log_normal_pdf, normal_pdf, lognormal_pdf, log_mvnormal_pdf, mvnormal_pdf, chi_squared_pdf, inverse_gamma_pdf, jacobian, mean, weighted_mean, mean_and_variance, weighted_mean_and_variance, mean_and_stdev, weighted_mean_and_stdev, mean_and_covariance, weighted_mean_and_covariance, correlation, weighted_correlation, sigma_points_and_weights, uniform_kernel, gaussian_kernel, covariance, rmsep, ef, symmetrize!, autodiff

include("input_output.jl")
export save_to_csv, load_from_csv, save_to_obs, load_from_obs, get_path_separator, get_abs_pygmalion_dir, get_pygmalion_directory, makedir, get_last_results_directory, create_results_directory, copy, save, realdirname, realbasename, get_observation_files, print_data, write_parameters, write_state, print_vec, write_vec, load_model, load_parameters, load_state, load_control, load_observations, load_configuration, write_vector, load_vector
if VERSION >= v"0.7.0"
	export State, Parameters, Control, ObservationNoise, ModellingNoise, f, ofl, create_observation_function
	export x0, u, p
end

include("observations.jl")
export get_index, get_observation_function, get_maximum_time, get_merged_timeline, get_timeline, get_observation_model_list, get_number_of_observations, get_total_number_of_observations, get_number_of_values, get_total_number_of_values, get_number_of_values, filter, split, is_observed, get_simulation_observer_model_list, get_simulation_observation_model_list, get_experimental_observation_model_list, register!, get_observation_model_maximum_size, is_vector_observation, observed_variables

include("system_observations.jl")
export +, .+, -, .-, *, .*, .^, /, ./, ==, operate, same_shape, max

include("sampling.jl")
export AbstractSamplingRule, SamplingRule, CustomSamplingRule, SamplingRuleList
export get_label_list, sample, mean, var

include("probability_density_functions.jl")
export log_pdf, pdf, log_transition_pdf, log_observation_pdf, log_likelihood_observation_pdf
export pdf, transition_pdf, observation_pdf, likelihood_observation_pdf

include("helpers.jl")
export om_findfirst, retrieve_subset, create_from_subset, field_copy!, fill_with_subset!, copy_subset!, system_observation_list, vec_to_dict, dict_to_vec, add_backslashes, isrange, str2bool, to_str

include("filters.jl")
export effective_sample_size, random_number_list, resample_number_particles, create_indices, update_from_indices!, mask_matrix, bandwidth_cr, convolution!, Bound, BoundList, check_bounds!, check_bounds, create_weights, init_weights

end

mn = nothing
on = nothing

