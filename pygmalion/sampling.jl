
abstract type AbstractSamplingRule end

struct SamplingRule <: AbstractSamplingRule
    ll::LabelList
    law::Label
    vl::Tuple
end

struct CustomSamplingRule <: AbstractSamplingRule
    ll::LabelList
    law::Sampleable
end

const SamplingRuleList = Array{<:AbstractSamplingRule, 1}
# Patch-up job : doesn't work if array contains both SR and CustomSR types
# This is done for compatibility with former scripts, shouldn't be used
SamplingRuleList(srl::Array{Any, 1}) = convert(Array{typeof(srl[1]), 1}, srl)
#const CustomSamplingRuleList = Array{CustomSamplingRule, 1}
#const AnySamplingRuleList = Array{<:AbstractSamplingRule, 1}

function get_label_list(srl::SamplingRuleList)
    n = length(srl)
    ll = Label[]
    for i = 1:n
        ll = [ll; srl[i].ll]
    end
    return ll
end

function get_distribution(sr::SamplingRule)
    if sr.law == "normal"
        d = Normal(sr.vl[1], sr.vl[2])
    elseif sr.law == "uniform"
        d = Uniform(sr.vl[1], sr.vl[2])
    elseif sr.law == "discrete-uniform"
        d = DiscreteUniform(sr.vl[1], sr.vl[2])
    elseif sr.law == "poisson"
        d = Poisson(sr.vl[1], sr.vl[2])
    elseif sr.law == "mvnormal"
        symmetrize!(sr.vl[2])
        d = MvNormal(sr.vl[1], sr.vl[2])
    elseif sr.law == "gamma"
        d = Gamma(sr.vl[1], sr.vl[2])
    elseif sr.law == "inverse-gamma"
        d = InverseGamma(sr.vl[1], sr.vl[2])
    elseif sr.law == "wishart"
        d = Wishart(sr.vl[1], sr.vl[2])
    elseif sr.law == "inverse-wishart"
        d = InverseWishart(sr.vl[1], sr.vl[2])
    elseif sr.law == "mixture-model"
        d = MixtureModel(sr.vl[1], sr.vl[2])
    else
        error("Wrong name of law in sampling rule.")
    end
	
    return d
end
get_distribution(sr::CustomSamplingRule) = sr.law

function sample(x0, srl::SamplingRuleList, nbr_samples::Int)
    if VERSION >= v"0.7.0" 
        xl = Array{typeof(x0)}(undef, nbr_samples)
        vec_dist = Array{Any}(undef, length(srl))
    else
        xl = Array{typeof(x0)}(nbr_samples)
        vec_dist = Array{Any}(length(srl))
    end
    for j = 1:length(srl)
        vec_dist[j] = get_distribution(srl[j])
    end
    for i = 1:nbr_samples
        x = deepcopy(x0)
        for j = 1:length(srl)
            r = rand(vec_dist[j])
            fill_with_subset!(x, r, srl[j].ll)
        end
        xl[i] = x
    end
    return xl
end
sample(sr::AbstractSamplingRule) = rand(get_distribution(sr))

mean(sr::AbstractSamplingRule) = mean(get_distribution(sr))
var(sr::AbstractSamplingRule) = var(get_distribution(sr))

# This is a not recommended way of coding in julia >= v0.7
# We should use broadcasting mean.()
function mean(srl::SamplingRuleList)
    @warn "Pygmalion : mean(srl::SamplingRuleList) is deprecated. Use mean.() instead."
    nb_sr = length(srl)
	if VERSION >= v"0.7.0" means = Vector{Float64}(undef, nb_sr)
    else means = Vector{Float64}(nb_sr)
    end
    for i in 1:nb_sr
		means[i] = mean(srl[i])
	end
	return means
end

function var(srl::SamplingRuleList)
    @warn "Pygmalion : var(srl::SamplingRuleList) is deprecated. Use var.() instead."
    nb_sr = length(srl)
    if VERSION >= v"0.7.0" vars = Vector{Float64}(undef, nb_sr)
    else vars = Vector{Float64}(nb_sr) 
    end
    for i in 1:nb_sr
		vars[i] = var(srl[i])
	end
	return vars
end

#sample{Parameters,State}(x0::Union{Parameters,State}, srl::SamplingRuleList) = sample(x0, srl, 1)[1]
sample(x0, srl::SamplingRuleList) = sample(x0, srl, 1)[1]

function covariance(srl::SamplingRuleList)
    n = length(srl)
    m = zeros(n, n)
    for i = 1:length(srl)
        sr = srl[i]
        if sr.law == "normal"
            m[i,i] = sr.vl[2] ^ 2
        elseif sr.law == "uniform"
            m[i,i] = abs(sr.vl[2] - sr.vl[1]) ^ 2
        end
    end
    return m
end

