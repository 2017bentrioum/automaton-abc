
const Time = Int
const Timeline = Array{Time, 1}
const Label = AbstractString
const LabelList{T <: Label} = Array{T, 1}
const Value = Float64
const ValueList = Array{Value, 1}
const ObservationTuple = Tuple{Time, ValueList}
const ObservationTupleList = Array{ObservationTuple}

struct ObserverModel
    om::Label
    tml::Timeline
end

struct Observer
    om::Label
    f::Function
    tml::Timeline
    nbr_args::Int
end

Observer(om::Label, f::Function, tml::Timeline) = Observer(om, f, tml, get_number_arguments(f))

const ObserverModelList = Array{ObserverModel, 1}
const ObserverList = Array{Observer, 1}

mutable struct SystemObservation
    title::Label
    oml::LabelList
    otll::Array{ObservationTupleList}
    set::Function
    print::Function
    function SystemObservation(oml; title = "0")
        instance = new()
        instance.title = title
        instance.oml = oml
        instance.otll = [Tuple{Time, Vector}[] for i = 1:length(instance.oml)]
        instance.set = function(om::Label, t::Time, v)
            i = om_findfirst(om, instance.oml)
            if typeof(v) == Int || typeof(v) == Value
                push!(instance.otll[i], (t, [v]))
            else
                push!(instance.otll[i], (t, v))
            end
        end
        instance.print = function()
            for i = 1:length(instance.oml)
                for j = 1:length(instance.otll[i])
                    println(instance.oml[i] * "," * string(instance.otll[i][j][1]) * "," * string(instance.otll[i][j][2]))
                end
            end
        end
        return instance
    end
end

const SystemObservationList = Array{SystemObservation}

function get_number_arguments(m)
    nbr_args = 0
    if VERSION >= v"0.5.0"
        nbr_args = length(methods(m).mt.defs.sig.parameters) - 1
    elseif VERSION >= v"0.4.5"
        nbr_args = length(methods(m).defs.sig.parameters)
    end
    if nbr_args == 0 error("Could not infer the number of parameters from the method.") end
    return nbr_args
end

function get_observation_model_list(g::ObserverList)
    oml = Label[]
    for o in g
        push!(oml, o.om)
    end
    return oml
end

function get_observation_model_list(g::ObserverList,
                                    t::Time)
    oml = Label[]
    for o in g
        if t in o.tml
            push!(oml, o.om)
        end
    end
    return oml
end

function simulate(f::Function, g::ObserverList,
                  x0::State,
                  u::Control,
                  p::Parameters;
                  mn = nothing,
                  on = nothing,
                  full_timeline = false) where {State, Control, Parameters}   
    if :ctmc_time_end in fieldnames(Control)
        # For the compatibility of different versions of Julia
        x_current = x0
        time_current = x0.time
        t = 1
        ctmc_tmax = getfield(u, :ctmc_time_end)
        oml = get_observation_model_list(g)
        so = SystemObservation(oml)
        t_current = x0.time
        while t_current < ctmc_tmax
            if mn == nothing
                x_current = f(t, x_current, u, p)
            else
                x_current = f(t, x_current, u, p, mn)
            end
            t_current = x_current.time
            for o in g
                nbr_args = o.nbr_args
                if nbr_args == 3
                    vl = o.f(x_current, p, on)
                elseif nbr_args == 2
                    vl = o.f(x_current, on)
                elseif nbr_args == 1
                    vl = o.f(x_current)
                end
                so.set(o.om, t, vl)
            end
            t += 1
        end
        # Patch-up job for julia compatibility with 0.6
        is_post_proc = false
        try
            is_post_proc = isa(post_processing, Function)
        catch err
            if isa(err, UndefVarError)
                is_post_proc = false
            end
        end
        if is_post_proc
            Main.post_processing(so)
        end
        return so
    else
        t_max = get_maximum_time(g)
        # For the compatibility of different versions of Julia
        xl = Array{State, 1}()
        if VERSION >= v"0.7.0" xl = Array{State, 1}(undef, t_max+1)
        else xl = Array{State, 1}(t_max+1)
        end
        xl[1] = x0
        oml = get_observation_model_list(g)
        so = SystemObservation(oml)
        for t = 1:t_max
            if mn == nothing
                xl[t+1] = f(t, xl[t], u, p)
            else
                xl[t+1] = f(t, xl[t], u, p, mn)
            end
            for o in g
                if t in o.tml || full_timeline
                    nbr_args = o.nbr_args
                    if nbr_args == 3
                        vl = o.f(xl[t+1], p, on)
                    elseif nbr_args == 2
                        vl = o.f(xl[t+1], on)
                    elseif nbr_args == 1
                        vl = o.f(xl[t+1])
                    end
                    so.set(o.om, t, vl)
                end
            end
        end
        # Patch-up job for julia compatibility with 0.6
        is_post_proc = false
        try
            is_post_proc = isa(post_processing, Function)
        catch err
            if isa(err, UndefVarError)
                is_post_proc = false
            end
        end
        if is_post_proc
            Main.post_processing(so)
        end
        return so
    end
end

function simulate(f::Function, g::ObserverList,
                               x0::State,
                               u::Control,
                               pl::Array{Parameters};
                               mn = nothing,
                               on = nothing) where {State, Control, Parameters}
    nbr_samples = length(pl)
    sol = Array{SystemObservation}(nbr_samples)
    for i = 1:nbr_samples
        sol[i] = simulate(f, g, x0, u, pl[i]; mn = mn, on = on)
    end
    return sol
end

function simulate(f::Function,
                  p::Parameters,
                  edl::Array{Tuple};
                  mn = nothing,
                  on = nothing) where {Parameters}
    sol = Array{SystemObservation}(0)
    for ed in edl
        x0, u, so_exp, g = ed
        push!(sol, simulate(f, g, x0, u, p; mn = mn, on = on))
    end
    return sol
end

function simulate_state_list(f::Function,
                             g::ObserverList,
                             x0::State,
                             u::Control,
                             p::Parameters;
                             mn = nothing,
                             on = nothing) where {State, Control, Parameters}
    t_max = get_maximum_time(g)
    xl = Array{State}(t_max+1)
    xl[1] = x0
    oml = get_observation_model_list(g)
    so = SystemObservation(oml)
    for t = 1:t_max
        if mn == nothing
            xl[t+1] = f(t, xl[t], u, p)
        else
            xl[t+1] = f(t, xl[t], u, p, mn)
        end
        for o in g
            if t in o.tml
                nbr_args = o.nbr_args
                if nbr_args == 3
                    vl = o.f(xl[t+1], p, on)
                elseif nbr_args == 2
                    vl = o.f(xl[t+1], on)
                else
                    vl = o.f(xl[t+1])
                end
                so.set(o.om, t, vl)
            end
        end
    end
    return (xl, so)
end

function next_prediction!(f::Function,
                          g::ObserverList,
                          xl::Array{State},
                          u::Control,
                          p::Parameters;
                          mn = nothing,
                          on = nothing) where {State, Control, Parameters}
    has_observed = false
    t = length(xl) - 1
    oml = get_observation_model_list(g)
    so = SystemObservation(oml)
    while !has_observed
        t += 1
        if mn == nothing
            push!(xl, f(t, xl[t], u, p))
        else
            push!(xl, f(t, xl[t], u, p, mn))
        end
        for o in g
            if t in o.tml
                nbr_args = o.nbr_args
                if nbr_args == 3
                    vl = o.f(xl[t+1], p, on)
                elseif nbr_args == 2
                    vl = o.f(xl[t+1], on)
                else
                    vl = o.f(xl[t+1])
                end
                so.set(o.om, t, vl)
                has_observed = true
            end
        end
    end
    return so
end

function next_prediction!(f::Function,
                          g::ObserverList,
                          x::State,
                          u::Control,
                          p::Parameters,
                          t::Time;
                          mn = nothing,
                          on = nothing) where {State, Control, Parameters}
    has_observed = false
    tml = get_merged_timeline(g)
    i = findfirst(z -> z > 0, tml - t)
    tf = tml[i]
    oml = get_observation_model_list(g, tf)
    so = SystemObservation(oml)
    while !has_observed
        t += 1
        if mn == nothing
            field_copy!(x, f(t, x, u, p))
        else
            x = f(t, x, u, p, mn)
        end
        for o in g
            if t in o.tml
                nbr_args = o.nbr_args
                if nbr_args == 3
                    vl = o.f(x, p, on)
                elseif nbr_args == 2
                    vl = o.f(x, on)
                else
                    vl = o.f(x)
                end
                so.set(o.om, t, vl)
                has_observed = true
            end
        end
    end
    return x, so
end

function next_prediction_observe_all!(f::Function,
                                      g::ObserverList,
                                      x::State,
                                      u::Control,
                                      p::Parameters,
                                      t::Time;
                                      mn = nothing,
                                      on = nothing) where {State, Control, Parameters}
    has_observed = false
    tml = get_merged_timeline(g)
    i = findfirst(z -> z > 0, tml - t)
    tf = tml[i]
    oml = get_observation_model_list(g, tf)
    so = SystemObservation(oml)
    while !has_observed
        t += 1
        if mn == nothing
            field_copy!(x, f(t, x, u, p))
        else
            field_copy!(x, f(t, x, u, p, mn))
        end
        for o in g
            nbr_args = o.nbr_args
            if nbr_args == 3
                vl = o.f(x, p, on)
            elseif nbr_args == 2
                vl = o.f(x, on)
            else
                vl = o.f(x)
            end
            so.set(o.om, t, vl)
            if t in o.tml
                has_observed = true
            end
        end
    end
    return so
end

function next_prediction!(f::Function,
                          g::ObserverList,
                          xl::Array{State},
                          u::Control,
                          pl::Array{Parameters},
                          t::Time;
                          mn = nothing,
                          on = nothing) where {State, Control, Parameters}
    assert(length(xl) == length(pl))
    nbr_sim = length(xl)
    sol = Array{SystemObservation}(nbr_sim)
    for i = 1:nbr_sim
        xl[i], sol[i] = next_prediction!(f, g, xl[i], u, pl[i], t; mn = mn, on = on)
    end
    return sol
end

function next_prediction!(f::Function,
                          g::ObserverList,
                          xl::Array{State},
                          u::Control,
                          p::Parameters,
                          t::Time;
                          mn = nothing,
                          on = nothing) where {State, Control, Parameters}
    nbr_sim = length(xl)
    sol = Array{SystemObservation}(nbr_sim)
    for i = 1:nbr_sim
        xl[i], sol[i] = next_prediction!(f, g, xl[i], u, p, t; mn = mn, on = on)
    end
    return sol
end

macro observation_function()
    return esc(quote
        function create_observer_function(name)
            s = Symbol(name)
            is_ofl = false
            try
                is_ofl = isa(ofl, Array)
            catch err
                if isa(err, UndefVarError)
                    is_ofl = false
                end
            end
            if is_ofl
                ind = 0
                if VERSION >= v"0.7.0" ind = findfirst(isequal(s), map(x -> methods(x).mt.name, ofl))
                elseif VERSION >= v"0.5.0" ind = findfirst(map(x -> methods(x).mt.name, ofl), s)
                elseif VERSION >= v"0.4.6" ind = findfirst(map(x -> methods(x).name, ofl), s)
                end
                if ind != 0 && ind != nothing
                    return ofl[ind]
                end
            end
            if s in fieldnames(State)
                return observer(x) = getfield(x, s)
            else
                error("Wrong name of observation model: $name.")
            end
        end
        function create_observation_function(ol)
            if VERSION >= v"0.7.0" of = Array{Observer}(undef, 0)
            else of = Array{Observer}(0)
            end
            is_real_observation = false
            for o in ol
                oms = split(o.om, ",")
                for n in oms
                    if n == "all"
                        for s in fieldnames(State)
                            push!(of, Observer(string(s), create_observer_function(string(s)), ol[1].tml))
                        end
                    else
                        if !(Symbol(n) in fieldnames(State)) is_real_observation = true end
                        push!(of, Observer(n, create_observer_function(n), o.tml))
                    end
                end
            end
            return of
        end
    end)
end

macro simple_ctmc_observation_function()
    return esc(quote
        function create_observer_function(name)
            s = Symbol(name)
            return observer(x) = getfield(x, s)
        end
        function create_observation_function(ol)
            of = Array{Observer}(undef, 0)
            for o in ol
                oms = split(o.om, ",")
                for n in oms
                    if n == "all"
                        for s in fieldnames(State)
                            push!(of, Observer(string(s), create_observer_function(string(s)), ol[1].tml))
                        end
                    else
                        push!(of, Observer(n, create_observer_function(n), o.tml))
                    end
                end
            end
            return of
        end
    end)
end

