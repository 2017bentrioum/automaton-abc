
macro operator_obs_real(op)
    opn = Symbol("$op")
    # dopn = Symbol(".$op")
    return quote
        function $(esc(opn))(obs::ObservationTuple, x::T) where {T <: Real}
            return (first(obs), convert(Array{Value}, broadcast($(esc(opn)), last(obs), x)))
        end
        function $(esc(opn))(x::T, obs::ObservationTuple) where {T <: Real}
            return (first(obs), convert(Array{Value}, broadcast($(esc(opn)), x, last(obs))))
        end
        # $(esc(dopn))(obs::ObservationTuple, x::T) = $(esc(opn))(obs, x) where {T <: Real}
        # $(esc(dopn))(x::T, obs::ObservationTuple) = $(esc(opn))(x, obs) where {T <: Real}
    end
end

macro function_obs(f)
    fn = Symbol("$f")
    return quote
        function $(esc(fn))(obs::ObservationTuple)
            return (first(obs), convert(Array{Value}, $(esc(fn)).(last(obs))))
        end
        function $(esc(fn))(obsl::Array{ObservationTuple})
            nbr_obs = length(obsl)
            obslr = Array{ObservationTuple}(nbr_obs)
            for i = 1:nbr_obs
                obslr[i] = $(esc(fn))(obsl[i])
            end
            return obslr
        end
    end
end

macro operator_obs_obs(op)
    opn = Symbol("$op")
    return quote
        function $(esc(opn))(obs1::ObservationTuple, obs2::ObservationTuple)
            @assert first(obs1) == first(obs2)
            return (first(obs1), broadcast($(esc(opn)), last(obs1), last(obs2)))
        end
        # $(esc(dopn))(obs1::ObservationTuple, obs2::ObservationTuple) = $(esc(opn))(obs1::ObservationTuple, obs2::ObservationTuple)
    end
end

macro operator_so_real(op)
    opn = Symbol("$op")
    # dopn = Symbol(".$op")
    return quote
        function $(esc(opn))(so::SystemObservation, x::T) where {T <: Real}
            nbr_om = length(so.oml)
            sor = SystemObservation(so.oml; title = so.title)
            for i = 1:nbr_om
                # sor.otll[i] = $(esc(opn))(so.otll[i], x)
                sor.otll[i] = broadcast($(esc(opn)), so.otll[i], x)
            end
            return sor
        end
        function $(esc(opn))(x::T, so::SystemObservation) where {T <: Real}
            nbr_om = length(so.oml)
            sor = SystemObservation(so.oml; title = so.title)
            for i = 1:nbr_om
                # sor.otll[i] = $(esc(opn))(x, so.otll[i])
                sor.otll[i] = broadcast($(esc(opn)), x, so.otll[i])
            end
            return sor
        end
    end
end

macro function_so(f)
    fn = Symbol("$f")
    return quote
        function $(esc(fn))(so::SystemObservation)
            nbr_om = length(so.oml)
            sor = SystemObservation(so.oml; title = so.title)
            for i = 1:nbr_om
                sor.otll[i] = $(esc(fn)).(so.otll[i])
            end
            return sor
        end
    end
end

macro operator_so_so(op)
    opn = Symbol("$op")
    # dopn = Symbol(".$op")
    return quote
        function $(esc(opn))(so1::SystemObservation, so2::SystemObservation; title = "")
            # assert(so1.oml == so2.oml)
            nbr_om = length(so1.oml)
            ## if title == ""
            ##     title = so1.title
            ## end
            so = SystemObservation(so1.oml; title = title)
            for i = 1:nbr_om
                @assert length(so1.otll[i]) == length(so2.otll[i])
                so.otll[i] = broadcast($(esc(opn)), so1.otll[i], so2.otll[i])
            end
            return so
        end
    end
end

macro operator_sol_real(op)
    opn = Symbol("$op")
    # dopn = Symbol(".$op")
    return quote
        function $(esc(opn))(sol1::SystemObservationList, x::T) where {T <: Real}
            nbr_so = length(sol1)
            sol = Array{SystemObservation}(nbr_so)
            for i = 1:nbr_so
                sol[i] = $(esc(opn))(sol1[i], x)
            end
            return sol
        end
    end
end

macro operator_sol_so(op)
    opn = Symbol("$op")
    return quote
        function $(esc(opn))(sol1::SystemObservationList, so2::SystemObservation; title = "")
            nbr_so = length(sol1)
            sol = Array{SystemObservation}(nbr_so)
            for i = 1:nbr_so
                sol[i] = $(esc(opn))(sol1[i], so2)
            end
            return sol
        end
        function $(esc(opn))(so2::SystemObservation, sol1::SystemObservationList; title = "")
            nbr_so = length(sol1)
            sol = Array{SystemObservation}(nbr_so)
            for i = 1:nbr_so
                sol[i] = $(esc(opn))(sol1[i], so2)
            end
            return sol
        end
    end
end

macro operator_sol_sol(op)
    opn = Symbol("$op")
    return quote
        function $(esc(opn))(sol1::SystemObservationList, sol2::SystemObservationList; title = "")
            assert(length(sol1) == length(sol2))
            nbr_so = length(sol1)
            sol = Array{SystemObservation}(nbr_so)
            for i = 1:nbr_so
                sol[i] = $(esc(opn))(sol1[i], sol2[i])
            end
            return sol
        end
    end
end

opl = [+, -, *, /]

for op in opl
    @eval @operator_obs_real($op)
    @eval @operator_obs_obs($op)
    @eval @operator_so_real($op)
    @eval @operator_so_so($op)
    @eval @operator_sol_so($op)
    @eval @operator_sol_sol($op)
end

function make_positive!(so::SystemObservation)
    for i = 1:length(so.oml)
        for j = 1:length(so.otll[i])
            so.otll[i][j] = so.otll[i][j][1], max.(so.otll[i][j][2], 0.0)
        end
    end
end

@operator_obs_real(^)
@operator_so_real(^)
@operator_sol_real(^)

fl = [exp, log, sqrt]

for f in fl
    @eval @function_obs($f)
    @eval @function_so($f)
end

function ==(so1::SystemObservation, so2::SystemObservation)
    bool_oml = so1.oml == so2.oml
    bool_otll = so1.otll == so2.otll
    return bool_oml && bool_otll
end

function same_shape(so1, so2)
    if so1.oml != so2.oml return false end
    for i = 1:length(so1.oml)
        om = so1.oml[i]
        tml1 = get_timeline(so1, om)
        tml2 = get_timeline(so2, om)
        if tml1 != tml2 return false end
    end
    return true
end

function filter(so::SystemObservation,
                om::Label)
    sof = SystemObservation([om])
    ind = get_index(so, om)
    sof.otll[1] = deepcopy(so.otll[ind])
    return sof
end

function filter(so::SystemObservation,
                oml::LabelList)
    sof = SystemObservation(oml)
    for i = 1:length(oml)
        ind = get_index(so, oml[i])
        sof.otll[i] = deepcopy(so.otll[ind])
    end
    return sof
end

function filter(so::SystemObservation,
                oml::LabelList,
                tml)
    sof = SystemObservation(oml)
    for i = 1:length(oml)
        ind = get_index(so, oml[i])
        for j = 1:length(so.otll[ind])
            if so.otll[i][j][1] in tml
                push!(sof.otll[i], so.otll[ind][j])
            end
        end
    end
    return sof
end

function filter(so::SystemObservation,
                g::ObserverList,
                t::Time)
    oml = Label[]
    for o in g
        if t in o.tml push!(oml, o.om) end
    end
    so_new = SystemObservation(oml)
    for om in oml
        i = get_index(so, om)
        nbr_obs = length(so.otll[i])
        j = 0
        for k = 1:nbr_obs
            if so.otll[i][k][1] == t
                j = k
                break
            end
        end
        assert(j != 0)
        so_new.set(om, t, so.otll[i][j][2])
    end
    return so_new
end

function split(so::SystemObservation)
    sol = Array{SystemObservation}(0)
    oml = get_observation_model_list(so)
    for om in oml
        push!(sol, filter(so, om))
    end
    return sol
end

function operate(so_det::SystemObservation,
                 so_sto::SystemObservation,
                 kind::Label)
    if kind == "additive-normal"
        return so2 - so1
    elseif kind == "multiplicative-normal"
        return (so2 - so1) / so1
    end
    error("Wrong name of kind: $kind.")
end

function operate(so_det::SystemObservation,
                 so_sto::SystemObservation,
                 on::ObservationNoise) where {ObservationNoise}
    assert(length(so_det.oml) == 1)
    assert(length(so_sto.oml) == 1)
    om_det = so_det.oml[1]
    om_sto = so_sto.oml[1]
    kind = extract_noise(on, om_sto, om_sto).kind
    return operate(so_det, so_sto, kind)
end

function operate(sol_det::SystemObservationList,
                 sol_sto::SystemObservationList,
                 on::ObservationNoise) where {ObservationNoise}
    sol = SystemObservation[]
    for i = 1:length(sol_sto)
        push!(sol, operate(sol_det[i], sol_sto[i], on))
    end
    return sol
end

function merge(sol::SystemObservationList)
end

## so_exp - so_sim wouldn't work because they do not have the same observation model!!!
