
function to_vec(sol::SystemObservationList,
                om::Label,
                n::Time)
    v = Array{Value}(0)
    if VERSION >= v"0.7.0" i = findfirst(isequal(om), sol[1].oml)
    else i = findfirst(sol[1].oml, om)
    end
    nbr_obs = length(sol[1].otll[i])
    j = 0
    for k = 1:nbr_obs
        if sol[1].otll[i][k][1] == n
            j = k
            break
        end
    end
    for so in sol
        v = [v; so.otll[i][j][2]]
    end
    return v
end

function to_vec(so::SystemObservation,
                om::Label,
                n::Time)
    if VERSION >= v"0.7.0" i = findfirst(isequal(om), so.oml) 
    else i = findfirst(so.oml, om)
    end
    nbr_obs = length(so.otll[i])
    j = 0
    for k = 1:nbr_obs
        if so.otll[i][k][1] == n
            j = k
            break
        end
    end
    return so.otll[i][j][2]
end

function to_vec(so::SystemObservation,
                n::Time)
    if VERSION >= v"0.7.0" v = Array{Value}(undef, 0)
    else v = Array{Value}(0)
    end
    nbr_om = length(so.oml)
    for i = 1:nbr_om
        nbr_obs = length(so.otll[i])
        j = 0
        for k = 1:nbr_obs
            if so.otll[i][k][1] == n
                j = k
                break
            end
        end
        if j > 0
            v = [v; so.otll[i][j][2]]
        end
    end
    return v
end

function to_vec(so::SystemObservation,
                om::Label)
    i = om_findfirst(om, so.oml)
    nbr_times = length(so.otll[i])
    nbr_per_time = length(so.otll[i][1][2])
    v = (VERSION >= v"0.7.0") ? Array{Value, 1}(undef, nbr_times * nbr_per_time) :
                                Array{Value, 1}(nbr_times * nbr_per_time)
    for j = 1:nbr_times
        ind_begin =  (j-1) * nbr_per_time + 1
        ind_end = j * nbr_per_time
        v[ind_begin:ind_end] = so.otll[i][j][2]
    end
    return v
end

function to_mat(so::SystemObservation,
                om::Label;
                ignore_zeros = false)
    if VERSION >= v"0.7.0" i = findfirst(isequal(om), so.oml) 
    else i = findfirst(so.oml, om)
    end
    nbr_times = length(so.otll[i])
    nbr_comp = length(so.otll[i][1][2])
    vec_timeline = Array{Array{Int,1}}(0)
    vec_data = Array{Array{Value,1}}(0)
    for j = 1:nbr_comp
        vec_timeline_j = zeros(0)
        vec_data_j = zeros(0)
        for t = 1:nbr_times
            if !isnan(so.otll[i][t][2][j])
                if (ignore_zeros && so.otll[i][t][2][j] != 0) || !ignore_zeros
                    # push!(vec_timeline_j, t)
                    push!(vec_timeline_j, so.otll[i][t][1])
                    push!(vec_data_j, so.otll[i][t][2][j])
                end
            end
        end
        push!(vec_timeline, vec_timeline_j)
        push!(vec_data, vec_data_j)
    end
    return vec_timeline, vec_data
end

function get_maximum_size(otl)
    s = 0
    for t = 1:length(otl)
        s = max(s, length(otl[t][2]))
    end
    return s
end

function to_mat(so::SystemObservation)
    vec_timeline = Array{Array{Int,1}}(0)
    vec_data = Array{Array{Value,1}}(0)
    for i = 1:length(so.oml)
        nbr_times = length(so.otll[i])
        nbr_comp = length(so.otll[i][1][2])
        for j = 1:nbr_comp
            vec_timeline_j = zeros(0)
            vec_data_j = zeros(0)
            for t = 1:nbr_times
                if !isnan(so.otll[i][t][2][j])
                    # push!(vec_timeline_j, t)
                    push!(vec_timeline_j, so.otll[i][t][1])
                    push!(vec_data_j, so.otll[i][t][2][j])
                end
            end
            # println("vec_timeline_j = $vec_timeline_j")
            push!(vec_timeline, vec_timeline_j)
            push!(vec_data, vec_data_j)
        end
    end
    return vec_timeline, vec_data
end

function to_mat(so::SystemObservation,
                vec_timeline::Array{Array{Int,1}})
    vec_data = Array{Array{Value,1}}(0)
    ind = 1
    for i = 1:length(so.oml)
        nbr_comp = get_maximum_size(so.otll[i])
        for j = 1:nbr_comp
            vec_data_j = zeros(0)
            for t in vec_timeline[ind]
                # t0 = t
                if VERSION >= v"0.7.0" t0 = findfirst(isequal(so.otll[i]), x -> x[1] == t)
                else t0 = findfirst(x -> x[1] == t, so.otll[i])
                end
                if j <= length(so.otll[i][t0][2])
                    push!(vec_data_j, so.otll[i][t0][2][j])
                else
                    push!(vec_data_j, NaN)
                end
            end
            push!(vec_data, vec_data_j)
            if ind < length(vec_timeline)
                ind += 1
            else
                break
            end
        end
    end
    return vec_timeline, vec_data
end

function to_mat(so::SystemObservation,
                vec_timeline::Array{Array{Int,1}},
                om::Label)
    vec_data = Array{Array{Value,1}}(0)
    ind = 1
    i = get_index(so, om)
    nbr_comp = get_maximum_size(so.otll[i])
    for j = 1:nbr_comp
        vec_data_j = zeros(0)
        for t in vec_timeline[ind]
            # t0 = t
            if VERSION >= v"0.7.0" t0 = findfirst(isequal(so.otll[i]), x -> x[1] == t)
            else t0 = findfirst(x -> x[1] == t, so.otll[i])
            end
            if j <= length(so.otll[i][t0][2])
                push!(vec_data_j, so.otll[i][t0][2][j])
            else
                push!(vec_data_j, NaN)
            end
        end
        push!(vec_data, vec_data_j)
        if ind < length(vec_timeline)
            ind += 1
        else
            break
        end
    end
    return vec_timeline, vec_data
end

function to_vec(so::SystemObservation)
    if VERSION >= v"0.7.0" v = Array{Value}(undef, 0)
    else v = Array{Value}(0)
    end
    nbr_om = length(so.oml)
    for i = 1:nbr_om
        v = [v; to_vec(so, so.oml[i])]
    end
    return v
end

function to_vec(sol::SystemObservationList)
    if VERSION >= v"0.7.0" v = Array{Value}(undef, 0)
    else v = Array{Value}(0)
    end
    nbr_so = length(sol)
    for i = 1:nbr_so
        v = [v; to_vec(sol[i])]
    end
    return v
end

function to_vec(sol::SystemObservationList, om::Label)
    if VERSION >= v"0.7.0" v = Array{Value}(undef, 0)
    else v = Array{Value}(0)
    end
    nbr_so = length(sol)
    for i = 1:nbr_so
        v = [v; to_vec(sol[i], om)]
    end
    return v
end

function to_vec(x, l::Label)
    return getfield(x, Symbol(l))
end

function to_vec(x, ll::LabelList)
    nbr_labels = length(ll)
    if nbr_labels == 0 error("Empty LabelList in to_vec") end
    vec_elt = to_vec(x, ll[1])
    if typeof(vec_elt) <: Array
        nbr_per_label = length(vec_elt)
        vec = (VERSION >= v"0.7.0") ? Array{Value, 1}(undef, nbr_labels * nbr_per_label) : Array{Value, 1}(nbr_labels * nbr_per_label)
        vec[1:nbr_per_label] = vec_elt
        for j = 2:nbr_labels
            ind_begin = (j-1) * nbr_per_label + 1
            ind_end = j * nbr_per_label
            vec[ind_begin:ind_end] = to_vec(x, ll[j])
        end
    else
        vec = (VERSION >= v"0.7.0") ? Array{Value, 1}(undef, nbr_labels) : vec = Array{Value, 1}(nbr_labels)
        vec[1] = vec_elt
        for j = 2:nbr_labels
            vec[j] = to_vec(x, ll[j])
        end
    end
    return vec
end

function to_vec(xl::T, l::Label) where T <: Array
    if VERSION >= v"0.7.0" vec = Array{Value}(undef, 0)
    else vec = Array{Value}(0)
    end
    for x in xl
        vec = [vec; to_vec(x, l)]
    end
    return vec
end

function to_vec(p::Parameters, ll_p::LabelList, x::State, ll_x::LabelList) where {Parameters, State}
    nbr_p = length(ll_p)
    nbr_x = length(ll_x)
    if VERSION >= v"0.7.0" vec = Array{Value}(undef, nbr_p + nbr_x)
    else vec = Array{Value}(nbr_p + nbr_x)
    end
    for i = 1:nbr_p
        vec[i] = getfield(p, Symbol(ll_p[i]))
    end
    for i = 1:nbr_x
        vec[nbr_p+i] = getfield(x, Symbol(ll_x[i]))
    end
    return vec
end

function to_vec(edl::Array{Tuple})
    if VERSION >= v"0.7.0" vec = Array{Value}(undef, 0)
    else vec = Array{Value}(0)
    end
    for ed in edl
        vec = [vec; to_vec(ed[3])]
    end
    return vec
end

function to_mat(xl::T, ll::LabelList) where T <: Array
    nbr_samples = length(xl)
    nbr_x = length(ll)
    mat = zeros(nbr_x, nbr_samples)
    for j = 1:nbr_samples
        mat[:,j] = to_vec(xl[j], ll)
    end
    return mat
end

function to_mat(pl::Array{Parameters}, ll_p::LabelList, xl::Array{State}, ll_x::LabelList) where {Parameters, State}
    assert(length(pl) == length(xl))
    nbr_samples = length(pl)
    nbr_p = length(ll_p)
    nbr_x = length(ll_x)
    mat = zeros(nbr_p + nbr_x, nbr_samples)
    for j = 1:nbr_samples
        mat[:,j] = to_vec(pl[j], ll_p, xl[j], ll_x)
    end
    return mat
end

function to_mat(sol::SystemObservationList)
    nbr_so = length(sol)
    nbr_obs = get_total_number_of_observations(sol[1])
    mat = zeros(nbr_obs, nbr_so)
    for j = 1:nbr_so
        mat[:,j] = to_vec(sol[j])
    end
    return mat
end

function to_mat(sol::SystemObservationList, om::Label)
    nbr_so = length(sol)
    nbr_obs = get_number_of_values(sol[1], om)
    mat = zeros(nbr_obs, nbr_so)
    for j = 1:nbr_so
        mat[:,j] = to_vec(sol[j], om)
    end
    return mat
end
